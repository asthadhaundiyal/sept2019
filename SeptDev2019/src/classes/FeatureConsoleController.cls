public class FeatureConsoleController {
    
    List<Feature> features;
    String NPS_FEATURE = 'Enable Customer Survey & ROI Feature';
    String QA_FEATURE = 'Enable Quality Audit Feature';
    String FCR_FEATURE = 'Enable First Contact Resolution Feature';
    String EE_FEATURE = 'Enable Employee Engagement Survey Feature';
    String NPS_AI_FEATURE = 'Enable AI for Customer Feedback Feature';
    String QA_CC_AI_FEATURE = 'Enable AI for Case Categorisation Feature';
    String QA_SE_AI_FEATURE = 'Enable AI for Quality and Compliance Feature';
    String CASE_LANG_DETECT_FEATURE = 'Enable AI Case Language Auto Detect Feature';
    
    public FeatureConsoleController() {
        features = new List<Feature>();
        features.add(new Feature(FeatureConsoleAPI.npsEnabled(), NPS_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.qaEnabled(), QA_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.fcrEnabled(), FCR_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.eeEnabled(), EE_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.npsWithAIEnabled(), NPS_AI_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.qaWithAIEnabled(), QA_CC_AI_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.qaSEWithAIEnabled(), QA_SE_AI_FEATURE));
        features.add(new Feature(FeatureConsoleAPI.caseLangAutoDetectEnabled(), CASE_LANG_DETECT_FEATURE));
    }
    
    public class Feature {
        public Boolean enabled {get; set;}
        public String name{get;set;}
        public Feature(boolean enabled, String name) {
            this.enabled = enabled;
            this.name = name;
        }
    }
    
    public List<Feature> getFeatures() {
        return features;
    }
    
    
    public void save() {
        for (Feature feature : features) {
            if(feature.enabled) {
                try {
                    if(feature.name.equals(NPS_FEATURE)) {
                        FeatureConsoleAPI.enableNPS();
                    }else if (feature.name.equals(QA_FEATURE)) {
                        FeatureConsoleAPI.enableQA();
                    }else if (feature.name.equals(FCR_FEATURE)) {
                        FeatureConsoleAPI.enableFCR();
                    }else if (feature.name.equals(EE_FEATURE)) {
                        FeatureConsoleAPI.enableEE();
                    }else if(feature.name.equals(NPS_AI_FEATURE)) {
                        FeatureConsoleAPI.enableNPSwithAI();
                    }else if (feature.name.equals(QA_CC_AI_FEATURE)) {
                        FeatureConsoleAPI.enableQAWithAI();
                    }else if (feature.name.equals(QA_SE_AI_FEATURE)) {
                        FeatureConsoleAPI.enableQASEWithAI();
                    }else if (feature.name.equals(CASE_LANG_DETECT_FEATURE)) {
                        FeatureConsoleAPI.enableCaseLangAutoDetect();
                    }
                }
                catch(FeatureAccessException e) {
                    feature.enabled = false;
                    ApexPages.addMessages(e);
                }
            }
            else {
                if(feature.name.equals(NPS_FEATURE)) {
                    FeatureConsoleAPI.disableNPS();
                }else if (feature.name.equals(QA_FEATURE)) {
                    FeatureConsoleAPI.disableQA();
                }else if (feature.name.equals(FCR_FEATURE)) {
                    FeatureConsoleAPI.disableFCR();
                }else if (feature.name.equals(EE_FEATURE)) {
                    FeatureConsoleAPI.disableEE();
                }else if(feature.name.equals(NPS_AI_FEATURE)) {
                    FeatureConsoleAPI.disableNPSWithAI();
                }else if (feature.name.equals(QA_CC_AI_FEATURE)) {
                    FeatureConsoleAPI.disableQAWithAI();
                }else if (feature.name.equals(QA_SE_AI_FEATURE)) {
                    FeatureConsoleAPI.disableQASEWithAI();
                }else if (feature.name.equals(CASE_LANG_DETECT_FEATURE)) {
                    FeatureConsoleAPI.disableCaseLangAutoDetect();
                }
            }
        }
    }
}