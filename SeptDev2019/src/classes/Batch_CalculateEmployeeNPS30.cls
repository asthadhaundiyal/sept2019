//Calculates the number of surveys for an employee in last 30/60/90 days
global class Batch_CalculateEmployeeNPS30 implements Database.Batchable<sObject>{
    // called in batch size of 1
    
    global final String Query;
    global final set<id> employeeIds;


    global Batch_CalculateEmployeeNPS30(){
        Query = getQuery() + ' where Has_In_Gage_Licence__c = true';
    }

    private string getQuery(){
        return 'select id from Employee__c';
    }

    public string getEmployeeQuery(){
        string mindate = string.valueOf(Date.today().addDays(EventHandler.SURVEY_90_DAYS));
        string maxdate = string.valueOf(Date.today().addDays(1));
        return 'select id,Surveys_in_Last_30_Days__c,Surveys_in_Last_60_Days__c ,Surveys_in_Last_90_Days__c , (select id,Completed_Date__c from NPS__r where Completed_Date__c >='+mindate+' and Completed_Date__c <='+maxdate+') from Employee__c where Has_In_Gage_Licence__c = true';
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        id empId;
        for(sObject s: scope){
            Employee__c e = (Employee__c)s;
            empId = e.id;
        }
        //calulate Surveys in last 30/60/90 days
        if(empId != null){
            Employee__c employee;
            try{
            	employee = database.query(getEmployeeQuery() + ' and id = :empId limit 1');
                system.debug('Employee to be processed '+ employee);
            }catch(exception e){}  // failure expected when employee doesnt have In-Gage licence
            
            if(employee != null){
            	
            	// reset values prior to recalculation
            	employee.Surveys_in_Last_30_Days__c = 0;
                employee.Surveys_in_Last_60_Days__c = 0;
                employee.Surveys_in_Last_90_Days__c = 0;
                
                // recalculate employee
	            employee = EventHandler.calculateNPSForEmployee(employee);
	            database.update(employee,false);
            }
        }
    }

    global void finish(Database.BatchableContext BC){
    }
}