global class SendEmployeeSurveySchedule implements Schedulable {

    global static String CRON_EXP = '0 0 0 3 9 ? 2022';
  //scheduling is done by post install script.-- Attiq Ur-Rehman

    global void execute(SchedulableContext sc) {

        //List<Messaging.SingleEmailMessage> emailtoEmployee=new List<Messaging.SingleEmailMessage>();
        List<Employee__c> employees = [select id, User_Record__c,Next_Survey_Date__c from Employee__c where Next_Survey_Date__c =: system.today()];
        List<EmailTemplate> template=[SELECT ID, Name from EmailTemplate where Name='Employee Engagement Survey Auto' limit 1];
        list<Employee__c> updateEmployees = new list<Employee__c>();
        
        // email licenced users only
        set<id> licencedUsers = User_Trigger.licencedUsers();
        
        // confirm licenced users are active
        for(user u: [select id,isActive from user where id in: licencedUsers]){
        	if(!u.isActive)  licencedUsers.remove(u.id);
        }
        
        if(employees.size() > 0){

    		for(Employee__c emp : employees){
              if(emp.User_Record__c!=null){
              	if(licencedUsers.contains(emp.User_Record__c) || Test.isRunningTest()){ // unable to replicate managed package users in test class
              		Date dt=emp.Next_Survey_Date__c;
	                // next survey date
	                dt=dt.addMonths(1);
	                emp.Next_Survey_Date__c=dt;
	                emp.Send_Engagement_Survey__c = true;
	                updateEmployees.add(emp);
	
					// commented out because this action is performed by Process Builder.
	               /* // send email notification
	                Messaging.SingleEmailMessage mail=new Messaging.SingleEmailMessage();
	                mail.setTargetObjectId(emp.User_Record__c);
			            mail.setTemplateId(template[0].Id); // template Id
			            mail.setSaveAsActivity(false);
			            mail.setWhatId(emp.id);
	                emailtoEmployee.add(mail);
	                */
	                
              	}
              }
  			}
			
            //if(emailtoEmployee.size()>0)
             //Messaging.sendEmail(emailtoEmployee);
             //update employees;
             
            if(!updateEmployees.isEmpty()) database.update(updateEmployees,false);
        }
	}
	
}