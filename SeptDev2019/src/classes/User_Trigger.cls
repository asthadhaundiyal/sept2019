public with sharing class User_Trigger extends TriggerHandler {

    // *************************************
    // ******* Public Declarations *********
    // *************************************

    // ******************************
    // ******* Constructor **********
    // ******************************
    public User_Trigger() {}

    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {}

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {}

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {

        /*  // disabled because new users are not licenced users at the same time in a Production environment
        set<id> userIds = new set<id>();
        for(User u : (List<User>) newObjects){
            userIds.add(u.id);
        }
        createEmployee(userIds);
        */
    }

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
    	updateEmployeeTable( (List<User>) newObjects, (Map<Id, User>) oldObjectsMap);
    }

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    @future
    public static void createEmployee(set<id> userIds) {
    	createEmployeeRecord(userIds);
    }
	
	
	public static void createEmployeeRecord(set<id> userIds){
    	
    	// check users are licenced users.
    	set<id> lUsers = licencedUsers();
    	set<id> licencedUserIds = new set<id>();
    	for(id id: userIds){
    		if(lUsers.contains(id)) licencedUserIds.add(id);
    	}
        system.debug('licencedUserIds: '+ licencedUserIds);
    	
    	// check users are not already Employees
    	for(Employee__c emp: [select id,User_Record__c from Employee__c where User_Record__c in: licencedUserIds]){
    		licencedUserIds.remove(emp.User_Record__c);
    	}
        system.debug('licencedUserIds after removing already created employees: '+ licencedUserIds);
    	if(licencedUserIds.size()>0) createTheEmployeeRecords(licencedUserIds);
    }	
    
    public static void createTheEmployeeRecords(set<id> userIds){	
		List<Employee__c> employeesToInsert = new List<Employee__c>();
		map<id,user> userMap= new map<id,user>();
		userMap.putAll([select id, Email, FirstName, LastName, ManagerId, ProfileId from User where isActive = true LIMIT 50000]);
        
        system.debug('userMap'+ userMap);
		
		set<id> mgrIds = new set<id>();
		for(user u : userMap.values()){ 	
			if(u.ManagerId != null) mgrIds.add(u.ManagerId); 	// get the manager user Ids
			if(!userIds.contains(u.id)) userMap.remove(u.id);	// only process Ids passed in userIds
		}
        system.debug('mgrIds'+ mgrIds);
        system.debug('userMap after managers removed'+ userMap);
	  
	  	// process the userIds
		for(id id : userIds){
            system.debug('userMap.containsKey(id): '+ userMap.containsKey(id));
			if(userMap.containsKey(id)){
				user u = userMap.get(id);
                system.debug('u: '+ u);
				Id ingageRecordType;
			    
		    	// if user Id is ManagerId on other user records, then setup as Manager
				if(mgrIds.contains(u.id)) { 
					ingageRecordType=Schema.SObjectType.Employee__c.getRecordTypeInfosByName().get('Create New Manager').getRecordTypeId();
				}else {
					ingageRecordType=Schema.SObjectType.Employee__c.getRecordTypeInfosByName().get('Create New Employee').getRecordTypeId();
				}
                system.debug('u.FirstName: '+ u.FirstName);
                system.debug('u.LastName: '+ u.LastName);
                system.debug('u.ManagerId: '+ u.ManagerId);
                system.debug('ingageRecordType: '+ ingageRecordType);
                system.debug('u.id: '+ u.id);
                system.debug('u.Email: '+ u.Email);
				
				if(ingageRecordType!=null){
				    employeesToInsert.add(new Employee__c(
					First_Name__c = u.FirstName,
					Last_Name__c = u.LastName,
					Manager_User__c = u.ManagerId,
					RecordTypeId = ingageRecordType,
					User_Record__c = u.id,
					Email__c=u.Email,
                    Has_In_Gage_Licence__c = true,
					OwnerId = u.id
				    ));
				}
			}
			
		}
        system.debug('employeesToInsert: '+ employeesToInsert);
        system.debug('employeesToInsert.size(): '+ employeesToInsert.size());
		//if(employeesToInsert.size() > 0)	database.insert(employeesToInsert,false);
        if(employeesToInsert.size() > 0){
            Database.SaveResult[] srList = Database.insert(employeesToInsert, false);
            for (Database.SaveResult sr : srList) {
                if (!sr.isSuccess()) {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }

    }
        
    private void updateEmployeeTable(list<user> triggerNew, map<id,user> oldMap){
    	
    	map<id,user> userChangedMap = new map<id,user>();
    	for(user u: triggerNew){
            system.debug('user details: '+ u);
    		if(u.firstname != oldMap.get(u.id).firstname
	    		 || u.lastname != oldMap.get(u.id).lastname
	    		 || u.email != oldMap.get(u.id).email
	    		 || u.managerId != oldMap.get(u.id).managerId
    		 ){ userChangedMap.put(u.id,u); }
    	}
        system.debug('userChangedMap: '+ userChangedMap);
    	if(!userChangedMap.isEmpty()){
    		list<Employee__c> empToUpdate = [select id,User_Record__c,First_Name__c,Last_Name__c,Manager_User__c,Email__c from Employee__c where User_Record__c in :userChangedMap.keySet()];
    		system.debug('empToUpdate: '+ empToUpdate);
            if(!empToUpdate.isEmpty()){
    			for(user u : userChangedMap.values()){
    				for(Employee__c emp: empToUpdate){
                         system.debug('record to change: '+ +emp.User_Record__c);
    					if(u.Id == emp.User_Record__c){
                            system.debug('record to change: '+ +emp.User_Record__c);
    						emp.First_Name__c = u.FirstName;
							emp.Last_Name__c = u.LastName;
							emp.Manager_User__c = u.ManagerId;
							emp.Email__c=u.Email;
    					}
    				}
    			}
                system.debug('empToUpdate: '+ +empToUpdate);
    			database.update(empToUpdate,false);
    		}
    	}
    }


	public static set<id> licencedUsers(){
    	set<id> licencedUsers = new set<id>();
    	
    	PackageLicense inGagePackage;
        try{
        	inGagePackage = [SELECT Id FROM PackageLicense WHERE NamespacePrefix = 'in_gage' limit 1];
            system.debug('inGagePackage: '+ inGagePackage);
        
        	if(inGagePackage != null){
	        	boolean isSandbox = [SELECT IsSandbox FROM Organization].isSandbox;
	        	if(isSandbox){
	        		map<id,user> uMap = new map<id,user>([SELECT Id from User where isActive = true LIMIT 50000]);
					licencedUsers.addAll(uMap.keySet());
	        	}else{
	        		for(UserPackageLicense upl : [SELECT Id,UserId FROM UserPackageLicense WHERE PackageLicenseId = :inGagePackage.id]){
                        system.debug('UserPackageLicense: '+ upl);
		        		licencedUsers.add(upl.UserId);
		        	}
	        	}
                system.debug('licencedUsers: '+ licencedUsers);
	        }
        }
        catch(exception e){
        	// Try-Catch due to "List has no rows for assignment to SObject" failure in In-Gage Development Environment. 
        
        	if(test.isRunningTest() || userInfo.getOrganizationId() == '00D24000000K4NmEAK'){  // due to Try-Catch above
				map<id,user> uMap = new map<id,user>([SELECT Id from User where isActive = true LIMIT 50000]);
				licencedUsers.addAll(uMap.keySet());
			}
        }
        
    	return licencedUsers;
    }
	

    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
}