//Test class for AI_Batch_CreateNPSAI_T and AI_Batch_NPSPrediction_T
@istest
public class AI_Batch_NPSAI_T {
	
	
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static nps__c n;
    private static NPS_AI__c nAI;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        
       // Feature_InsertForTest.insertNPSAIFeature();
        
        setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = true, NPS_AI_Batch_Frequency__c = 1);
		insert setting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New');
		insert c;
        
        n = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English');
		insert n;
	}
	
	private static testmethod void testFCR() {
		
		setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
		// fire the batch process
		string cQuery = 'select id, Feedback_Comment__c, Language__c, AI_Language__c FROM NPS__c'; 
		database.executeBatch(new AI_Batch_CreateNPSAI(cQuery));
		
		Test.stopTest();
	}  
}