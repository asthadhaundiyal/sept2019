@istest
public class AI_Batch_CreateNPSTrainingData_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static List<nps__c> npsList;
    private static NPS_AI__c nAI;
    private static NPS_Feedback_Types__c npssetting;
    private static ingage_Application_Settings__c setting;
	
	private static void setupData(){
        
       // Feature_InsertForTest.insertNPSAIFeature();
        setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = true, NPS_AI_Batch_Frequency__c = 1);
		insert setting;
        
        npssetting = new NPS_Feedback_Types__c(Name='English Fees',Type__c ='Fees', Values__c ='fee,rates', Language__c='English', AI_Training_Language__c = 'English');        
        insert npssetting;
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test',  Language__c ='English');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New', AI_Language__c = 'English');
		insert c;

        npsList = new List<NPS__c>();
        NPS__c n2 = new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'Test English');
		npsList.add(n2);
	}
	
	private static testmethod void testTraining() {
		
		setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
		Test.startTest();
        insert npsList;
        Date startNPSDate = date.today();
        Date endNPSDate = date.today()+1;
        String startDateTime = DateTime.newInstance(startNPSDate.year(), startNPSDate.month(), startNPSDate.day()).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String closeDateTime = DateTime.newInstance(endNPSDate.year(), endNPSDate.month(), endNPSDate.day()).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
       	string npsQuery = 'select id, Feedback_Comment__c, Language__c, AI_Language__c, Reason__c FROM NPS__c where CreatedDate >=' + startDateTime + 'AND CreatedDate <=' + closeDateTime;
        database.executeBatch(new AI_Batch_CreateNPSTrainingData(npsQuery),1);
		Test.stopTest();
	}  
}