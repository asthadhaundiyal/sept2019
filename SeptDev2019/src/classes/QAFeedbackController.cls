public class QAFeedbackController {
    
    public QA_AI_Feedback__c qaFB {get; set;}
    public list<QA_AI_Feedback__c> qaFBList {get; set;}
    public List <QA_AI_Feedback__c> delqaFBList {get;set;}
    public ingage_Application_Settings__c setting {get;set;}
    public QA_AI__c qaAI {get; set;}
    public string qaCategory {get; set;}
    public string qaId {get; set;}
    public string qaLanguage {get; set;}
    public String qaText {get; set;}
    public boolean canEdit {get; set;}
    public boolean forCategory {get; set;}
    public Integer rowIndex {get;set;}
    public QA_AI_Feedback__c del;
    public List<SelectOption> qaCategories {get;set;}
    public List<SelectOption> qaSubCategories {get;set;}
    ApexPages.StandardController sc2;
    Map<String, Schema.SObjectField> caseFields = Case.getSobjectType().getDescribe().fields.getMap();
    Map<String, List<String>> subCategoryMap;
    public boolean qaSEAIEnabled {get;set;}
    public boolean qaAIEnabled {get;set;}
    
    public QAFeedbackController(ApexPages.StandardController controller) {
        
        this.qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        this.qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        
        this.qaFB = (QA_AI_Feedback__c) controller.getRecord();
        setting= ingage_Application_Settings__c.getOrgDefaults();
        canEdit = false;
        qaFBList = new list<QA_AI_Feedback__c>(); 
        this.sc2 = controller;
        forCategory = true;
        qaId = qaFB.QA_AI__c;
        if(qaId!=null){
        	qaAI = [select Text_to_Analyse__c, Language__c, Categorisation_Only__c from QA_AI__c where id= :qaId];
        	if(!qaAI.Categorisation_Only__c) forCategory = false;
            system.debug('forCategory: '+ forCategory);
        	qaText = qaAI.Text_to_Analyse__c;
            system.debug('qaText: '+ qaText);
        	qaLanguage = qaAI.Language__c;
            if(qaAIEnabled && !qaSEAIEnabled) qaFB.Text_for_Training__c = qaText;
        	qaFB.Language__c = qaLanguage;
            qaFBList.add(qaFB);
        }       
        //qaFBList.add(qaFB);
        
        String aiCategory = setting.AI_Case_Category__c;
        String aiSubCategory = setting.AI_Case_SubCategory__c;
        qaCategories=setCategories();
        subCategoryMap = AIUtilityClass.getFieldDependencies('Case', aiCategory, aiSubCategory);
        qaSubCategories = new List<SelectOption>(); 
        delqaFBList = new List < QA_AI_Feedback__c >();
    }
    
    public void addRow() {
        QA_AI_Feedback__c qaFB1 = new QA_AI_Feedback__c();
        qaFB1.QA_AI__c = qaId;
        qaFB1.Language__c = qaLanguage;
        qaFBList.add(qaFB1);
    }
    
    public PageReference save() {
        if(!qaFBList.isEmpty()) upsert qaFBList;
        canEdit = true;
        //if(!delqaFBList.isEmpty())delete delqaFBList;
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The feedback training data is successfully sent for retraining of the AI model!!'));
        return sc2.view();   
    }
    public void deleteRow() {
        rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
        del = qaFBList.remove(rowIndex);
        system.debug('del: '+ del);
        delqaFBList.add(del);
        
    }
    public PageReference cancelQAFB(){       
        String returnURL = apexPages.currentPage().getParameters().get('retURL');
        if(returnURL != null){
            pageReference pageRef = new pageReference(returnURL);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return sc2.cancel();
    }
    private List<SelectOption> setCategories(){
        String aiCategory = setting.AI_Case_Category__c;
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(' --None-- ', ' --None-- '));
        if(aiCategory!=null && caseFields.containsKey(aiCategory)){
            Schema.DescribeFieldResult fieldResult = caseFields.get(aiCategory).getDescribe();
            for( Schema.PicklistEntry f : fieldResult.getPicklistValues()){
                options.add(new SelectOption(f.getLabel(), f.getValue()));
            }
        }       
        return options;
    }
    public PageReference updateSubCategory(){
        String selectedCategory =  qaCategory;
        qaFB.Category__c = qaCategory;
        qaSubCategories.clear();
        qaSubCategories.add(new SelectOption(' --None-- ', ' --None-- '));
        system.debug('selectedCategory: '+ selectedCategory);
        if(selectedCategory!= null && subCategoryMap!= null && subCategoryMap.get(selectedCategory)!=null){
            for(String label : subCategoryMap.get(selectedCategory)){
                qaSubCategories.add(new SelectOption(label, label)); 
            }
        }     
        return null;
    }
}