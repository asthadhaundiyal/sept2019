/**
 * Test Class for QAAIFeedback_Trigger
 */
@isTest
private class QAAIFeedback_Trigger_T {

    	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static QA_AI_Feedback__c qaFB;
    private static ingage_Application_Settings__c setting;
	
    static testMethod void test() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
			FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAwithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            setting = new ingage_Application_Settings__c(Reduce_NPS_AI_Processing__c = false);
            insert setting;
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
        
        	qaFB = new QA_AI_Feedback__c(Text_for_Training__c = 'text for training', Category__c = 'Testing1', Language__c = 'English', Sub_Category__c ='test1', Emotion__c ='Happiness', Sentiment__c ='Positive');
            
            Test.startTest();
            insert qaFB;
            update qaFB;
            delete qaFB;
            Test.stopTest();
            
        }
    }
}