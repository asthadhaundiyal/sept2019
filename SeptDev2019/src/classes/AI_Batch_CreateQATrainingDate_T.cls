@isTest
public class AI_Batch_CreateQATrainingDate_T {
		
	private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
	
	private static void setupData(){
	
		emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
		insert emp;
        system.assertNotEquals(null,emp.id);
        
        a = new account(name = 'test', Language__c = 'English');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, origin = 'Email', contactId = con.id, in_gage__Employee__c = emp.id, subject ='test', description = 'test desc',type = 'Other', status = 'New', in_gage__AI_Language__c ='English');
		insert c;
	}
	
	private static testmethod void testTraining() {
		
        setupData();
        Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
        Test.startTest();
        Date startQADate = date.today();
        Date endQADate = date.today()+1;
        String startDateTime = DateTime.newInstance(startQADate.year(), startQADate.month(), startQADate.day()).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String closeDateTime = DateTime.newInstance(endQADate.year(), endQADate.month(), endQADate.day()).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        system.debug('startDateTime: '+startDateTime+ ' closeDateTime: '+closeDateTime);  
        
        string qaQuery = 'select id, Origin, Type, Subject, Description, AI_Language__c, Employee__c from Case where CreatedDate >=' + startDateTime + 'AND CreatedDate <=' + closeDateTime;
        system.debug('qaQuery: '+ qaQuery);
        database.executeBatch(new AI_Batch_CreateQATrainingData(qaQuery),100);
        Test.stopTest();
	}  
}