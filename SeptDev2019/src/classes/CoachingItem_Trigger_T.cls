@isTest
private class CoachingItem_Trigger_T {

	@isTest static void test_CoachingItemTrigger() {

		Employee__c emp;
		Employee__c mgr;
		Coaching_Items__c ci=new Coaching_Items__c();


		/*Profile agentProfile = [SELECT Id FROM Profile Limit 1];

			ingage_Application_Settings__c setting=new ingage_Application_Settings__c();
			setting.Manager_Profile_ID__c=agentProfile.Id;
			setting.Agent_Profile_ID__c=agentProfile.Id;
			insert setting;*/

		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
		UserRole ur = [SELECT Id FROM UserRole limit 1];
		// Create new user with a non-null user role ID

		System.runAs ([SELECT Id FROM User WHERE Id = :UserInfo.getUserId()][0]) {

				Test.startTest();

				User u = new User(
						Alias = 'alias',
						Email = 'email@email.email',
						Emailencodingkey = 'UTF-8',
						FirstName = 'FirstName',
						LastName = 'LastName',
						LanguageLocaleKey='en_US',
						LocalesIdKey='en_US',
						ProfileId = p.Id,
						UserRoleid = ur.Id,
						TimezonesIdKey='America/Los_Angeles',
						Username = String.valueOf(system.today())+'@email.email'
				);
				insert u;

						  mgr = new Employee__c();
							mgr.First_Name__c = 'Manager';
							mgr.User_Record__c=u.id;
							insert mgr;

							emp = new Employee__c();
							emp.First_Name__c = 'Employee';
							emp.Manager_User__c=u.Id;
							emp.User_Record__c=u.id;
							insert emp;

							ci.Employee__c=emp.Id;
							ci.NPS_score__c=0;

							insert ci;
							ci=[SELECT ID, Manager_s_Email__c from Coaching_Items__c where Id=: ci.Id];

				Test.stopTest();
		}

		system.assertNotEquals(ci.Manager_s_Email__c,null);
		system.assertEquals(ci.Manager_s_Email__c,'email@email.email');







	}



}