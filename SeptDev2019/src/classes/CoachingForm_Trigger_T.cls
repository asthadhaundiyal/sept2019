/**
 * Test Class for  CoachingForm_Trigger
 */
@isTest
private class CoachingForm_Trigger_T {

    /**
     *
     */
    static testMethod void test() {
        
        Employee__c emp = new Employee__c();
        emp.First_Name__c = 'Test';
        insert emp;
        
        Coaching_Form__c cf = new Coaching_Form__c();
        cf.Agent__c = emp.id;
        cf.Performance_Start_Date__c = system.today();
        cf.Performance_End_Date__c = system.today().addMonths(1);
        
        Test.startTest();
        
        	insert cf;
        	
        	update cf;
        	
        	delete cf;
        	
        	undelete cf;
        
        Test.stopTest();
    }
}