public with sharing class SurveyQuestion_Trigger extends TriggerHandler {

    // *************************************
    // ******* Public Declarations *********
    // *************************************

    // ******************************
    // ******* Constructor **********
    // ******************************
    public SurveyQuestion_Trigger() {}

    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {}

    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
    	checkUpdatedFriendsAndFamily((List<Survey_Question__c>) newObjects, (Map<Id, Survey_Question__c>) oldMap);
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
    	checkDeletedFriendsAndFamily((List<Survey_Question__c>) newObjects);
    }

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}


    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************

	private void checkUpdatedFriendsAndFamily(List<Survey_Question__c> triggerNew, Map<Id, Survey_Question__c> oldMap){
	// on update
	
		map<string,string> surveys = Survey_Trigger.inGageDefaultSurveyMap();
		
		map<id, Survey__c> defaultSurveys = new map<id, Survey__c>([select id, name from Survey__c where name in :surveys.keySet() or type__c in: surveys.values()]);
	
		for(Survey_Question__c sq: triggerNew){
			Survey_Question__c oldSq = oldMap.get(sq.id);
			
			if(defaultSurveys.containsKey(oldSq.Survey__c) 
			  && oldSq.name =='How did you hear about us' 
			  && oldSq.Choices__c.contains('recommendation')
			  && !Sq.Choices__c.contains('recommendation') ){
				sq.addError('Please review your Choices.  This question requires at least one choice to contain the word \'recommendation\'.');
			}
			
			if(oldSq.name != sq.name && defaultSurveys.containsKey(sq.Survey__c)){
				sq.addError('The default name fields are used by the In-Gage application and cannot be amended.');
			}
		}
	}

	private void checkDeletedFriendsAndFamily(List<Survey_Question__c> triggerNew){
	// on delete

		map<string,string> surveys = Survey_Trigger.inGageDefaultSurveyMap();
		map<id, Survey__c> defaultSurveys = new map<id, Survey__c>([select id, name from Survey__c where name in :surveys.keySet() or type__c in: surveys.values()]);
	
		for(Survey_Question__c sq: triggerNew){
			if(defaultSurveys.containsKey(sq.Survey__c) && sq.name =='How did you hear about us' && sq.Choices__c.contains('recommendation') ){
				sq.addError('This question record is used by the In-Gage application and cannot be deleted.');
			}
		}
	}


}