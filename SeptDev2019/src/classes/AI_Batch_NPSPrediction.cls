global class AI_Batch_NPSPrediction implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global final String query;
    global final set<id> npsAIIds;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();

	//used in real time
    global AI_Batch_NPSPrediction(set<id> npsAIIds){
        this.npsAIIds = npsAIIds;
        query = getNPSQuery() + ' where id in :npsAIIds';
    }
	
    //used during batching
    global AI_Batch_NPSPrediction(string query){
        this.query = query;
    }
    
	private string getNPSQuery(){
        return 'select id, Text_to_Analyse__c, AI_Category__c , Language__c, NPS__r.NPS_Comp_Value__c, NPS__r.Case_Category__c from NPS_AI__c ';
    }

   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<NPS_AI__c> npsAIList = (List<NPS_AI__c>)scope;
       system.debug('execute npsAIList: '+ npsAIList);
        List<NPS_AI__c> processedNPSAIList = new List<NPS_AI__c>();
        for(NPS_AI__c npsAI :npsAIList){
            
            String language = npsAI.Language__c;
            String textToAnalyse = npsAI.Text_to_Analyse__c;
            String caseCategory = npsAI.NPS__r.Case_Category__c;
            String npsCompValue = String.valueOf((npsAI.NPS__r.NPS_Comp_Value__c).intValue());

            if(String.isNotBlank(textToAnalyse) && String.isNotBlank(language)){
                
                String aiLang = language.toLowerCase();
                String customerId = setting.AI_Customer_ID__c;
                system.debug('customerId'+ customerId);
                HttpRequest req = new HttpRequest();
                String endPoint = 'https://ingage-ai.co.uk/nps/prediction/'+ aiLang + '/'+ customerId;
               
                system.debug('NPS Prediction EndPoint: '+ endPoint);
                req.setEndpoint(endPoint);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setBody(stringToJSONforNPSPrediction(textToAnalyse, caseCategory, npsCompValue));
                req.setTimeout(120000);
                Http http = new Http();
                HTTPResponse res = http.send(req);
                
                if (res.getStatusCode() != 200) {
                    System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                }else {
                    System.debug('NPS Prediction response: '+ res.getBody());
                    String npsCategory = '';
                    String npsCategoryScore = '';
                    PredictionAnalysis npsResults = (PredictionAnalysis)JSON.deserialize(res.getBody(), PredictionAnalysis.class);
                    for(Prediction result : npsResults.predictions){
            			Decimal relevance = result.confidence!=null ? (Decimal.valueOf(result.confidence)).setScale(2) : 0.00;
            			npsCategory += result.classification + ',';
                        npsCategoryScore += result.classification + ' - ' + String.valueOf(relevance) + ',';                      
        			}
                    
                    String npsReason = '';
                    if(npsCategory != null){
                    	List<String> npsAICategories = npsCategory.split(',');
                        system.debug('npsAICategories: '+ npsAICategories);
                        if(!npsAICategories.isEmpty()){
                            integer count = 0;
                            for(String s: npsAICategories) {
                                system.debug('s: '+s);
                                if(count == 0) npsReason += s + ','; 
                                else{
                                	if(npsReason.contains('Other')) npsReason = npsReason.remove('Other,');
                                   	if(!s.contains('Other')) npsReason += s + ',';  
                                }
                                count++;
                            }
                        }
                    }
                    npsAI.AI_Category__c = npsReason;
                    npsAI.AI_Category_Score__c = npsCategoryScore;     
                    processedNPSAIList.add(npsAI);  
                }   
            }
        }
        if(!processedNPSAIList.isEmpty()) update processedNPSAIList;
    }
    
    private Class PredictionAnalysis {
        List<Prediction> predictions {get; set;}
    }
    private Class Prediction{
        String classification {get; set;}
        String confidence {get; set;}
    }
    private static String stringToJSONforNPSPrediction(String content, String type, String promoterValue) {
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', content);
        if(type!=null) generator.writeStringField('type', type);
        if(promoterValue!=null) generator.writeStringField('promoter_val', promoterValue);
        generator.writeEndObject();
        String jsonString =  generator.getAsString();
        system.debug('jsonString: '+ jsonString);
		return jsonString;
	}   
    global void finish(Database.BatchableContext BC){} 
}