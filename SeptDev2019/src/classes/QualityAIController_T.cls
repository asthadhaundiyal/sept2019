@isTest
private class QualityAIController_T {
 	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
	
    private static testMethod void test12(){
		
       	User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
			FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAwithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();

            Email_For_Processing_Data__c emailSetting = new Email_For_Processing_Data__c(Name='testworld.com');
            insert emailSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test', Language__c ='English');
            insert a;
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            string jsonResponse = '{"sentences": [{ "sentence": "I really happy about this transaction.", "sentiment": {"classification": "Positive","confidence": 0.5478095781876149}, "emotions": [{"classification": "Concern","confidence": 0.3862179302537082}] }] }';
            string jsonResponse1 = '{"sentences": [{ "confidence": 0.6,"sentence": "I really happy about this transaction.", "sentiment": {"classification": "Positive","confidence": 0.5478095781876149}, "emotions": [{"classification": "Concern","confidence": 0.3862179302537082}] }] }';
            QA_AI__c qaAI = new QA_AI__c(AI_Score__c = 0.87,
                                              AI_Sentiment_Response__c = jsonResponse,
                                         	  FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Source__c = 'Customer', Categorisation_Only__c = true);
            
            insert qaAI;
            
            PageReference pageRef = new PageReference('/apex/QualityAIPage');
        	pageRef.getParameters().put('id', qaAI.id);
			Test.setCurrentPage(pageRef);

			ApexPages.StandardController stdCont = new ApexPages.StandardController(qaAI);
	    	QualityAIController QAFcontroller = new QualityAIController(stdCont);
            
            QA_AI__c qaAI1 = new QA_AI__c(AI_Score__c = 0.87,
                                              AI_Sentiment_Response__c = jsonResponse1,
                                         	  FirstTopCategoryLabel__c = 'TestCategory1', FirstTopCategoryScore__c = 0.5, FirstTopSubCategoryLabel__c = 'TestSubCategory1',
                                              Source__c = 'Customer', Categorisation_Only__c = true);
            
            insert qaAI1;
            
            
            PageReference pageRef1 = new PageReference('/apex/QualityAIPage');
        	pageRef1.getParameters().put('id', qaAI1.id);
			Test.setCurrentPage(pageRef1);

			ApexPages.StandardController stdCont1 = new ApexPages.StandardController(qaAI1);
	    	QualityAIController QAFcontroller1 = new QualityAIController(stdCont1);
    	
            
        }
        
	 }
}