global class AI_Batch_QATrainingLanguageDetection implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global final String query;
    global final set<id> qaAIFIds;

    global AI_Batch_QATrainingLanguageDetection(set<id> qaAIFIds){
        this.qaAIFIds = qaAIFIds;
        query = getCaseQuery() + ' where id in :qaAIFIds';
    }
     global AI_Batch_QATrainingLanguageDetection(String q){
        query = q;
    }
    private string getCaseQuery(){
        return 'select id, Text_for_Training__c, Language__c from QA_AI_Feedback__c';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<QA_AI_Feedback__c> qaAIFList = (List<QA_AI_Feedback__c>)scope;
        List<QA_AI_Feedback__c> qaAIFWithLangList = new List<QA_AI_Feedback__c>();
        for(QA_AI_Feedback__c q :qaAIFList){
            if(q.Text_for_Training__c != null){
                HttpRequest req = new HttpRequest();
                String endPoint = 'https://ingage-ai.co.uk/language/predict';
                system.debug('Language Detection EndPoint: '+ endPoint);
                req.setEndpoint(endPoint);
                req.setTimeout(120000);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(stringToJSONforLangDetection(q.Text_for_Training__c));
                req.setMethod('POST');
                system.debug('Request sent: '+ req);
                Http http = new Http();
                HTTPResponse res = http.send(req);
                if (res.getStatusCode() != 200) {
                    System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                }else {
                    System.debug('The response: '+ res.getBody());
                    String langDetected = (String)JSON.deserialize(res.getBody(), String.class);
                    q.Language__c = langDetected;
                    qaAIFWithLangList.add(q);
                }
            }
        }
        system.debug('qaAIFWithLangList: '+ qaAIFWithLangList);
        if(!qaAIFWithLangList.isEmpty()) update qaAIFWithLangList;
    }
    global void finish(Database.BatchableContext BC){}
    
    //Helper methods
    private static String stringToJSONforLangDetection(String description) {
        if(description.length() >= 24) description =+ description.substring(0, 24);
        system.debug('description: '+ description);
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', description);
        generator.writeEndObject();
        String jsonString =  generator.getAsString();
        system.debug('jsonString: '+ jsonString);
        return jsonString;
    }
}