global class Batch_NPSReviewAnalytics implements 
Database.Batchable<sObject>, Database.Stateful{
    
    global final String Query;
    global ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    
    global Batch_NPSReviewAnalytics(){
        Query = getQuery();
    }
    
    private string getQuery(){
        //string d = string.valueOf(date.today());
        date startDate = Date.today().addDays(-1);
        date closeDate = Date.today();
        string startDateTime = startDate.year()+'-'+dt(startDate.month())+'-'+dt(startDate.day())+'T20:00:00Z';
        string closeDateTime = closeDate.year()+'-'+dt(closeDate.month())+'-'+dt(closeDate.day())+'T21:00:00Z';
		system.debug('startDateTime '+ startDateTime + ' closeDateTime ' + closeDateTime);
        return 'select id, Account__c FROM NPS__c where CreatedDate >=' + startDateTime + 'AND CreatedDate <' + closeDateTime;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        list<NPS__c> npsList = (list<NPS__c>)scope;
        set<string> analyticIds = new set<string>();
        for(NPS__c nps: npsList){
            if(nps.Account__c != null){//this is for both opportunity and account
                analyticIds.add(string.valueOf(nps.Account__c));
            }   
        }
        integer Analytic_Search_Days = 0;
        if(setting.No_of_Days_Lookup_for_Analytics__c!=null)
            Analytic_Search_Days = (setting.No_of_Days_Lookup_for_Analytics__c).intValue();
        else Analytic_Search_Days = 30;
        date sDate = Date.today().addDays(-Analytic_Search_Days);
        string searchDate = sDate.year()+'-'+dt(sDate.month())+'-'+dt(sDate.day())+'T00:00:00Z';

        map<string,Analytics__c> aMap = new map<string,Analytics__c>();
        map<id,Analytics__c> updateMap = new map<id,Analytics__c>();
        
        string query = 'SELECT id, NPS__c,Account__c, recordId__c FROM Analytics__c WHERE (CreatedDate >=' +searchDate+ 'OR LastModifiedDate>=' +searchDate+') AND (recordId__c LIKE \'';
        integer i=0;
        for(NPS__c nps: npsList){
            query += string.valueOf(nps.Account__c);
            if(i < npsList.size()-1) query += '%\' OR recordId__c LIKE \'';
            i++;
        }
        query+= '%\')';
        list<Analytics__c> analyticsQuery = database.query(query);
        system.debug('analyticsQuery: '+ analyticsQuery);
        
        for(NPS__c nps : npsList){
            for(Analytics__c a : analyticsQuery){
                if(nps.Account__c == a.Account__c){ 
                    a.NPS__c = nps.Id;
                    a.stat_Case_NPS__c = null;
                    a.stat_Oppty__c = null;
                    aMap.put(a.recordId__c,a);  
                } 
            }
        }
        system.debug('aMap '+ aMap);
        if(!aMap.isEmpty()){
            // prevent dupe Analytic records
            updateMap.putAll(aMap.values());
        }
        
        set<id> updateIds = new set<id>();
        system.debug('UpdateMap '+ updateMap);
        Database.SaveResult[] saveResults = database.update(updateMap.values(),false);
    }
        private string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
}