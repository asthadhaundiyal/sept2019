public without sharing class Analytics_Trigger extends TriggerHandler {
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
	// class without sharding to enable deletion on the Analytics records when Surveys completed.  R Lloyd April 2016.
	    
    // *************************************
    // ******* Public Declarations *********
    // *************************************
    
    // ******************************
    // ******* Constructor **********
    // ******************************
    public Analytics_Trigger() {}
    
    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
    	populateFields((List<Analytics__c>)newObjects);
    }
    
    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
    	populateFields((List<Analytics__c>)newObjects);
    }

    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************

	private void populateFields(list<Analytics__c> triggerNew){
        
        set<string> caseIds = new set<string>();
        set<string> oppIds = new set<string>();
        set<string> npsIds = new set<string>();
        boolean caseClosed = false;
        boolean oppClosed = false;
        boolean npsCreated = false;
        
        map<string,id> npsTypeMap = new map<string,id>();
        for(NPS_Type__c type : [SELECT Id,Name FROM NPS_Type__c]){
            npsTypeMap.put(type.name,type.id);
        }
        for(Analytics__c a : triggerNew){
            if(a.Account__c!=null){
                if(a.NPS__c == null || a.stat_Case_NPS__c == 1 || a.stat_Oppty__c == 1){
                    if(a.Case__c != null){                       
                       caseClosed = true;
                       caseIds.add(a.Case__c);
                    }
                    if(a.Opportunity__c !=null){
                        oppClosed = true;
                        oppIds.add(a.Opportunity__c);
                    }
                }else{
                    npsCreated = true;
                    npsIds.add(a.NPS__c);
                }
                
            } 
        }
		map<id,NPS__c> npsMap = new map<id,NPS__c>();
		map<id,case> caseMap = new map<id,case>();
		map<id,opportunity> opptyMap = new map<id,opportunity>();
		
        if(!npsIds.isEmpty()) {
            npsMap = new map<id,NPS__c>([select id,Case__c,Account__c,Opportunity__c,Answer_1__c, Answer_2__c, How_Did_You_Hear_About_Us_Friend_Fam__c,NPS_Type__c from NPS__c where id in :npsIds]);
        }

        if(!caseIds.isEmpty()){
			caseMap = new map<id,case>([select id,in_gage__Closed_Date__c,ownerId,AccountId from case where id in: caseIds]);

		}
		if(!oppIds.isEmpty()){
			opptyMap = new map<id,opportunity>([select id,Closed_Date__c,ownerId,accountId from opportunity where id in: oppIds]);
		}
        system.debug('npsIds '+ npsIds);
        system.debug('caseIds '+ caseIds);
        system.debug('oppIds '+ oppIds);
        system.debug('npsMap '+ npsMap);
        system.debug('caseMap '+ caseMap);
        system.debug('opptyMap '+ opptyMap);
		// update field values
		for(Analytics__c a : triggerNew){
			if(!npsIds.isEmpty()){
				if(npsMap.containsKey(string.valueOf(a.NPS__c))){
					a.NPS_Score__c = npsMap.get(a.NPS__c).Answer_1__c;
                    a.Loyalty_Score__c = npsMap.get(a.NPS__c).Answer_2__c;
					a.Friend_Family_Recommended__c = npsMap.get(a.NPS__c).How_Did_You_Hear_About_Us_Friend_Fam__c;
					if(setting != null){
    					a.Loyal_Customer__c  = null;  // default value
                        if(setting.Loyal_Customer__c !=null ){
                         if(a.Loyalty_Score__c >= setting.Loyal_Customer__c) a.Loyal_Customer__c = 1;   
                        }   					
    				}
					if(npsMap.get(a.NPS__c).Answer_1__c >=9){
						a.stat_Promoter__c =1;
						a.stat_Detractor__c =null;
						
					}else if(npsMap.get(a.NPS__c).Answer_1__c <=6){
						a.stat_Detractor__c =1;
						a.stat_Promoter__c =null;
					}
					else{
						a.stat_Detractor__c =null;
						a.stat_Promoter__c =null;
					}
                    //a.Case__c = npsMap.get(a.NPS__c).Case__c;
				}
				a.stat_NPS__c = 1;
                if(a.Case__c!=null)a.stat_Case_NPS__c =1;
                else a.stat_Oppty_NPS__c =1;
                if(a.Opportunity__c!=null)a.stat_Oppty__c =1;
                if(a.Case__c!=null)a.stat_Case__c =1;
                a.NPS_Type__c = npsMap.get(a.NPS__c).NPS_Type__c;
			}
			
			if(!caseIds.isEmpty()){
				if(caseMap.containsKey(a.Case__c)){
					a.Record_ClosedDate__c = caseMap.get(a.Case__c).in_gage__Closed_Date__c;
					a.Account__c = caseMap.get(a.Case__c).accountId;
					string ownerId = string.valueOf(caseMap.get(a.Case__c).ownerId);
					a.ownerId = ownerId.left(3) == '500' ? ownerId : UserInfo.getUserId();
				}
				a.stat_Case__c = 1;
				if(a.NPS__c != null) a.stat_Case_NPS__c =1;
				a.NPS_Type__c = npsTypeMap.get('Service');

			}
			if(!oppIds.isEmpty()){
				if(opptyMap.containsKey(a.Opportunity__c)){
					a.Record_ClosedDate__c = opptyMap.get(a.Opportunity__c).Closed_Date__c;
					a.ownerId = opptyMap.get(a.Opportunity__c).ownerId;
					a.Account__c = opptyMap.get(a.Opportunity__c).accountId;
				}
				a.stat_Oppty__c = 1;
				if(a.NPS__c != null) a.stat_Oppty_NPS__c =1;
				a.NPS_Type__c = npsTypeMap.get('Sales');
			}
		}
	}
	
	
    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
}