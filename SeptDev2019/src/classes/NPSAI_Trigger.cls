public without sharing class NPSAI_Trigger extends TriggerHandler {
    
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    boolean npsAIEnabled {get;set;}
    
    public NPSAI_Trigger() {
        this.npsAIEnabled = FeatureConsoleAPI.npsWithAIEnabled();
    }

    public override void beforeInsert(List<SObject> newObjects) {}
    
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {}
    
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
        system.debug('npsAIEnabled :'+ npsAIEnabled);
        if(npsAIEnabled) updateNPSReason((List<NPS_AI__c>) newObjects, (Map<Id, NPS_AI__c>) oldObjectsMap);
    }
    
    private void updateNPSReason(List<NPS_AI__c> newObjects, Map<Id, NPS_AI__c> oldMap){
        set<id> npsIds = new set<id>();
        for(NPS_AI__c npsAI : newObjects){
            if(npsAI.AI_Category__c != oldMap.get(npsAI.id).AI_Category__c) npsIds.add(npsAI.NPS__c);
        }
        List<NPS__c> npsList = [select id, Reason__c, (select id,AI_Category__c from NPS_AI__r) from NPS__c where id in : npsIds];
        if(!npsList.isEmpty()){           
            for(NPS__c n: npsList){
                for(NPS_AI__c npsAI : n.NPS_AI__r) n.Reason__c = npsAI.AI_Category__c;                
            }
         update npsList;
        }
    }

}