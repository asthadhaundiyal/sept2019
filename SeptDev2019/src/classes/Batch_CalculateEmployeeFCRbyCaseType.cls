global class Batch_CalculateEmployeeFCRbyCaseType implements Database.Batchable<sObject>{
    // called in batch size of 1
    
    global final String query;

    global Batch_CalculateEmployeeFCRbyCaseType(){
        query = getQuery() + ' where Has_In_Gage_Licence__c = true and User_Record__r.isActive = true';
    }

    private string getQuery(){
        return 'select id, FCR_by_Case_Type_Highest_3__c, FCR_by_Case_Type_Lowest_3__c, User_Record_Profile__c, Employees_in_Hierarchy__c, Manager_Employee__r.Employees_in_Hierarchy__c from Employee__c';
    }
    
    private string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        id empId;
        Employee__c employee;
        for(sObject s: scope){
            employee = (Employee__c)s;
            empId = employee.id;
        }
        system.debug('empId: '+ empId);
        system.debug('employee: '+ employee);
            if(employee != null){
            	// reset values prior to recalculation
            	employee.FCR_by_Case_Type_Highest_3__c = '';
                employee.FCR_by_Case_Type_Lowest_3__c = '';
                
                // recalculate employee
	            employee = EventHandler.calculateFCRbyCaseTypeForEmpDB(employee);
	            database.update(employee,false);
            }
        
    }

    global void finish(Database.BatchableContext BC){
    }
}