public with sharing class Opportunity_Trigger extends TriggerHandler {
    
    //App Features
    private Boolean npsEnabled {get;set;}
    //App Features
    
    //Custom settings
    private ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    private set<String> oppTypesForTriggerSet;
    private set<String> oppRTypesForTriggerSet;
    //Custom settings
    
    //Eligiblity Map
    private map<Id,sObject> recordTypeEligibleMap;
    //Eligiblity Map
    
    // ******************************
    // ******* Constructor **********
    // ******************************
    public Opportunity_Trigger() {
        //App feature assignment
    	this.npsEnabled = FeatureConsoleAPI.npsEnabled();
        //App feature assignment
        oppTypesForTriggerSet = getOppTypesForTrigger();
        oppRTypesForTriggerSet = getOppRecordTypesForTrigger();
    }
    
    // **********************************
    // ******* Before Insert ************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects) {
        //updateClosedDate((List<opportunity>) newObjects,null);
        if(npsEnabled){
            recordTypeEligibleMap = getRecordTypeEligibleMap((List<Opportunity>) newObjects);
        	populateReferenceFields((List<opportunity>) newObjects,null);
        	checkSurveyContact((List<opportunity>) newObjects);
        }
    }
    
    // **********************************
    // ******* Before Update ************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {
        if(npsEnabled){
            recordTypeEligibleMap = getRecordTypeEligibleMap((List<Opportunity>) newObjects);
            boolean surveyUpdate = checkForSurveyUpdate((List<opportunity>) newObjects, (Map<Id, opportunity>) oldMap);
            if(!surveyUpdate){
                updateClosedDate((List<opportunity>) newObjects, (Map<Id, opportunity>) oldMap);
                populateReferenceFields((List<opportunity>) newObjects, (Map<Id, opportunity>) oldMap);
                checkSurveyContact((List<opportunity>) newObjects);
            }
            if(runBeforeUpdateOnce()){
                clearSendSurveyFields((List<opportunity>) newObjects, (Map<Id, opportunity>) oldMap);
                sendOpportunityWonSurvey((List<Opportunity>) newObjects, (Map<Id, Opportunity>) oldMap);
                sendReminderOpportunityWonSurvey((List<Opportunity>) newObjects, (Map<Id, Opportunity>) oldMap);              
            }
        }
    }
    
    // **********************************
    // ******* Before Delete ************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}
    
    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        if(npsEnabled){
        	recordTypeEligibleMap = getRecordTypeEligibleMap((List<Opportunity>) newObjects);
        	reviewAnalytics((List<Opportunity>) newObjects);
        }
    }
    
    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {
        if(npsEnabled){
        	recordTypeEligibleMap = getRecordTypeEligibleMap((List<Opportunity>) newObjects);
        	reviewAnalytics((List<Opportunity>) newObjects);
        }
    }
    
    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}
    
    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}
    
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    
    
    // bypasses trigger actions if only the Suvery Reminder Date is blanked.
    private boolean checkForSurveyUpdate(List<Opportunity> newObjects, Map<Id, Opportunity> oldMap){
        for(opportunity o:newObjects){
            if(o.Survey_Reminder_Date__c == null && oldMap.get(o.id).Survey_Reminder_Date__c != null) return true;
        }
        return false;
    }
    
    //clear send survey fields
    private void clearSendSurveyFields(List<Opportunity> triggerNew, Map<Id,Opportunity> oldMap){
        for(Opportunity opp: triggerNew){     
            opp.Send_Default_Survey__c = false;
            opp.Send_Default_Reminder_Survey__c = false;
            opp.Send_Custom_Survey__c = false;
            opp.Send_Custom_Reminder_Survey__c = false;
        } 
    }
    private void reviewAnalytics(list<opportunity> triggerNew){
        if(!setting.Reduce_FCR30_Calculations__c){
            if(runOnce()){
                integer Analytic_Search_Days = 0;
                if(setting.No_of_Days_Lookup_for_Analytics__c!=null)
                    Analytic_Search_Days = (setting.No_of_Days_Lookup_for_Analytics__c).intValue();
                else Analytic_Search_Days = 30;
                date sDate = Date.today().addDays(-Analytic_Search_Days);
                string searchDate = sDate.year()+'-'+dt(sDate.month())+'-'+dt(sDate.day())+'T00:00:00Z';
                set<id> opptyIds = new set<id>();
                for(opportunity o: triggerNew){
                    boolean eligibleToRun = true;
                    if(recordTypeEligibleMap != null && recordTypeEligibleMap.containsKey(o.id)){
                        string recordTypeId = (String)recordTypeEligibleMap.get(o.Id).get('RecordTypeId');
                        if(recordTypeId!=null && !oppRTypesForTriggerSet.isEmpty() && !oppRTypesForTriggerSet.contains(recordTypeId)) eligibleToRun = false; 
                    }
                    if(o.Type!=null && !oppTypesForTriggerSet.contains(o.Type)) eligibleToRun = false;
                    if(eligibleToRun){
                        if(o.isWon) opptyIds.add(o.id);
                    }
                }
                if(!opptyIds.isEmpty()) reviewAnalyticsAtFuture(opptyIds,searchDate);
            }
        }
    }
    
    private static void reviewAnalyticsAtFuture(set<id> opptyIds, string searchDate){
        if(system.isBatch() || system.isFuture()){
            reviewAnalyticsAtFuture2(opptyIds,searchDate);
        }else{
            reviewAnalyticsAtFuture1(opptyIds,searchDate);
        }       
    }
    
    @future
    private static void reviewAnalyticsAtFuture1(set<id> opptyIds, string searchDate){
        reviewAnalyticsAtFuture2(opptyIds,searchDate);
    }
    
    private static void reviewAnalyticsAtFuture2(set<id> opptyIds, string searchDate){
        List<Opportunity> oppList = [select id, accountid from opportunity where id in: opptyIds];
        // get all the Analytics records starting with the same caseIds
        string query = 'SELECT id, recordId__c,Opportunity__c,NPS__c FROM Analytics__c WHERE CreatedDate >=' +searchDate+ 'AND  (recordId__c LIKE \'';
        integer i=0;
        for(Opportunity o: oppList){
            query += string.valueOf(o.accountId);
            if(i < opptyIds.size()-1) query += '%\' OR recordId__c LIKE \'';
            i++;
        }
        query+= '%\')';
        system.debug('query '+ query);
        list<Analytics__c> analyticsQuery = database.query(query);
        list<Analytics__c> analyticsUpdate = new list<Analytics__c>();
        
        // flag presence of large ids ( case + NPS Id)
        set<string> opptyIdSet = new set<string>();
        
        // prepare the update list
        for(Opportunity o: oppList){
            boolean inAnalyticsQuery = false;
            for(Analytics__c a : analyticsQuery){
                string accountId = a.recordId__c.substringBefore(' ')+' ';
                if(string.valueOf(o.accountId)+' ' == accountId){
                    inAnalyticsQuery = true;
                    a.Opportunity__c = o.id;
                    analyticsUpdate.add(a);
                }
            }
            // create new Analytics record if one doesnt exist
            if(!inAnalyticsQuery) 
                analyticsUpdate.add(new Analytics__c(recordId__c = string.valueOf(o.accountid)+' ' + string.valueOf(o.id), 
                                                              Account__c = string.valueOf(o.accountid),
                                                              Opportunity__c = string.valueOf(o.id)));
            system.debug('inAnalyticsQuery '+ inAnalyticsQuery);
            system.debug('analyticsUpdate '+ analyticsUpdate);
        }
        if(!analyticsUpdate.isEmpty()){
            try{
                upsert analyticsUpdate recordId__c;
            }catch(exception e){
                system.debug('Error upserting Analytics record. Index[0] = : '+analyticsUpdate[0] + '. '+ e.getMessage());
            }
        }
    } 
    
    private static string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }
    private void updateClosedDate(list<opportunity> triggerNew, map<id,opportunity> oldMap){
        if(oldMap == null) oldMap = new map<id,opportunity>();
        for(opportunity o: triggerNew){
            if(o.isClosed){
                if(oldMap.containsKey(o.id)){
                    if(oldMap.get(o.id).isClosed == false) o.Closed_Date__c = o.closeDate;
                }else{
                    o.Closed_Date__c = o.closeDate;
                }
            }
        }
    }
    
    private void populateReferenceFields(list<opportunity> triggerNew, map<id,opportunity> oldMap){
        if(oldMap == null) oldMap = new map<id,opportunity>();
        
        set<id> contactIds = new set<id>();
        set<id> ownerIds = new set<id>();
        
        for(opportunity o : triggerNew){
            boolean eligibleToRun = true;
            if(recordTypeEligibleMap != null && recordTypeEligibleMap.containsKey(o.id)){
                string recordTypeId = (String)recordTypeEligibleMap.get(o.Id).get('RecordTypeId');
                if(recordTypeId!=null && !oppRTypesForTriggerSet.isEmpty() && !oppRTypesForTriggerSet.contains(recordTypeId)) eligibleToRun = false; 
            }
            if(o.Type!=null && !oppTypesForTriggerSet.contains(o.Type)) eligibleToRun = false;
            if(eligibleToRun){            
                if(oldMap.isEmpty()){ // insert single record
                    contactIds.add(o.Survey_Contact__c);
                    ownerIds.add(o.OwnerId);
                }else{ // update
                    if(o.Survey_Contact__c != null){ 
                        if(oldMap.containsKey(o.id)){
                            if(o.Survey_Contact__c != oldMap.get(o.id).Survey_Contact__c) contactIds.add(o.Survey_Contact__c);
                        }else{  // insert bulk
                            contactIds.add(o.Survey_Contact__c);
                        }
                    }
                    if(o.OwnerId != oldMap.get(o.id).OwnerId) ownerIds.add(o.OwnerId);
                }
            }
            
        }
        
        map<id,contact> cMap = new map<id,contact>();
        if(!contactIds.isEmpty()) cMap.putAll([select id,firstname,email from contact where id in: contactIds]);
        
        map<id,user> uMap = new map<id,user>();
        if(!ownerIds.isEmpty()) uMap.putAll([select id, Employee_ID__c from user where id in: ownerIds]);
        
        for(opportunity o : triggerNew){
            
            if(!cMap.isEmpty() && cMap.containsKey(o.Survey_Contact__c)){
                o.Survey_ContactId__c = cMap.get(o.Survey_Contact__c).id;
                o.Survey_Contact_First_Name__c = cMap.get(o.Survey_Contact__c).firstname;
                o.Survey_ContactEmail__c = cMap.get(o.Survey_Contact__c).email;
            }   
            
            if(!uMap.isEmpty()){
                //o.Opp_Owner_EmployeeID__c = uMap.get(o.OwnerId).Employee_ID__c;
                o.Opp_Owner_EmployeeID__c = uMap.containsKey(o.OwnerId) ? uMap.get(o.OwnerId).Employee_ID__c : null;
            }
        }
        
    }
    
    
    private void checkSurveyContact(List<opportunity> triggerNew){
        
        if(personAccountsEnabled()){
            
            set<id> ownerIds = new set<id>();
            set<id> accountIds = new set<id>();
            for(opportunity o : triggerNew){
                boolean eligibleToRun = true;
                if(recordTypeEligibleMap != null && recordTypeEligibleMap.containsKey(o.id)){
                    string recordTypeId = (String)recordTypeEligibleMap.get(o.Id).get('RecordTypeId');
                    if(recordTypeId!=null && !oppRTypesForTriggerSet.isEmpty() && !oppRTypesForTriggerSet.contains(recordTypeId)) eligibleToRun = false; 
                }
                if(o.Type!=null && !oppTypesForTriggerSet.contains(o.Type)) eligibleToRun = false;
                if(eligibleToRun){
                    ownerIds.add(o.OwnerId);
                    accountIds.add(o.AccountId);
                }
            }	
            map<id,user> uMap = new map<id,user>();
            if(!ownerIds.isEmpty()) uMap.putAll([select id, Employee_ID__c from user where id in: ownerIds]);
            
            if(!accountIds.isEmpty()){
                string query ='select id, PersonContactId,PersonContact.Email, PersonContact.FirstName from Account where id in: accountIds';
                list<sObject> aList = database.query(query);
                map<id,sObject> aMap = new map<id,sObject>(aList);   
                set<id> contactIds = new set<id>();
                
                try{
                    for(opportunity o: triggerNew){
                        account a = (account)aMap.get(o.AccountId);
                        string cid = string.valueof(a.get('PersonContactId'));
                        o.Survey_Contact__c = cid;
                        o.Survey_ContactId__c = cid;
                        contactIds.add(cid);
                    }
                    
                    map<id,contact> cMap = new map<id,contact>([select id, email, firstname from contact where id in: contactIds]);
                    
                    for(opportunity o: triggerNew){
                        string em,fn;
                        em = string.valueof(cMap.get(o.Survey_Contact__c).email);
                        fn = string.valueof(cMap.get(o.Survey_Contact__c).firstname);
                        
                        o.Survey_ContactEmail__c = em != null ? em : null;  
                        o.Survey_Contact_First_Name__c = fn != null ? fn : null;
                        o.Opp_Owner_EmployeeID__c = uMap.containsKey(o.OwnerId) ? uMap.get(o.OwnerId).Employee_ID__c : null;
                    }
                    
                }catch(exception e){
                    // expected when Person Account is not enabled.
                }                
            }           	
        }
    }
    private void sendOpportunityWonSurvey(List<Opportunity> newObjects, Map<Id, Opportunity> oldMap){
        set<id> oppIds = new set<id>();
        set<id> oppForDefaultSurveySet = new set<id>();
        map<Id,Account> accountMap = getAccountMap(newObjects);
        map<Id,Contact> contactMap = getContactMap(newObjects);
        map<Id,String> oppOwnerProfileMap = getOppOwnerProfileName(newObjects);
        map<Id,sObject> oppSurveyEligibleMap = getOppSurveyEligibleMap(newObjects);
        Integer coolOffPeriod = setting.in_gage__Survey_Cooling_off_Period__c != null ? Integer.valueOf(setting.in_gage__Survey_Cooling_off_Period__c) : 30;
        system.debug('coolOffPeriod: '+ coolOffPeriod);
        for(Opportunity opp : newObjects){
            system.debug('opp id: '+ opp.id);
            system.debug('opp is won?: '+ opp.isWon);
            system.debug('opp new stageName: '+ opp.StageName);
            system.debug('opp old stageName: '+ oldMap.get(opp.id).StageName);
            system.debug('opp employee id: '+ opp.Opp_Owner_EmployeeID__c);
            if(opp.isWon && opp.StageName != oldMap.get(opp.id).StageName && opp.Opp_Owner_EmployeeID__c != null){
                system.debug('opp is won and closed with employee id');
                boolean eligibleToRun = true;
                if(recordTypeEligibleMap != null && recordTypeEligibleMap.containsKey(opp.id)){
                    string recordTypeId = (String)recordTypeEligibleMap.get(opp.Id).get('RecordTypeId');
                    if(recordTypeId!=null && !oppRTypesForTriggerSet.isEmpty() && !oppRTypesForTriggerSet.contains(recordTypeId)) eligibleToRun = false; 
                }
                
                if(opp.Type!=null && !oppTypesForTriggerSet.contains(opp.Type)) eligibleToRun = false;
                system.debug('eligibleToRun: '+ eligibleToRun);
                if(eligibleToRun){
                    //Survey Setting profile check
					boolean surveySettingEligible = false;
                    if(oppOwnerProfileMap != null && oppOwnerProfileMap.containsKey(opp.id)){
                    	Survey_Settings__c  surveySetting = Survey_Settings__c.getValues(oppOwnerProfileMap.get(opp.Id));
                        if(surveySetting!= null && surveySetting.in_gage__Send_Opportunity_Won_Survey__c){
                        	surveySettingEligible = true;   
                        }   
                    } 
                    boolean surveyCustomEligible = false;
                    if(oppSurveyEligibleMap != null && oppSurveyEligibleMap.containsKey(opp.id)){
                        surveyCustomEligible = (Boolean) oppSurveyEligibleMap.get(opp.Id).get('Is_Survey_Eligible_To_Send__c');
                    }else if(userInfo.getOrganizationId() == '00D24000000K4NmEAK'){
						surveyCustomEligible = true;
					}
                    boolean accSurveyEligible = false;
                    system.debug('Account details: '+ accountMap);
                    Date coolOffDate = Date.today() - coolOffPeriod;
                    system.debug('Cool off period date: '+ coolOffDate);
                    if(opp.AccountId!=null){
                        if(!accountMap.get(opp.AccountId).Do_Not_Survey__c
                           && (accountMap.get(opp.AccountId).Survey_Sent_Date__c == null || accountMap.get(opp.AccountId).Survey_Sent_Date__c < coolOffDate)){
                               accSurveyEligible = true;
                           }
                        
                    }
                    boolean conSurveyEligible = false;
                    system.debug('Contact details: '+ contactMap);
                    if(opp.Survey_Contact__c!= null){
                        if(!contactMap.get(opp.Survey_Contact__c).Do_Not_Survey__c
                           && (contactMap.get(opp.Survey_Contact__c).Last_Survey_Sent_Date__c == null || contactMap.get(opp.Survey_Contact__c).Last_Survey_Sent_Date__c < coolOffDate)){
                               conSurveyEligible = true;
                           }
                        
                    }
                    system.debug('surveySettingEligible '+ surveySettingEligible + ' accSurveyEligible '+ accSurveyEligible
                                + ' conSurveyEligible: '+ conSurveyEligible
                                + ' surveyCustomEligible: '+ surveyCustomEligible
                                + 'String.isNotBlank(setting.Opportunity_Won_Survey__c): ' + String.isNotBlank(setting.Opportunity_Won_Survey__c));
                    if(surveySettingEligible && accSurveyEligible && conSurveyEligible && String.isNotBlank(setting.Opportunity_Won_Survey__c)
                       && String.isNotBlank(opp.Survey_ContactEmail__c)
                       && !opp.Opportunity_Won_Survey_Sent__c 
                       && opp.StageName != oldMap.get(opp.id).StageName
                       && surveyCustomEligible){
                           opp.Can_Send_Opportunity_Won_Survey__c = true;
                           if(setting.Is_Custom_Survey__c){
                               system.debug('Running custom survey process builder');
                               opp.Send_Custom_Survey__c = true;
                           }else{
                               system.debug('Running default survey processes');                       
                               //update Survey Sent field
                               opp.Opportunity_Won_Survey_Sent__c = true;
                               //send email
                               opp.Send_Default_Survey__c = true;
                               //update Oppty Won Survey Reminder Date
                               if(opp.Survey_Reminder_Date__c == null){
                                   if(opp.Survey_Reminder_Days__c!=null){
                                       integer surveyReminderDays = Integer.valueOf(opp.Survey_Reminder_Days__c);
                                       opp.Survey_Reminder_Date__c = Date.Today() + surveyReminderDays;  
                                   }
                               }else opp.Survey_Reminder_Date__c = null;
                               
                               oppForDefaultSurveySet.add(opp.id); 
                           }   
                       }
                }   
            }
            
        }
        if(!oppForDefaultSurveySet.isEmpty()){
            //sendSurvey(oppForDefaultSurveySet, 'Send_Opportunity_Won_Survey');
            createSurveyTask(oppForDefaultSurveySet, 'Send Opportunity Won Survey to Customer');
            updateSurveySentDateOnContact(oppForDefaultSurveySet);
        } 
    }
    private void sendReminderOpportunityWonSurvey(List<Opportunity> newObjects, Map<Id, Opportunity> oldMap){
        set<id> oppForReminderSurveySet = new set<id>();
        map<Id,sObject> oppSurveyEligibleMap = getOppSurveyEligibleMap(newObjects);
        for(Opportunity opp: newObjects){
            system.debug('Criteria to send reminder survey - Opportunity is won Closed: '+ opp.isWon 
                         +' ,Can Send Opportunity Survey: '+ opp.Can_Send_Opportunity_Won_Survey__c);
            if(opp.isWon && opp.Can_Send_Opportunity_Won_Survey__c){
                boolean eligibleToRun = true;
                if(recordTypeEligibleMap != null && recordTypeEligibleMap.containsKey(opp.id)){
                    string recordTypeId = (String)recordTypeEligibleMap.get(opp.Id).get('RecordTypeId');
                    if(recordTypeId!=null && !oppRTypesForTriggerSet.isEmpty() && !oppRTypesForTriggerSet.contains(recordTypeId)) eligibleToRun = false; 
                }
                if(opp.Type!=null && !oppTypesForTriggerSet.contains(opp.Type)) eligibleToRun = false;
                if(eligibleToRun){
                    boolean surveyReminderEligible = false;            
                    if(opp.StageName == oldMap.get(opp.id).StageName 
                       && (!opp.Opportunity_Won_Survey_Sent__c 
                           && opp.Opportunity_Won_Survey_Sent__c != oldMap.get(opp.id).Opportunity_Won_Survey_Sent__c
                           && opp.Survey_Reminder_Date__c!=null)){
                               if(opp.Survey_Reminder_Date__c >= Date.today()-10 && opp.Survey_Reminder_Date__c <= Date.today()){
                                   surveyReminderEligible = true;    
                               }
                           }
                    system.debug('Criteria to send reminder survey - surveyReminderEligible '+ surveyReminderEligible);
                    if(surveyReminderEligible){
                        if(setting.Is_Custom_Survey__c){                       
                            opp.Send_Custom_Reminder_Survey__c = true;
                            system.debug('Running custom survey process builder');
                        }else{
                            system.debug('Running default survey processes');
                            //update Survey Sent
                            opp.Opportunity_Won_Survey_Sent__c = true;
                            //update Survey Reminder date
                            opp.Survey_Reminder_Date__c = null;
                            //send email
                            opp.Send_Default_Reminder_Survey__c = true;
                            oppForReminderSurveySet.add(opp.id); 
                        }   
                    }
                }  
            }
            
        }
        if(!oppForReminderSurveySet.isEmpty()){                
            createSurveyTask(oppForReminderSurveySet, 'Send Opportunity Won Survey to Customer');
            updateSurveySentDateOnContact(oppForReminderSurveySet);
        }    
    }      
    
    private void createSurveyTask(set<id> oppIds, string subject){
        system.debug('createSurveyTask for: '+ oppIds +' with subject: '+ subject);
        List<Task> surveyTaskList = new List<Task>();
        List<Opportunity> oppList = [select id, ownerid  from opportunity where id in :oppIds];
        if(!oppList.isEmpty()){
            for(Opportunity opp : oppList){                
                Task surveyTask = new Task();
                surveyTask.OwnerId = opp.OwnerId;
                surveyTask.Subject = subject;
                surveyTask.Status = 'Completed';
                surveyTask.WhatId = opp.id;
                surveyTaskList.add(surveyTask);
            }
        }
        if(!surveyTaskList.isEmpty())insert surveyTaskList; 
    }
    private void updateSurveySentDateOnContact(set<id> oppIds){
        system.debug('updateSurveySentDateOnContact for: '+ oppIds);
        Set<Id> contactIds = new Set<Id>();
        List<Opportunity> oppList = [select id, Survey_Contact__c from Opportunity where id in :oppIds];
        system.debug('oppList: '+ oppList);
        for(Opportunity opp: oppList){
            if(opp.Survey_Contact__c!=null) contactIds.add(opp.Survey_Contact__c);
        }
        system.debug('contactIds: '+ contactIds);
        List<Contact> conList = [select id, Last_Survey_Sent_Date__c from Contact where id in :contactIds];
        system.debug('conList: '+ conList);
        if(!conList.isEmpty()){
            for(Contact c: conList) c.Last_Survey_Sent_Date__c = Date.Today();   
        }
        system.debug('conList: '+ conList);
        if(!conList.isEmpty()){
            try{
                update conList;
            }catch(exception e){
                system.debug('Error updating Last Survey Sent Date on Contact with ID = : '+conList[0] + '. '+ e.getMessage());
            }
        }         
    }
    
    // *********************************************************************************************************************************************************************************************
    // ********************************************************************** Utility Methods ******************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    //get Opportunity Types included in custom setting
    private Set<String> getOppTypesForTrigger(){
    	List<Opp_Types_To_Run_Opp_Trigger__c> oppTypeSetting = Opp_Types_To_Run_Opp_Trigger__c.getall().values();     
        Set<String> oppTypesForTriggerSet = new Set<String>();
        if(!oppTypeSetting.isEmpty()){
            for(Opp_Types_To_Run_Opp_Trigger__c oType : oppTypeSetting) oppTypesForTriggerSet.add(oType.Name);
        }
        return oppTypesForTriggerSet;        
    }
    //get Opportunity Record Types included in custom setting
    private Set<String> getOppRecordTypesForTrigger(){
    	List<Opp_RTypes_To_Run_Opp_Trigger__c> oppRTypeSetting = Opp_RTypes_To_Run_Opp_Trigger__c.getall().values();           
        Set<String> oppRTypesForTriggerSet = new Set<String>();
        if(!oppRTypeSetting.isEmpty()){
            for(Opp_RTypes_To_Run_Opp_Trigger__c oType : oppRTypeSetting) oppRTypesForTriggerSet.add(oType.Name);
        }
        return oppRTypesForTriggerSet;        
    }
    public Map<Id, sObject> getOppSurveyEligibleMap(List<Opportunity> oppList){
        
        SObject so = Schema.getGlobalDescribe().get('Opportunity').newSObject();
        boolean customFieldExists = false;
        string theQuery = '';
        
        if(so.getSobjectType().getDescribe().fields.getMap().containsKey('Is_Survey_Eligible_To_Send__c')){
            customFieldExists = true;
            theQuery = 'select Id, Is_Survey_Eligible_To_Send__c from Opportunity WHERE Id IN';
        }
        
        Map<Id,sObject> oppSurveyEligibleMap = null;
        if(customFieldExists){
            Set<Id> oppIds = new Set<Id>();
            for(opportunity opp: oppList){
                oppIds.add(opp.id);
            }        
            if(oppIds.size() > 0){
                theQuery += ':oppIds';
                oppSurveyEligibleMap = new Map<Id,sObject>((List<Opportunity>)Database.query(theQuery));
            }
        }
        return oppSurveyEligibleMap;
    }
    private Map<Id,Account> getAccountMap(List<Opportunity> oppList){
        Set<Id> accountIds = new Set<Id>();
        Map<Id,Account> accountMap = new Map<Id,Account>();
        for(Opportunity opp : oppList) if(opp.AccountId != null) accountIds.add(opp.AccountId);
        if(accountIds.size() > 0)                
            accountMap = new Map<Id,Account>([select Id, Do_Not_Survey__c, Survey_Sent_Date__c from Account where Id IN: accountIds]);
        return accountMap;        
    }
    private Map<Id,Contact> getContactMap(List<Opportunity> oppList){
        Set<Id> contactIds = new Set<Id>();
        Map<Id,Contact> contactMap = new Map<Id,Contact>();
        for(Opportunity o : oppList) if(o.Survey_Contact__c != null) contactIds.add(o.Survey_Contact__c);
        if(contactIds.size() > 0)                
            contactMap = new Map<Id,Contact>([select Id, Do_Not_Survey__c,Last_Survey_Sent_Date__c from Contact where Id IN: contactIds]);
        return contactMap;        
    }
     private Map<Id,String> getOppOwnerProfileName(List<Opportunity> oppList){
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : oppList){
          if(opp.OwnerId != null) oppIds.add(opp.Id); 
        }
        Map<Id,String> profileMap = new Map<Id,String>();
        if(oppIds.size() > 0) {
            List<Opportunity> oppProfiles = [select id, Owner.profileId from Opportunity where id IN :oppIds];
            for(Opportunity opp : oppProfiles) profileMap.put(opp.Id,opp.Owner.profileId);
        }
        return profileMap; 
    }
    private Map<Id, sObject> getRecordTypeEligibleMap(List<Opportunity> oppList){
        
        Map<Id,sObject> recordTypeEligibleMap = null;
        SObject so = Schema.getGlobalDescribe().get('Opportunity').newSObject();
        
        if(so.getSobjectType().getDescribe().fields.getMap().containsKey('RecordTypeId')){
            string theQuery = 'select Id, RecordTypeId from Opportunity WHERE Id IN';
            Set<Id> oppIds = new Set<Id>();
            for(Opportunity o: oppList){
                oppIds.add(o.id);
            }        
            if(oppIds.size() > 0){
                theQuery += ':oppIds';
                recordTypeEligibleMap = new Map<Id,sObject>((List<Opportunity>)Database.query(theQuery));
            }
        }
        return recordTypeEligibleMap;
    }
    public Boolean personAccountsEnabled(){
        try{
            sObject testObject = new Account();
            testObject.get( 'isPersonAccount' );
            return true;
        }
        catch( Exception ex ){
            return false;
        }
    }
}