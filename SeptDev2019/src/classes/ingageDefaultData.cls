public class ingageDefaultData {
	
	
	public static void insertDefaultQuestions(){
		
		integer qsCount = [select count() from Question_Set__c where name = 'In-Gage Service Question Set' and Active__c = true];
		
		if(qsCount ==0){
		
			Question_Set__c qs = new Question_Set__c(name = 'In-Gage Service Question Set', Active__c = true);
			insert qs;
			
			list<Question_Section__c> sections = new list<Question_Section__c>();
			sections.add(new Question_Section__c(Section_Name__c='Begin with Enthusiasm and Listen',Section_Number__c='1',Question_Set__c = qs.id));
			sections.add(new Question_Section__c(Section_Name__c='Clarify and Promise',Section_Number__c='2',Question_Set__c = qs.id));
			sections.add(new Question_Section__c(Section_Name__c='Do It',Section_Number__c='3',Question_Set__c = qs.id));
			sections.add(new Question_Section__c(Section_Name__c='Confirm',Section_Number__c='4',Question_Set__c = qs.id));
			insert sections;
			
			list<Question__c> questions = new list<Question__c>();
			questions.add(new Question__c(Question__c='Naturally Energetic',Scoring__c= 10,Question_Number__c = 1, Question_Section__c=sections[0].id));
			questions.add(new Question__c(Question__c='Name Given and Company Name Used',Scoring__c= 5,Question_Number__c = 2, Question_Section__c=sections[0].id));
			questions.add(new Question__c(Question__c='Active Listening',Scoring__c= 5,Question_Number__c = 3, Question_Section__c=sections[0].id));
			questions.add(new Question__c(Question__c='Understanding the Customers Concern',Scoring__c= 5,Question_Number__c = 1, Question_Section__c=sections[1].id));
			questions.add(new Question__c(Question__c='Get the Customers Name and Use It',Scoring__c= 5,Question_Number__c = 2, Question_Section__c=sections[1].id));
			questions.add(new Question__c(Question__c='Advise Actions for Resolution & Time',Scoring__c= 10,Question_Number__c = 3, Question_Section__c=sections[1].id));
			questions.add(new Question__c(Question__c='Trouble Shoot & Get Best Resolution',Scoring__c= 5,Question_Number__c = 1, Question_Section__c=sections[2].id));
			questions.add(new Question__c(Question__c='Build Emotional Connection',Scoring__c= 10,Question_Number__c = 2, Question_Section__c=sections[2].id));
			questions.add(new Question__c(Question__c='Res Given/Alt solution within SLA&Pro',Scoring__c= 5,Question_Number__c = 3, Question_Section__c=sections[2].id));
			questions.add(new Question__c(Question__c='Doc of Fault&Customer Interact',Scoring__c= 10,Question_Number__c = 4, Question_Section__c=sections[2].id));
			questions.add(new Question__c(Question__c='If Call Back Offered, Did You Do It?',Scoring__c= 10,Question_Number__c = 5, Question_Section__c=sections[2].id));
			questions.add(new Question__c(Question__c='Repeat Back Solution',Scoring__c= 5,Question_Number__c = 1, Question_Section__c=sections[3].id));
			questions.add(new Question__c(Question__c='Anything Else if Applic&Cover issues',Scoring__c= 5,Question_Number__c = 2, Question_Section__c=sections[3].id));
			questions.add(new Question__c(Question__c='Inspirational Close',Scoring__c= 10,Question_Number__c = 3, Question_Section__c=sections[3].id));
			insert questions;
		}
		
	}
		
	public static void insertDefaultCustomSettingValues(){
		ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
		setting.Loyal_Customer__c = 7;
		setting.Detractor_Impact__c = 2;
		setting.Company_Name__c = userInfo.getOrganizationName();
		setting.Survey_Site_Background_Image_URL__c= GetResourceURL('InGage_Resources')+'/sites/images/in-gageSurvey.jpg';
		upsert setting;
	}
	
	public static String getResourceURL(String resourceName){
	    List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];
	    if(resourceList.size() == 1){
	       String namespace = resourceList[0].NamespacePrefix;
	       return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName; 
	    }
	    else return '';
	}
	
	public static void insertNPStypeRecords(){
		list<NPS_Type__c> types = new list<NPS_Type__c>();
		types.add(new NPS_Type__c(name = 'Service'));
		types.add(new NPS_Type__c(name = 'Sales'));
		insert types;
	}
	
	public static void insertDefaultSurveys(){
		
		ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
        if(FeatureConsoleAPI.npsEnabled()){
            setting.Customer_NPS_Survey_ID__c = defaultNPSsurvey();
			setting.Opportunity_Won_Survey__c = defaultWonOpportunitySurvey(); 
        }
        if(FeatureConsoleAPI.eeEnabled()) setting.Employee_Engagement_Survey__c = defaultEngagementSurvey();

		upsert setting;
	}
	
	private static id defaultEngagementSurvey(){

		Survey__c sv = new Survey__c(
		    Name = 'Employee Engagement Survey',
		    Type__c = 'Employee Engagement',
		    Survey_Container_CSS__c = '#survey_container{ margin-left: auto; margin-right: auto; width: 95% ; box-shadow: 0 0 14px #3366ff; -moz-box-shadow: 0 0 14px #3366ff; -webkit-box-shadow: 0 0 14px #3366ff; } .buttonStyle{width: 25%; }',
		    Hide_Survey_Name__c = true,
            Language__c = 'English',
		    URL__c = 'Undefined'    
		);
		sv.Submit_Response__c = 'Thank you for completing the '+sv.name+'.';
		insert sv;
		
		list<Survey_Question__c> newQuestions = new list<Survey_Question__c>();
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =0, 
		    Question__c = 'I know very clearly what is expected of me in my role',
		    Name = 'I know very clearly what is expected of me in my role',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =1, 
		    Question__c = 'My manager cares about me as a person',
		    Name = 'My manager cares about me as a person',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =2, 
		    Question__c = 'I receive praise or recognition weekly',
		    Name = 'I receive praise or recognition weekly',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id 
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =3, 
		    Question__c = 'My manager cares about my development at work',
		    Name = 'My manager cares about my development at work',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =4, 
		    Question__c = 'My team mates seem engaged and motivated',
		    Name = 'My team mates seem engaged and motivated',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id 
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =5, 
		    Question__c = 'I relate to the purpose vision and values of our company',
		    Name = 'I relate to the purpose vision and values of our company',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =6, 
		    Question__c = 'I would recommend <<company>> as a great place to work',
		    Name = 'I would recommend my employer as a great place to work',
		    Choices__c = '1\n2\n3\n4\n5',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(1= Strongly disagree , 5= Strongly agree)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =7, 
		    Question__c = 'What is the best thing that has happened to you at work this month?',
		    Name = 'What is the best thing that has happened to you at work this month?',
		    Type__c = 'Free Text',
		    Required__c = true,
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =8, 
		    Question__c = 'Any comments you would like to add?',
		    Name = 'Any comments you would like to add?',
		    Type__c = 'Free Text',
		    Survey__c  = sv.id
		));
		
		insert newQuestions;
		
		return sv.id;
	}

	private static id defaultNPSsurvey(){

		Survey__c sv = new Survey__c(
		    Name = 'NPS Survey',
		    Type__c = 'Net Promoter Score',
		    Survey_Container_CSS__c = '#survey_container{ margin-left: auto; margin-right: auto; width: 95% ; box-shadow: 0 0 14px #3366ff; -moz-box-shadow: 0 0 14px #3366ff; -webkit-box-shadow: 0 0 14px #3366ff; } .buttonStyle{width: 25%; }',
		    Hide_Survey_Name__c = true,
            Language__c = 'English',
		    URL__c = 'Undefined'    
		);
		sv.Submit_Response__c = 'Thank you for completing our survey and helping us to improve the way we work with our customers.';
		insert sv;
		
		list<Survey_Question__c> newQuestions = new list<Survey_Question__c>();
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =0, 
		    Question__c = 'How likely are you to recommend <<company>> to a friend or family member?',
		    Name = 'How likely are you to recommend us to a friend or family member?',
		    Choices__c = '0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(0= Extremely unlikely , 10= Extremely likely)',
            NPS_Field_Map__c = 'Answer_1__c',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =1, 
		    Question__c = 'How likely are you to continue to use <<company>>?',
		    Name = 'How likely are you to continue to use us?',
		    Choices__c = '0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		   	Guidance__c = '(0= Extremely unlikely , 10= Extremely likely)',
            NPS_Field_Map__c = 'Answer_2__c',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =2, 
		    Question__c = 'Any feedback you would like to leave us?',
		    Name = 'Any feedback you would like to leave us?',
		    Type__c = 'Free Text',
            NPS_Field_Map__c ='Feedback_Comment__c',
		    Survey__c  = sv.id
		));
		
		insert newQuestions;
		
		return sv.id;
	}

	private static id defaultWonOpportunitySurvey(){

		Survey__c sv = new Survey__c(
		    Name = 'Won Opportunity Survey',
		    Type__c = 'Won Opportunity',
		    Survey_Container_CSS__c = '#survey_container{ margin-left: auto; margin-right: auto; width: 95% ; box-shadow: 0 0 14px #3366ff; -moz-box-shadow: 0 0 14px #3366ff; -webkit-box-shadow: 0 0 14px #3366ff; } .buttonStyle{width: 25%; }',
		    Hide_Survey_Name__c = true,
            Language__c = 'English',
		    URL__c = 'Undefined'    
		);
		sv.Submit_Response__c = 'Thank you for completing our survey and helping us to understand more about our customers.';
		//sv.Submit_Response__c = 'Thank you for completing our survey and helping us to improve the way we work with our customers.';
		insert sv;
		
		list<Survey_Question__c> newQuestions = new list<Survey_Question__c>();
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =0, 
		    Question__c = 'How did you hear about <<company>>?',
		    Name = 'How did you hear about us',
		    Choices__c = 'Internet search (e.g. Google or Bing)\nFriends or Family recommendation\nSocial media (e.g. Facebook or twitter)\nTV or Radio\nInternet article (e.g. News or blogs)',
		    Type__c = 'Single Select--Vertical',
		    Required__c = true,
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =1, 
		    Question__c = 'How likely are you to recommend <<company>> to a friend or family member?',
		    Name = 'How likely are you to recommend us to a friend or family member?',
		    Choices__c = '0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n10',
		    Type__c = 'Single Select--Horizontal',
		    Required__c = true,
		    Guidance__c = '(0= Very Unlikely , 10= Very Likely)',
		    Survey__c  = sv.id
		));
		
		newQuestions.add(new Survey_Question__c(
		    OrderNumber__c =2, 
		    Question__c = 'Any feedback you would like to leave us?',
		    Name = 'Any feedback you would like to leave us?',
		    Type__c = 'Free Text',
		    Survey__c  = sv.id
		));
		
		insert newQuestions;
		
		return sv.id;
	}
    
    public static void insertDefaultWallboardReports(){
        set<string> wallboardName = new set<string>{'FCR Wallboard','NPS Wallboard','Quality Wallboard'};
        integer wbCount = [select count() from Wallboard__c where Name in: wallboardName];
        system.debug('wbCount '+ wbCount);
        if(wbCount==0){
            //check with Rod
            /*Employee__c emp = new Employee__c(First_Name__c = 'test', Last_Name__c = 'employee',User_Record__c = userInfo.getUserId());
            insert emp;*/
            
            list<Wallboard__c> wallboards = new list<Wallboard__c>();
            Wallboard__c fcrWB = new Wallboard__c(name ='FCR Wallboard');
            Wallboard__c npsWB = new Wallboard__c(name ='NPS Wallboard');
            Wallboard__c quaWB = new Wallboard__c(name ='Quality Wallboard');
            wallboards.add(fcrWB);
            wallboards.add(npsWB);
            wallboards.add(quaWB);
            insert wallboards;
            
            list<Wallboard_Report__c> wallboardReports = new list<Wallboard_Report__c>{
                new Wallboard_Report__c(
                    Name='First Case Resolution, Last 30 Days',
                    Sort_Values__c = null,
                    Top_x__c = null,
                    Gauge_Low_Range__c = 25,
                    Gauge_High_Range__c = 75,
                    SOQL__c = 'ClosedDate = last_n_days:30',
                    Display_Order__c = '1',
                    Wallboard__c = fcrWB.id
                ),
                    new Wallboard_Report__c(
                        Name='FCR By Manager, Last 30 Days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = null,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'First_Agent__c != null AND Employee__c != null AND Employee__r.Manager_Employee__c != null AND ClosedDate = last_n_days:30 AND Employee__r.x30_Day_Case_Count__c > 50',
                        Display_Order__c = '2',
                        Wallboard__c = fcrWB.id
                    ),
                    new Wallboard_Report__c(
                        Name='Top 10 FCR Performers, Last 30 Days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = 10,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'First_Agent__c != null AND Employee__c != null AND ClosedDate = last_n_days:30 AND Employee__r.x30_Day_Case_Count__c > 50',
                        Display_Order__c = '3',
                        Wallboard__c = fcrWB.id
                    ),
                    
                    new Wallboard_Report__c(
                        Name='Customer NPS Last 7 Days',
                        Sort_Values__c = null,
                        Top_x__c = null,
                        Gauge_Low_Range__c = 20,
                        Gauge_High_Range__c = 50,
                        SOQL__c = 'Completed_Date__c = last_n_days:7 AND Employee__r.Has_In_Gage_Licence__c = true',
                        Display_Order__c = '1',
                        Wallboard__c = npsWB.id
                    ),
                    new Wallboard_Report__c(
                        Name='NPS by Manager Last 30 Days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = null,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'Completed_Date__c = last_n_days:30 AND Employee__r.Has_In_Gage_Licence__c = true AND Employee__r.Manager_Employee__c != null AND Employee__r.Surveys_in_Last_30_Days__c > 10',
                        Display_Order__c = '2',
                        Wallboard__c = npsWB.id
                    ),
                    new Wallboard_Report__c(
                        Name='Top 10 NPS by Individual Last 30 days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = 10,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'Completed_Date__c = last_n_days:30 AND Employee__r.Has_In_Gage_Licence__c = true AND Employee__r.Surveys_in_Last_30_Days__c > 10',
                        Display_Order__c = '3',
                        Wallboard__c = npsWB.id
                    ),
                    
                    new Wallboard_Report__c(
                        Name='Quality Score last 30 days',
                        Sort_Values__c = null,
                        Top_x__c = null,
                        Gauge_Low_Range__c = 60,
                        Gauge_High_Range__c = 75,
                        SOQL__c = 'Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:30',
                        Display_Order__c = '1',
                        Wallboard__c = quaWB.id
                    ),
                    new Wallboard_Report__c(
                        Name='Quality Assessment Score by Manager, Last 30 Days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = null,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:30 AND Agent__r.Manager_Employee__c != null',
                        Display_Order__c = '2',
                        Wallboard__c = quaWB.id
                    ),
                    new Wallboard_Report__c(
                        Name='Quality Scores by Individuals, Last 90 Days',
                        Sort_Values__c = 'Descending',
                        Top_x__c = null,
                        Gauge_Low_Range__c = null,
                        Gauge_High_Range__c = null,
                        SOQL__c = 'Submit_Form__c = true AND Submitted_Form_Date__c = last_n_days:90',
                        Display_Order__c = '3',
                        Wallboard__c = quaWB.id
                    )
                    
                    };
                    insert wallboardReports; 
        }
    }
    
    
    
}