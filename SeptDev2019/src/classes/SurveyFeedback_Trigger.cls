public with sharing class SurveyFeedback_Trigger extends TriggerHandler
{
     // *************************************
    // ******* Public Declarations *********
    // *************************************



    // ******************************
    // ******* Constructor **********
    // ******************************
    public SurveyFeedback_Trigger()
    {
    }




    // **********************************
    // ******* Before Insert *************
    // **********************************
    public override void beforeInsert(List<SObject> newObjects)
    {
    }

    // **********************************
    // ******* Before Update *************
    // **********************************
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap)
    {
    }

    // **********************************
    // ******* Before Delete *************
    // **********************************
    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap)
    {
    }

    // **********************************
    // ******* After Insert *************
    // **********************************
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap)
    {
        CloneRecordsToManagement(newobjects);
    }


    // **********************************
    // ******* After Update *************
    // **********************************
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap)
    {
    }

    // **********************************
    // ******* After Delete *************
    // **********************************
    public override void afterDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap){
    }

    // **********************************
    // ******* After UnDelete ***********
    // **********************************
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap)
    {
    }

    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private void CloneRecordsToManagement(list<sobject> NewObjects)
    {
    	system.debug('CloneRecordsToManagement');
        CheckRecursionsDuringTestCoverage = true;
        if (!IsMethodAllowedToRecurse('CloneRecordsToManagement',1)) return; // we don't want to clone the clones.
		

        map<id,user> ManagerMap = getManagerMapData();
        map<id,Employee__c> EmployeeMap = getEmployeeMapData(false);
        map<id,Employee__c> EmployeeByUserMap = getEmployeeMapData(true);
        list<Employee_Survey_Anonymous__c> NewRecordsToInsert = new list<Employee_Survey_Anonymous__c>();
        list<Employee_Feedback_Anonymous__c> anonymousFeedback = new list<Employee_Feedback_Anonymous__c>();


        for(sobject so:newobjects)
        {
            user Manager ;
            user ManagersManager;
            user UserRecord;
            Engagement_Survey_Feedback__c egf = (Engagement_Survey_Feedback__c)so;
            system.debug('EnagementSurveyFeedback = '+ egf);
            Employee__c employee = EmployeeMap.get(egf.Employee_Lookup__c);

            if (Employee.User_Record__c != null) userRecord = ManagerMap.get(Employee.User_Record__c);
            if (UserRecord.ManagerID != null) Manager = ManagerMap.get(userRecord.managerID);
            if (Manager != null && Manager.ManagerID != null) ManagersManager = Managermap.get(Manager.managerid);
            system.debug('userRecord = '+userRecord);
            system.debug('manager = '+manager);
            system.debug('managersmanager = '+managersmanager);
            if (Manager != null)
            {// this block will create new record in employee survey anonymous
                employee__c ManagerEmployee = EmployeeByUserMap.get(Manager.id);
                Employee_Survey_Anonymous__c NewEGF = (Employee_Survey_Anonymous__c) CopyThisEGF(egf,false,true); // we are no longer using CLONE as it left the Previous owner in place.
                //newegf.Employee_Lookup__c =ManagerEmployee.id;
                if(ManagerEmployee != null) newegf.Related_Manager__c = ManagerEmployee.id;
                newegf.ownerid = manager.id; //- put this back in once the record is not an master detail
                newegf.Started_Date__c = egf.Started_Date__c;
                NewRecordsToInsert.add(NewEgf);
            }
            if (ManagersManager != null)
            {// this block saved new records in employee feedback anonymous
                employee__c ManagersManagerEmployee = EmployeeByUserMap.get(ManagersManager.id);
                employee__c ManagerEmployee = EmployeeByUserMap.get(Manager.id);
                Employee_Feedback_Anonymous__c NewEGF = (Employee_Feedback_Anonymous__c) CopyThisEGF(Egf,true,false); // we are no longer using CLONE as it left the Previous owner in place.
                if(ManagerEmployee != null) newegf.Related_Manager__c = ManagerEmployee.id;
                newegf.ownerid = managersmanager.id; // put this back in once the record is no longer a master detail
                newegf.Started_Date__c = egf.Started_Date__c;
                anonymousFeedback.add(NewEgf);
            }

        }
		system.debug('NewRecordsToInsert' + JSON.serializePretty(NewRecordsToInsert));
		system.debug('anonymousFeedback = ' + JSON.serializePretty(anonymousFeedback));
        if (NewRecordsToInsert.size()>0) database.insert(NewRecordsToInsert,false);
        if(anonymousFeedback.size()>0) database.insert(anonymousFeedback,false);
    }

    private sObject CopyThisEGF(Engagement_Survey_Feedback__c oldegf, boolean CloneComments, boolean CloneScore)
    {
        Employee_Survey_Anonymous__c newegf;
        Employee_Feedback_Anonymous__c newFeedback;
        sObject returnsObject;
        if (CloneScore)
        {
            newegf = new Employee_Survey_Anonymous__c();

            newegf.Question_1_1__c = oldegf.Question_1_1__c;
            newegf.Question_2__c = oldegf.Question_2__c;
            newegf.Question_3__c = oldegf.Question_3__c;
            newegf.Question_4__c = oldegf.Question_4__c;
            newegf.Question_5__c = oldegf.Question_5__c;
            newegf.Question_6__c = oldegf.Question_6__c;
            newegf.Question_7__c = oldegf.Question_7__c;

            newegf.Completion_Status__c = oldegf.Completion_Status__c;
            newegf.Contact__c = oldegf.Contact__c;
            newegf.Related_Manager__c = oldegf.Related_Manager__c;
            newegf.Started_Date__c = oldegf.Started_Date__c;
            returnsObject=newegf;

        }
        else if(CloneComments){
           newFeedback=new Employee_Feedback_Anonymous__c();
           newFeedback.Additional_Comments__c = oldegf.Additional_Comments_1__c;
           newFeedback.Question_8_1__c = oldegf.Question_8_1__c;
           newFeedback.Related_Manager__c=oldegf.Related_Manager__c;
           returnsObject=newFeedback;

        }

        // redundant fields -- by AuR
        //newegf.Employee_Lookup__c = oldegf.Employee_Lookup__c;
        //newegf.Device_Type__c = oldegf.Device_Type__c;
        //newegf.Browser__c = oldegf.Browser__c;

        return returnsObject;

    }

    // *********************************************************************************************************************************************************************************************
    // *************************************Utility Methods                 ************************************************************************************************************************
    // *********************************************************************************************************************************************************************************************
    private map<id,user> GetManagerMapData()
    {
        return new map<id,user>([select id,managerid from user where isactive=true]);
    }

    private map<id,Employee__c> GetEmployeeMapData(boolean ByUserID)
    {
        map<id,Employee__c> retMap = new map<id,Employee__c>();
        list<employee__c> emplist = [select id,User_Record__c from Employee__c where User_Record__r.IsActive = true];
        for(employee__c e:emplist)
        {
            if (!ByUserID)
            {
                retmap.put(e.id,e);
            }
            else
            {
                retmap.put(e.User_Record__c,e);
            }
        }
         return retmap;

    }


}