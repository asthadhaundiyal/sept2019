/**
 * Test Class for Email_Trigger
 */
@isTest
private class EmailMessage_Trigger_T {

    /**
     *
     */
	static testMethod void testAI() {
        
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            system.debug('qa se enabled: '+ FeatureConsoleAPI.qaSEWithAIEnabled());
            
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c = false);
            insert setting;
        
            Employee__c emp = new Employee__c();
        	emp.First_Name__c = 'Test23';
        	insert emp;
            
            Account a = new account(name = 'test23', Language__c='English');
			insert a;
			Contact con = new contact(lastname ='Test23', email ='test23.test@test.com', accountId = a.id);
			insert con;
        	Case c = new Case(AccountId = a.id, contactId = con.id, subject = 'test32', type='Other', Status = 'New', Description='test test', in_gage__AI_Language__c ='English');
        	c.in_gage__Employee__c = emp.id;
            insert c;
            EmailMessage e1 = new EmailMessage(TextBody='Your account is unlocked and fully functional now', Incoming=false, ParentId = c.Id);
        	
            Test.startTest();
            insert e1;
            
            update e1;
        	
        	delete e1;
        	
        	undelete e1;
            Test.stopTest();
        }
            
    }
}