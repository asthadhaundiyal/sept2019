@isTest(SeeAllData = true)
public with sharing class Sch_NPSchatterPost_T {
	
	
	private static void setupData(){
	
      	User u =[SELECT ID from User where Profile.Name='System Administrator' and isActive = true limit 1];
		
		Employee__c emp = new Employee__c(First_Name__c = 'Test', User_Record__c=u.id, ownerId = u.id, Has_In_Gage_Licence__c = true);
		insert emp;
		
		case c = new Case(Status = 'Closed', in_gage__First_Employee__c = emp.id, ownerId = u.id);
		insert c;
		
		NPS__c nps = new NPS__c(
			Employee__c = emp.id,
			Answer_1__c = 10,
			case__c = c.id);
		insert nps;
		
	}
	
	
	private static testmethod void testScheduledJob(){
		
		setupData();
		
		// schedule the test job
		/*datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';*/
        String schedule = '0 0 0,6,12,18 ? * *';
		String jobId = system.schedule('testSch_NPSchatterPost', schedule ,  new Sch_NPSchatterPost());
		
		test.startTest();
		Sch_NPSchatterPost.rescheduleJob();
		test.stopTest(); // runs the @future method
		
	}
	
}