global class AI_Batch_CreateQAF implements Database.Batchable<sObject>{
    // called in batch size of 1
    
    global final String query;
    global final set<id> caseIds;

	//real time call
    global AI_Batch_CreateQAF(set<id> caseIds){
        this.caseIds = caseIds;
        query = getCaseQuery() + ' where id in :caseIds';
    }
	//batch call
    global AI_Batch_CreateQAF(string query){
       this.query = query;
    }
    
	private string getCaseQuery(){
        return 'select id, IsClosed, Employee__c, (select id from Quality_Assessment_Forms__r) from Case'; 
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Case> caseList = (List<Case>)scope;    
        Set<id> caseIds = new Set<id>();
        boolean qafExists = false;
        for(Case c :caseList){           
            if(c.IsClosed) caseIds.add(c.Id);
        }
        if(!caseIds.isEmpty()){        
        	//create QAF form
       		EventHandler.createAIQAF(caseList);
        }
               
    }
    
    global void finish(Database.BatchableContext BC){} 
}