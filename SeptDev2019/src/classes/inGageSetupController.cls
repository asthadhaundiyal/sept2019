public without sharing class inGageSetupController {

        // INSTALLATION RECORDS
        public void createChatterGroup() {
            list<CollaborationGroup> cgCheck = [select id from CollaborationGroup where Name = 'Legendary Customer Service' and CollaborationType='Public'];
            if(cgCheck.isEmpty()){
                id owner = Userinfo.getUserId();
                CollaborationGroup chatterGroup = new CollaborationGroup(Name = 'Legendary Customer Service', CollaborationType='Public', OwnerId = owner);
                insert chatterGroup;
            }            
        }
        
        public void populateEmployeeTable(){
            IngagePostInstallation ipi = new IngagePostInstallation();
            ipi.populateEmployeeTable();
        }
        public void restoreSurveySiteImage(){
            ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
            setting.Survey_Site_Background_Image_URL__c= ingageDefaultData.getResourceURL('InGage_Resources')+'/sites/images/in-gageSurvey.jpg';
            upsert setting;
        }
        
        public void installDefaultQAFquestions(){
            // already checks to see if records exist
            ingageDefaultData.insertDefaultQuestions();
        }
        
        public void installDefaultSurveyQuestions(){
        	set<string> surveyNames = new set<string>{'Employee Engagement Survey','NPS Survey','Won Opportunity Survey'};
        	set<string> types = new set<string>{'Employee Engagement','Net Promoter Score','Won Opportunity'};
        	list<Survey__c> surveyList = [select id from Survey__c where Name in: surveyNames OR Type__c in: types];
            if(surveyList.size()!=3) ingageDefaultData.insertDefaultSurveys();        
        }
        public void installDefaultWallboardReports(){
            //checks to see if records exist
            ingageDefaultData.insertDefaultWallboardReports();
        }
		/*public void createSendSurveyCustomField(){
            
                MetadataService.CustomField caseCustomField = new MetadataService.CustomField();
                caseCustomField.fullName = 'Case.Is_Survey_Eligible_To_Send__c';
                caseCustomField.externalId = false;
                caseCustomField.formula = 'true';
                caseCustomField.formulaTreatBlanksAs = 'BlankAsZero';
                caseCustomField.label = 'Is Survey Eligible To Send';
                caseCustomField.required = false;
                caseCustomField.type_x = 'Checkbox';
                caseCustomField.unique = false;
            
                MetadataService.CustomField oppCustomField = new MetadataService.CustomField();
                oppCustomField.fullName = 'Opportunity.Is_Survey_Eligible_To_Send__c';
                oppCustomField.externalId = false;
                oppCustomField.formula = 'true';
                oppCustomField.formulaTreatBlanksAs = 'BlankAsZero';
                oppCustomField.label = 'Is Survey Eligible To Send';
                oppCustomField.required = false;
                oppCustomField.type_x = 'Checkbox';
                oppCustomField.unique = false;
            
                MetadataService.MetadataPort service = createService();
                List<MetadataService.SaveResult> results =
                    service.createMetadata(
                        new MetadataService.Metadata[] { caseCustomField, oppCustomField});
                handleSaveResults(results[0]);
            }
    
            public static MetadataService.MetadataPort createService()
            { 
                MetadataService.MetadataPort service = new MetadataService.MetadataPort();
                service.SessionHeader = new MetadataService.SessionHeader_element();
                service.SessionHeader.sessionId = UserInfo.getSessionId();
                return service;		
        }
    
    public static void handleSaveResults(MetadataService.SaveResult saveResult)
    {
        // Nothing to see?
        if(saveResult==null || saveResult.success)
            return;
        // Construct error message and throw an exception
        if(saveResult.errors!=null)
        {
            List<String> messages = new List<String>();
            messages.add(
                (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') +
                    'occured processing component ' + saveResult.fullName + '.');
            for(MetadataService.Error error : saveResult.errors)
                messages.add(
                    error.message + ' (' + error.statusCode + ').' +
                    ( error.fields!=null && error.fields.size()>0 ?
                        ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
            if(messages.size()>0)
                throw new MetadataServiceCustomFieldException(String.join(messages, ' '));
        }
        if(!saveResult.success)
            throw new MetadataServiceCustomFieldException('Request failed with no specified error.');
    }*/


        // TEST RECORDS
        public void createTestRecords(){
                //IngagePostInstallation.populateEmployeeTable();
        }


}