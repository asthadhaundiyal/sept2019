global class FeatureConsoleAPI {
    //For NPS Feature
    global static void enableNPS() {
        if (!FeatureManagement.checkPackageBooleanValue('NPS_Feature_Activated')) {
            throw new FeatureAccessException('NPS feature not currently licensed');
        }
        //FeatureManagement.changeProtection('NPS__c', 'CustomObject', 'Unprotected');
        //FeatureManagement.changeProtection('NPS_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('NPS_Feature_Enabled', true);
    }
    
    global static void disableNPS() {
        FeatureManagement.setPackageBooleanValue('NPS_Feature_Enabled', false);
    }
    
    global static boolean npsEnabled() {
        system.debug('NPS enabled: '+ FeatureManagement.checkPackageBooleanValue('NPS_Feature_Enabled'));
        system.debug('NPS activated: '+ FeatureManagement.checkPackageBooleanValue('NPS_Feature_Activated'));
        return FeatureManagement.checkPackageBooleanValue('NPS_Feature_Enabled') && FeatureManagement.checkPackageBooleanValue('NPS_Feature_Activated');
        //return true;
    }
    
    //For QA Feature
    global static void enableQA() {
        if (!FeatureManagement.checkPackageBooleanValue('QA_Feature_Activated')) {
            throw new FeatureAccessException('QA feature not currently licensed');
        }
        //FeatureManagement.changeProtection('NPS_AI__c', 'CustomObject', 'Unprotected');
        //FeatureManagement.changeProtection('NPS_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('QA_Feature_Enabled', true);
    }
    
    global static void disableQA() {
        FeatureManagement.setPackageBooleanValue('QA_Feature_Enabled', false);
    }
    
    global static boolean qaEnabled() {
        system.debug('QA enabled: '+ FeatureManagement.checkPackageBooleanValue('QA_Feature_Enabled'));
        system.debug('QA activated: '+ FeatureManagement.checkPackageBooleanValue('QA_Feature_Activated'));
        return FeatureManagement.checkPackageBooleanValue('QA_Feature_Enabled') && FeatureManagement.checkPackageBooleanValue('QA_Feature_Activated');
        //return true;
    }
    
    //For FCR Feature
    global static void enableFCR() {
        if (!FeatureManagement.checkPackageBooleanValue('FCR_Feature_Activated')) {
            throw new FeatureAccessException('FCR feature not currently licensed');
        }
        //FeatureManagement.changeProtection('NPS_AI__c', 'CustomObject', 'Unprotected');
        //FeatureManagement.changeProtection('NPS_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Enabled', true);
    }
    
    global static void disableFCR() {
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Enabled', false);
    }
    
    global static boolean fcrEnabled() {
        system.debug('FCR enabled: '+ FeatureManagement.checkPackageBooleanValue('FCR_Feature_Enabled'));
        system.debug('FCR activated: '+ FeatureManagement.checkPackageBooleanValue('FCR_Feature_Activated'));
        return FeatureManagement.checkPackageBooleanValue('FCR_Feature_Enabled') && FeatureManagement.checkPackageBooleanValue('FCR_Feature_Activated');
        //return true;
    }
    
    //For EE Feature
    global static void enableEE() {
        if (!FeatureManagement.checkPackageBooleanValue('EE_Feature_Activated')) {
            throw new FeatureAccessException('EE feature not currently licensed');
        }
        //FeatureManagement.changeProtection('NPS_AI__c', 'CustomObject', 'Unprotected');
        //FeatureManagement.changeProtection('NPS_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('EE_Feature_Enabled', true);
    }
    
    global static void disableEE() {
        FeatureManagement.setPackageBooleanValue('EE_Feature_Enabled', false);
    }
    
    global static boolean eeEnabled() {
        system.debug('EE enabled: '+ FeatureManagement.checkPackageBooleanValue('EE_Feature_Enabled'));
        system.debug('EE activated: '+ FeatureManagement.checkPackageBooleanValue('EE_Feature_Activated'));
        return FeatureManagement.checkPackageBooleanValue('EE_Feature_Enabled') && FeatureManagement.checkPackageBooleanValue('EE_Feature_Activated');
        //return true;
    }
    
    //For NPS AI Feature
    global static void enableNPSwithAI() {
        if (!FeatureManagement.checkPackageBooleanValue('NPS_with_AI_Activated')) {
            throw new FeatureAccessException('NPS with AI feature not currently licensed');
        }
        FeatureManagement.changeProtection('NPS_AI__c', 'CustomObject', 'Unprotected');
        FeatureManagement.changeProtection('NPS_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Enabled', true);
    }
    
    global static void disableNPSWithAI() {
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Enabled', false);
    }
    
    global static boolean npsWithAIEnabled() {
        system.debug('NPS AI enabled: '+ FeatureManagement.checkPackageBooleanValue('NPS_with_AI_Enabled'));
        system.debug('NPS AI activated: '+ FeatureManagement.checkPackageBooleanValue('NPS_with_AI_Activated'));
        return FeatureManagement.checkPackageBooleanValue('NPS_with_AI_Enabled') && FeatureManagement.checkPackageBooleanValue('NPS_with_AI_Activated');
        //return true;
    }
    
    //For QA CC Feature
    global static void enableQAWithAI() {
        if (!FeatureManagement.checkPackageBooleanValue('QA_with_AI_Activated')) {
            throw new FeatureAccessException('AI Case Categorisation feature not currently licensed');
        }
        FeatureManagement.changeProtection('QA_AI__c', 'CustomObject', 'Unprotected');
        FeatureManagement.changeProtection('QA_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Enabled', true);
    }
    
    global static void disableQAWithAI() {
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Enabled', false);
    }
    
    global static boolean qaWithAIEnabled() {
        system.debug('QA Case Categorisation enabled: '+ FeatureManagement.checkPackageBooleanValue('QA_with_AI_Enabled'));
        system.debug('QA Case Categorisation activated: '+ FeatureManagement.checkPackageBooleanValue('QA_with_AI_Activated'));
        return FeatureManagement.checkPackageBooleanValue('QA_with_AI_Enabled') && FeatureManagement.checkPackageBooleanValue('QA_with_AI_Activated');
        //return true;
    }
    
    //For QA SE Feature
    global static void enableQASEWithAI() {
        if (!FeatureManagement.checkPackageBooleanValue('QA_SE_with_AI_Activated')) {
            throw new FeatureAccessException('AI Sentiment & Emotion feature not currently licensed');
        }
        FeatureManagement.changeProtection('QA_AI__c', 'CustomObject', 'Unprotected');
        FeatureManagement.changeProtection('QA_AI_Feedback__c', 'CustomObject', 'Unprotected');
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Enabled', true);
    }
    
    global static void disableQASEWithAI() {
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Enabled', false);
    }
    
    global static boolean qaSEWithAIEnabled() {
        system.debug('QA Sentiment and Emotion enabled: '+ FeatureManagement.checkPackageBooleanValue('QA_SE_with_AI_Enabled'));
        system.debug('QA Sentiment and Emoiton activated: '+ FeatureManagement.checkPackageBooleanValue('QA_SE_with_AI_Activated'));
        return FeatureManagement.checkPackageBooleanValue('QA_SE_with_AI_Enabled') && FeatureManagement.checkPackageBooleanValue('QA_SE_with_AI_Activated');
        //return true;
    } 
    
    //For Case Language Auto Detect Feature
    global static void enableCaseLangAutoDetect() {
        if (!FeatureManagement.checkPackageBooleanValue('Case_Language_Auto_Detect_Activated')) {
            throw new FeatureAccessException('Case Language Auto Detect feature not currently licensed');
        }
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Enabled', true);
    }
    
    global static void disableCaseLangAutoDetect() {
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Enabled', false);
    }
    
    global static boolean caseLangAutoDetectEnabled() {
        system.debug('Case Language Auto Detect enabled: '+ FeatureManagement.checkPackageBooleanValue('Case_Language_Auto_Detect_Enabled'));
        system.debug('Case Language Auto Detect activated: '+ FeatureManagement.checkPackageBooleanValue('Case_Language_Auto_Detect_Activated'));
        return FeatureManagement.checkPackageBooleanValue('Case_Language_Auto_Detect_Enabled') && FeatureManagement.checkPackageBooleanValue('Case_Language_Auto_Detect_Activated');
        //return true;
    } 
}