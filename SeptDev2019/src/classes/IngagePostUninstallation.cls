global class IngagePostUninstallation implements UninstallHandler{
	global void onUninstall(UninstallContext context) {

		List<String> jobNames = new List<String>{
	'In-gage Employee Survey Schedulet'
};

List<CronTrigger> jobs = [
	SELECT
		Id,
		CronJobDetail.Name
	FROM
		CronTrigger
	WHERE CronJobDetail.Name IN :jobNames
];
if ( jobs != null && jobs.size() > 0 ) {
	for ( CronTrigger job : jobs ) {
		System.abortJob( job.Id );
		System.debug( 'Removed job ' + job.CronJobDetail.Name + ' with id: ' + job.Id );
	}
} else {
	System.debug( 'No active jobs found.' );
}

	}
}