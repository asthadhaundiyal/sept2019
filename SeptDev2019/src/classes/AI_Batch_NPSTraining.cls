global class AI_Batch_NPSTraining implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global final String query;
    global final set<id> npsFBIds;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();

	//for real time processing
    global AI_Batch_NPSTraining(set<id> npsFBIds){
        this.npsFBIds = npsFBIds;
        query = getNPSQuery() + ' where id in :npsFBIds and Added_for_Training__c = false';
    }
    
	//for batch processing
    global AI_Batch_NPSTraining(string query){
        this.query = query;
    }
    
	private string getNPSQuery(){
        return 'select id, Text_for_Training__c, Category__c, Language__c, Added_for_Training__c, NPS_Comp_Value__c, Case_Category__c from NPS_AI_Feedback__c';
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<NPS_AI_Feedback__c> npsAIList = (List<NPS_AI_Feedback__c >)scope;
        List<NPS_AI_Feedback__c> processedNPSAIList = new List<NPS_AI_Feedback__c>();
        for(NPS_AI_Feedback__c npsAI :npsAIList){
            
            String language = npsAI.Language__c;
            String textToTrain = npsAI.Text_for_Training__c;
            String npsCategory = npsAI.Category__c;
            String caseCategory = npsAI.Case_Category__c;
            String npsCompValue = String.valueOf((npsAI.NPS_Comp_Value__c).intValue());
            
            if(String.isNotBlank(textToTrain) && String.isNotBlank(npsCategory) && String.isNotBlank(language)){
                
                String aiLang = language.toLowerCase();
                String customerId = setting.AI_Customer_ID__c;
                HttpRequest req = new HttpRequest();
                String endPoint = 'https://ingage-ai.co.uk/api/nps/training/'+ aiLang + '/'+ customerId;
                system.debug('NPS Training EndPoint: '+ endPoint);
                req.setEndpoint(endPoint);
                req.setTimeout(120000);
                req.setMethod('POST');
                req.setHeader('Content-Type', 'application/json');
                req.setBody(stringToJSONforTraining(textToTrain, npsCategory, caseCategory, npsCompValue));
                Http http = new Http();
                HTTPResponse res = http.send(req);
                if (res.getStatusCode() != 200) {
                    System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                }else {
                    System.debug('The training data was added successfully');
                    npsAI.Added_for_Training__c = true;
                    processedNPSAIList.add(npsAI);
                }   
            }
        }
        if(!processedNPSAIList.isEmpty()) update processedNPSAIList;
    }
    
   	private static String stringToJSONforTraining(String content, String category, String caseCategory, String promoterValue) {
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', content);
        generator.writeStringField('category', category);
        if(caseCategory!=null) generator.writeStringField('type', caseCategory);
        if(promoterValue!=null)generator.writeStringField('promoter_val', promoterValue);
        generator.writeEndObject();
        system.debug('JSON text: '+ generator.getAsString());
		return generator.getAsString();
	}
    
    global void finish(Database.BatchableContext BC){
    } 
}