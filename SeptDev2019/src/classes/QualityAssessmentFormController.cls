public with sharing class QualityAssessmentFormController {
    
    public in_gage__Quality_Assessment_Form__c qa {get; set;}
    public boolean displayAnswers {get; set;}
    public boolean canRead {get; set;}
    public boolean canEdit {get; set;}
    public boolean canDel {get; set;}
    public boolean newRecord {get; set;}
    public boolean requiredFields {get; set;}
    public boolean seeCriticalErrorFields {get; set;}
    public string detailMode {get; set;}
    public string mode {get; set;}
    public string agentName {get; set;}
    public string managerName {get; set;}
    public decimal answerCount {get; set;}
    public boolean isAgentUser {get; set;}
    public boolean isQAuser {get; set;}
    public boolean isLocked {get; set;}
    public boolean hasSuccessfullyAppealed {get; set;}
    
    public map<id,Critical_Error__c> criticalErrorMap;
    public map<id,Critical_Error_Flag__c> criticalErrorFlagMap;
    public list<criticalError> criticalErrorList {get; set;}
    private map<id,Question_Section__c> sectionMap;
    public map<String, list<qAndAs>> qAndAlist { get; set; }
    ApexPages.StandardController sc2;
    public UserRecordAccess uAccess {get; set;}
    public map<string,string> customLabel {get; set;}
    
    //AI Data
    public boolean displayAIData {get;set;}
    public AgentAI agentAI {get; set;}
    public CustomerAI customerAI {get; set;}  
    public boolean aiComp {get;set;}
    //AI Data
    public QualityAssessmentFormController(ApexPages.StandardSetController controller) {        
    }
    
    public QualityAssessmentFormController(ApexPages.StandardController sc) {
        
        
        if(!test.isRunningTest()){
            // System.SObjectException: You cannot call addFields when the data is being passed into the controller by the caller.
            list<string> addFields = new list<string>{'Call_Date__c','Type__c','Case__c', 'Agent__c' ,'Opportunity__c','Agent_Full_Name__c','Number_of_Answers__c','Question_Set__c','Submit_Form__c','Profanity__c','Security_Check__c','Rudeness__c','Unauthorized_Termination_of_Call__c','Misrepresentation_of_the_Company__c','Comments__c','CLID__c','Call_Recording_Link__c','Submitted_Form_Date__c','Appeal_Status__c', 'Appeal_Reason_Long__c', 'Can_Submit_Form__c', 'AI_Created__c',
                'Quality_Team_Agent__c', 'AI_Behavioural_Score__c', 'Call_Duration__c', 'Case_Type__c', 'Agent__r.User_Record__c', 'Agent__r.Manager_User__r.Name',
                'in_gage__Name__c', 'in_gage__DOB__c', 'in_gage__Address__c', 'in_gage__High_Risk_Agent_Comms__c','in_gage__High_Risk_Customer_Comms__c', 'in_gage__Low_Risk_Agent_Comms__c', 'in_gage__Low_Risk_Customer_Comms__c',
                'in_gage__To_Investigate_Agent_Comms__c', 'in_gage__To_Investigate_Customer_Comms__c'};
                sc.addFields(addFields);
        }
        
        if(apexPages.currentPage().getParameters().get('id')!= null){
            this.qa= (in_gage__Quality_Assessment_Form__c)sc.getRecord();
            
            uAccess = [SELECT HasDeleteAccess,HasEditAccess,HasReadAccess,RecordId FROM UserRecordAccess WHERE RecordId = :qa.id AND UserId = :userInfo.getUserId()];
            canRead = uAccess.HasReadAccess;
            canEdit = uAccess.HasEditAccess;
            canDel = uAccess.HasDeleteAccess;
            agentName = qa.Agent_Full_Name__c;
            managerName = qa.Agent__r.Manager_User__r.Name;
        }else{
            qa = new in_gage__Quality_Assessment_Form__c(Type__c = null, Case__c = null, Opportunity__c = null, Agent__c = null);
            id cid = apexPages.currentPage().getParameters().get('cId');
            if(cid != null){
                qa.Case__c = cid;
                qa.Type__c = 'Case';
                qa.Call_Date__c = [select ClosedDate from case where id= :cid].ClosedDate;
            }
            id eid = apexPages.currentPage().getParameters().get('eId');
            if(eid != null){
                qa.Agent__c = eid;
                list<Employee__c> empList = [select Full_Name__c from Employee__c where id = :eid limit 1];
                if(!empList.isEmpty()){
                    string fullName = empList[0].Full_Name__c;
                    agentName = fullName;
                }
            }
            newRecord = true;
            canRead = true;
            canEdit = true;
            canDel = true;
            
        }
        
        customLabel = customLabelValues();
        
        isAgentUser = false;
        ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
        List<PermissionSetAssignment> qaFullPSList =  [SELECT PermissionSetId FROM PermissionSetAssignment 
                                                       WHERE AssigneeId= :UserInfo.getUserId() 
                                                       AND PermissionSet.Name = 'In_Gage_Full_Quality_Permission_Set'];
        List<PermissionSetAssignment> qaPSList =  [SELECT PermissionSetId FROM PermissionSetAssignment 
                                                       WHERE AssigneeId= :UserInfo.getUserId() 
                                                   AND PermissionSet.Name = 'In_Gage_Quality_Analyst_Permission_Set'];

        if(qaPSList.isEmpty() && qaFullPSList.isEmpty()) isAgentUser = true;
        //if(setting.Agent_Profile_ID__c != null) if(userInfo.getProfileId().left(15) == setting.Agent_Profile_ID__c.left(15)) isAgentUser = true;
        if(isAgentUser) canEdit = false;
        
        set<id> qaTeam = new set<id>();
        list<Group> qafQAgroup = [SELECT Id FROM Group WHERE DeveloperName = 'in_gage_Quality_Assessment_Form_QA_Team' AND Type='Queue' limit 1];
        if(!qafQAgroup.isEmpty()){
            list<GroupMember> qaList = [SELECT Id,UserOrGroupId FROM GroupMember WHERE GroupId = :qafQAgroup[0].id];
            for(GroupMember qa: qaList){
                if(string.valueOf(qa.UserOrGroupId).left(3)=='005') qaTeam.add(qa.UserOrGroupId);
            }
        }        
        isQAuser = qaTeam.contains(userInfo.getUserId());
        
        requiredFields = true;  // set required fields to on.
        if(qa.Question_Set__c != null) refreshCriticalErrors();
        
        //if(apexPages.currentPage().getURL().contains('qafEdit')) loadQAFedit();
        loadQAFedit();
        
        this.sc2=sc;
        
        //qAndAlist = new map<String, list<Quality_Assessment_Form_Answer__c>>();
        qAndAlist = new map<String, list<qAndAs>>();
        displayAnswers = false;
        answerCount = qa.Number_of_Answers__c == null ? 0 : qa.Number_of_Answers__c;
        if(detailMode == null) detailMode ='maindetail';
        string urlMode = apexPages.currentPage().getParameters().get('mode');
        if(urlMode != null) detailMode = urlMode;
        if(mode == null) mode = 'maindetail';
        if(qa.Question_Set__c != null && sectionMap == null) displayAnswerRecords();
        isLocked = false;
        try{
            isLocked = approval.isLocked(qa); 
        }catch(exception e){
            // errors expected the system permission not enabled.
            // Setup > Create > Workfows & Approvals > Process Automation Settings > Enable record locking and unlocking in Apex
            
            if(qa.Appeal_Status__c == 'With Quality Team' || qa.Appeal_Status__c == 'With Manager') isLocked = true;
        }        
        
        //hasSuccessfullyAppealed = qa.Appeal_Reason__c != null && qa.Appeal_Status__c == 'Successful';
        hasSuccessfullyAppealed = qa.Appeal_Reason_Long__c != null && qa.Appeal_Status__c == 'Successful';
        
        //AI
        agentAI = new AgentAI();
        customerAI = new CustomerAI();
        displayAIData = false;
        aiComp = false;
        if(qa.AI_Created__c) processAIData(qa.Case__c, qa.Agent__c);  
        //AI
    }
    
    
    public PageReference newQAF(){
        // called on the VF page 'qafNew' to redirect to VF page 'qaf'.
        // also called on the QAF page itself.
        
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('mode', 'edit');
        
        case caseRecord = new case();
        id recordId = apexPages.currentPage().getParameters().get('id');
        if(recordId != null){
            string objType = string.valueOf(recordId.getSObjectType());
            if(objType == 'Case'){
                caseRecord = [select id, accountId, ownerId from case where id = :recordId];
                pageRef.getParameters().put('cId', caseRecord.id);
                if(caseRecord.accountId != null) pageRef.getParameters().put('aid', caseRecord.accountId);
                
                string ownerObjType = string.valueOf(caseRecord.ownerId.getSObjectType());
                if(ownerObjType == 'User'){
                    user userRecord = [select id, in_gage__Employee_ID__c from user where id = :caseRecord.ownerId];
                    pageRef.getParameters().put('eId', userRecord.in_gage__Employee_ID__c);
                }
            }
        }
        pageRef.getParameters().put('retURL', '/'+recordId);
        
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference loadQAFedit(){
        // called on the VF page 'qafEdit' to redirect to VF page 'qaf'.
        
        PageReference pageRef = new PageReference('/apex/qaf');
        if(qa.id!=null)  pageRef.getParameters().put('id', qa.id);
        pageRef.getParameters().put('mode', 'edit');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    public PageReference editQAF(){
        detailMode = 'edit';
        return null;
    }
    public PageReference submitForm(){
        qa.Submit_Form__c = true;
        qa.Quality_Team_Agent__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        update qa;
        return null;
    }
    
    
    public PageReference cancelQAF(){
        
        qAndAlist.clear();
        
        if(apexPages.currentPage().getParameters().get('clone')=='1'){
            delete qa;
            requiredFields = false;
        }
        
        string returnURL = apexPages.currentPage().getParameters().get('retURL');
        
        if(returnURL != null){
            pageReference pageRef = new pageReference(returnURL);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
        return sc2.cancel();
    }
    
    public PageReference saveQAF(){
        
        
        if(qa.Type__c =='Opportunity'){
            qa.Case__c = null;
        }else{
            qa.Opportunity__c = null;
        }
        
        // update any CriticalErrors
        list<Critical_Error_Flag__c> insertErrorFlagList = new list<Critical_Error_Flag__c>();
        list<Critical_Error_Flag__c> deleteErrorFlagList = new list<Critical_Error_Flag__c>();
        
        if(criticalErrorList != null){
            
            if(!criticalErrorList.isEmpty()){
                
                for(criticalError crEr : criticalErrorList){
                    // if already in the map and now false, delete it.
                    if(criticalErrorFlagMap.containsKey(crEr.flag.id) && !crEr.flag.flag__c) deleteErrorFlagList.add(criticalErrorFlagMap.get(crEr.flag.id));
                    
                    // if not in the map and is flagged, insert it.
                    if(crEr.flag.id == null && crEr.flag.flag__c) insertErrorFlagList.add(new Critical_Error_Flag__c(in_gage__Quality_Assessment_Form__c = qa.id, Critical_Error__c= crEr.err.id, Flag__c = true));
                    
                    // update the original field values on the QAF
                    if(crEr.err.Name =='Profanity') qa.Profanity__c = crEr.flag.flag__c;
                    if(crEr.err.Name =='Security Check Failed') qa.Security_Check__c = crEr.flag.flag__c;
                    if(crEr.err.Name =='Rudeness') qa.Rudeness__c = crEr.flag.flag__c;
                    if(crEr.err.Name =='Unauthorized Termination of Call') qa.Unauthorized_Termination_of_Call__c = crEr.flag.flag__c;
                    if(crEr.err.Name =='Misrepresentation of the Company') qa.Misrepresentation_of_the_Company__c = crEr.flag.flag__c;
                }
            }
            
        }
        
        
        
        
        // insert/update the QA form
        if(qa.id == null){
            insert qa;
            if(qa.Question_Set__c != null) insert createAnswers(qa.id);
            
            PageReference pageRef = new PageReference('/apex/qaf');
            pageRef.getParameters().put('id', qa.id);
            //pageRef.getParameters().put('clone', '0');
            pageRef.setRedirect(true);
            return pageRef;
            //return null;
        }else{
            
            if(qa.Question_Set__c != null){
                
                if(qAndAlist.size() ==0){
                    insert createAnswers(qa.id);
                }else{
                    update theAnswers();
                }
            }
            update qa;
            
            // Critical error cause re-firing of QAF triggers, hence their DML is after the QA form is committed. 
            if(!deleteErrorFlagList.isEmpty()) database.delete(deleteErrorFlagList,false);
            if(!insertErrorFlagList.isEmpty()) database.insert(insertErrorFlagList,false);
            
            PageReference pageRef = new PageReference('/apex/qaf');
            pageRef.getParameters().put('id', qa.id);
            if(apexPages.currentPage().getParameters().get('clone') =='1') pageRef.getParameters().put('clone', '0');
            pageRef.setRedirect(true);
            return pageRef;
            //return null;
        }
        
        return null;
        //return sc2.save();
    }
    
    public PageReference cloneQAF(){
        
        in_gage__Quality_Assessment_Form__c cloneQA = (in_gage__Quality_Assessment_Form__c)sc2.getRecord().clone(false,false,false,false);
        cloneQA.Question_Set__c = null;
        insert cloneQA;
        PageReference pageRef = new PageReference('/apex/qaf');
        pageRef.getParameters().put('id', cloneQA.id);
        pageRef.getParameters().put('mode', 'edit');
        pageRef.getParameters().put('clone', '1');
        pageRef.getParameters().put('retURL', apexPages.currentPage().getURL());
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public pagereference submitForAppeal(){
        
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        //req.setComments(qa.Appeal_Reason__c);
        req.setComments(qa.Appeal_Reason_Long__c);
        req.setObjectId(qa.id);
        req.setProcessDefinitionNameOrId('Appeals_Process');
        req.setSkipEntryCriteria(false);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req);
        
        return null;
    }
    
    public pagereference refreshCriticalErrors(){
        
        criticalErrorList = new list<criticalError>();
        seeCriticalErrorFields = true;
        
        criticalErrorMap = new map<id,Critical_Error__c>([select id, name, Question_Set__c from Critical_Error__c where Question_Set__c = :qa.Question_Set__c]);
        system.debug('criticalErrorMap: '+ criticalErrorMap);
        if(!criticalErrorMap.isEmpty()){
            seeCriticalErrorFields = false;
            //criticalErrorList = new list<criticalError>();
            criticalErrorFlagMap = new map<id,Critical_Error_Flag__c>([select id,in_gage__Quality_Assessment_Form__c,Critical_Error__c,Flag__c from Critical_Error_Flag__c where Critical_Error__c in: criticalErrorMap.keySet() AND in_gage__Quality_Assessment_Form__c = :qa.id]);
            system.debug('criticalErrorFlagMap: '+ criticalErrorFlagMap);
            
            for(id id: criticalErrorMap.keySet()){
                criticalError ce = new criticalError();
                system.debug('id: '+ id);
                ce.err = criticalErrorMap.get(id);
                system.debug('ce.err: '+ ce.err);
                for(id flgId: criticalErrorFlagMap.keySet()){
                    system.debug('id: '+ id);
                    if(criticalErrorFlagMap.get(flgId).Critical_Error__c == id){
                        system.debug('flgId: '+ flgId);
                        system.debug('criticalErrorFlagMap.get(flgId).Critical_Error__c: '+ criticalErrorFlagMap.get(flgId).Critical_Error__c);
                        ce.flag = criticalErrorFlagMap.get(flgId);
                        system.debug('ce.flag: '+ ce.flag);
                    }
                }
                if(ce.flag == null) ce.flag = new Critical_Error_Flag__c();
                system.debug('ce.flag: '+ ce.flag);
                criticalErrorList.add(ce);
            }
        }
        system.debug('seeCriticalErrorFields: '+ seeCriticalErrorFields);
        
        return null;   
    }
    
    public class criticalError{
        public Critical_Error__c err {get; set;}
        public Critical_Error_Flag__c flag {get; set;}
        
        
        public criticalError(){
            err = new Critical_Error__c();
            flag = new Critical_Error_Flag__c();
        }
        
        
        public criticalError(Critical_Error__c er, Critical_Error_Flag__c f){
            err = er;
            flag = f;
        }
    }
    
    
    public class qAndAs{
        public Quality_Assessment_Form_Answer__c answer {get; set;}
        public list<SelectOption> pkListOptions {get; set;}
        public decimal maxPicklistValue {get; set;}
        
        public qAndAs(Quality_Assessment_Form_Answer__c a, list<SelectOption> pko, decimal mpv){
            answer = a;
            pkListOptions = pko;
            maxPicklistValue = mpv;
        }
        
    }
    
    public map<string,string> customLabelValues(){
        map<string,string> returnMap = new map<string,string>{
            'Appeals_Section' => 'Appeals',
                'Appeal_Reason' => 'Appeal Reason',
                'Submit_for_Appeal' => 'Submit for Appeal',
                'Display_Appeals' => 'true'
                };
                    map<string,Ingage_Customisation__c> cs = Ingage_Customisation__c.getAll();
        
        for(string s: returnMap.keySet()){
            if(cs.containsKey(s)){
                string value = cs.get(s).Value__c;
                if(cs.get(s).Type__c == 'Boolean') value=value.toLowerCase();
                returnMap.put(s, value);
            }
        }
        return returnMap;
    }
    
    
    public void displayAnswerRecords(){
        
        sectionMap = new map<id,Question_Section__c>([select id,Section_Name__c,Section_Number__c,Total_Scoring__c from Question_Section__c where Question_Set__c = :qa.Question_Set__c order by Section_Number__c]);
        
        // obtain answer records
        list<Quality_Assessment_Form_Answer__c> answers = [select id,Answer__c,Picklist_Answer__c,Question_Type__c,N_A__c,Question_Set__c,Question_Number__c,Question_Text__c,Comments__c,Score__c,Question__r.Question_Section__c,Question__r.Scoring__c,Question__r.Picklist_Options__c, Question__r.Picklist_value_for_100_Score__c, Question__r.Question_Tooltip__c from Quality_Assessment_Form_Answer__c where in_gage__Quality_Assessment_Form__c = :qa.id order by Question_Number__c];
        //if(answers.isEmpty()) answers = createAnswers();
        
        displayAnswers = true;
        
        // for each section
        for(Question_Section__c qs: sectionMap.values()){
            // add the question and answer
            for(Quality_Assessment_Form_Answer__c a : answers){
                if(a.Question__r.Question_Section__c == qs.id) {
                    list<SelectOption> pkListOptions;
                    decimal mpv=0;
                    if(a.Question__r.Picklist_Options__c != null){
                        pkListOptions = getPickListValues(a.Question__r.Picklist_Options__c);
                    }
                    list<qAndAs> ansa = new list<qAndAs>();
                    if(qAndAlist.containsKey(qs.id)) ansa = qAndAlist.get(qs.id);
                    ansa.add(new qAndAs(a,pkListOptions,mpv));
                    qAndAlist.put(qs.id,ansa);
                }
            }
        }
    }
    
    
    public list<selectOption> getPickListValues(string answerString){
        list<selectOption> returnList = new list<selectOption>();
        for(string s : answerString.split('\n')){
            string sTrim = s.trim();
            if(sTrim.length()>0) {
                returnList.add(new selectOption(sTrim,sTrim));
            }
        }
        return returnList;
    }
    
    
    public Question_Section__c[] getsectionList(){
        return sectionMap.values();
    }
    
    
    public list<Quality_Assessment_Form_Answer__c> createAnswers(){
        return createAnswers(qa.Id);
    }
    
    public list<Quality_Assessment_Form_Answer__c> createAnswers(id qaId){
        
        list<Quality_Assessment_Form_Answer__c> newAnswers = new list<Quality_Assessment_Form_Answer__c>();
        
        // obtain section and question records
        map<id,Question_Section__c> sectionMap = new map<id,Question_Section__c>([select id,Section_Name__c from Question_Section__c where Question_Set__c =: qa.Question_Set__c order by Section_Number__c]);
        list<Question__c> questions = [select id, Question__c, Question_No__c, Question_Section__c, Scoring__c, Picklist_Options__c,Picklist_value_for_100_Score__c from Question__c where Question_Section__c in: sectionMap.keySet() order by Question_No__c];
        
        // increment through each question to create an answer record
        for(Question__c q: questions){
            newAnswers.add(new Quality_Assessment_Form_Answer__c(
                in_gage__Quality_Assessment_Form__c = qaId,
                Question_Set__c = qa.Question_Set__c,
                Question__c = q.id,
                Question_Type__c = q.Picklist_Options__c != null ? 'Picklist' : null,
                Picklist_Answer__c = q.Picklist_value_for_100_Score__c != null ? string.valueOf(q.Picklist_value_for_100_Score__c) : null,
                Answer__c = false
            ));
        }
        
        return newAnswers;
    }
    
    
    public list<Quality_Assessment_Form_Answer__c> theAnswers(){
        
        list<Quality_Assessment_Form_Answer__c> allAnswers = new list<Quality_Assessment_Form_Answer__c>();
        
        // get all the answers for all the sections
        for(Question_Section__c qs: sectionMap.values()){
            for(qAndAs aa : qAndAlist.get(qs.id)){
                allAnswers.add(aa.answer);
            }
        }
        
        map<id,Quality_Assessment_Form_Answer__c> allAnswersMap = new map<id,Quality_Assessment_Form_Answer__c>();
        allAnswersMap.putAll(allAnswers);
        
        //update scoring
        map<id,Question__c> questionMap = new map<id,Question__c>([select id,Scoring__c,Question_Section__r.Section_Number__c from Question__c where Question_Section__c in: sectionMap.keySet()]);
        if(!questionMap.isEmpty()){
            for(Quality_Assessment_Form_Answer__c ansa : allAnswers){
                if(ansa.answer__c || ansa.N_A__c){
                    ansa.Score__c = questionMap.get(ansa.question__c).Scoring__c;
                }else if(ansa.Question_Type__c == 'Picklist'){
                    decimal score = 0;                      
                    try{
                        if(ansa.Question__r.Picklist_value_for_100_Score__c >0) score = (decimal.valueOf(ansa.Picklist_Answer__c) / ansa.Question__r.Picklist_value_for_100_Score__c) * questionMap.get(ansa.question__c).Scoring__c;
                    }catch(exception e){
                        // expected failure when non-numerical values in picklist
                    }
                    ansa.Score__c = score.setScale(2);
                }else{
                    ansa.Score__c = 0;
                }
                ansa.Section_Number__c = decimal.valueOf(questionMap.get(ansa.question__c).Question_Section__r.Section_Number__c);
            }
        }
        
        return allAnswers;
        
    }
    //AI
    private void processAIData(String caseId, String empId){
        
        //Customer
        Case c = [select id, (select id, Source__c  from QA_AI__r)from Case where Id = :caseId];
        
        if(c.QA_AI__r != null && c.QA_AI__r.size()>0){
            displayAIData = true;
            
            Set<Id> qaAICustomerSet = new set<id>();
            for(QA_AI__c qaAI: c.QA_AI__r){
                if(qaAI.Source__c == 'Customer') qaAICustomerSet.add(qaAI.id);
            } 
            List<AggregateResult> customerQAList = [select Source__c, AVG(AI_Score__c),
                                                    AVG(Anger_Score__c), AVG(Concern_Score__c),AVG(Happiness_Score__c),
                                                    AVG(Neutral_Score__c), AVG(Frustration_Score__c),AVG(Urgeny_Score__c) 
                                                    from QA_AI__c
                                                    where id in :qaAICustomerSet and Source__c = 'Customer' group by Source__c];  
            system.debug('customerQAList: '+ customerQAList);
            
            
            for(AggregateResult q : customerQAList){
                
                /*Decimal cPSentiment = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                Decimal cNSentiment = (Decimal)q.get('expr1')!=null ? ((Decimal)q.get('expr1')).setScale(2) : 0.00;
                Decimal cNeuSentiment = (Decimal)q.get('expr2')!=null ? ((Decimal)q.get('expr2')).setScale(2) : 0.00;
                Decimal cSentScore = Math.max(cPSentiment, Math.max(cNSentiment, cNeuSentiment));
                if(cSentScore!=null) cSentScore = cSentScore.setScale(2);
                String cSentLabel = 'None';
                if(Math.max(cPSentiment, Math.max(cNSentiment, cNeuSentiment)) == cPSentiment) cSentLabel = 'Positive';
                else if(Math.max(cPSentiment, Math.max(cNSentiment, cNeuSentiment)) == cNSentiment) cSentLabel = 'Negative';
                else if(Math.max(cPSentiment, Math.max(cNSentiment, cNeuSentiment)) == cNeuSentiment) cSentLabel = 'Neutral';*/
                
                Decimal cSentScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                    String cSentLabel = '';
                    Decimal actualScore = cSentScore * 3;
                    if(0 <= actualScore &&  actualScore < 0.5) cSentLabel = 'Very Negative';
                    else if(0.5 <= actualScore && actualScore < 1.5) cSentLabel = 'Negative';
                    else if(1.5 <= actualScore && actualScore < 2.5) cSentLabel = 'Neutral';
                    else if(2.5 <= actualScore && actualScore < 3.5) cSentLabel = 'Positive';
                    else if(3.5 <= actualScore) cSentLabel = 'Very Positive';
                
                CustomerSentiment cSentiment = new CustomerSentiment();
                //cSentiment.cPSentiment = cPSentiment;
                //cSentiment.cNSentiment = cNSentiment;
                //cSentiment.cNeuSentiment = cNeuSentiment;
                cSentiment.cSentScore = cSentScore;
                cSentiment.cSentLabel = cSentLabel;
                
                
                CustomerEmotion cEmotion = new CustomerEmotion();
                Map<Decimal, String> cEmotionMap = new Map<Decimal, String>();
                
                cEmotionMap.put((Decimal)q.get('expr1'),'Anger');
                cEmotionMap.put((Decimal)q.get('expr2'),'Concern');
                cEmotionMap.put((Decimal)q.get('expr3'),'Happiness');
                cEmotionMap.put((Decimal)q.get('expr4'),'Neutral');
                cEmotionMap.put((Decimal)q.get('expr5'),'Frustration');
                cEmotionMap.put((Decimal)q.get('expr6'),'Urgency');
                if(!cEmotionMap.isEmpty()){
                    List<Decimal> cList = new List<Decimal>();
                    cList.addAll(cEmotionMap.keySet());
                    cList.sort();
                    if(!cList.isEmpty() && cList.size() > 0){
                        
                        decimal top1 = cList[cList.size()-1];
                        if(top1!=null){
                            cEmotion.top1emo = top1.setScale(2);
                            cEmotion.top1emoLabel = cEmotionMap.get(top1);
                        }
                        
                        /*decimal top2 = cList[2];
                        if(top2!=null){
                            cEmotion.top2emo = top2.setScale(2);
                            cEmotion.top2emoLabel = cEmotionMap.get(top2); 
                        }
                        
                        decimal top3 = cList[1];
                        if(top3!=null){
                            cEmotion.top3emo = top3.setScale(2);
                            cEmotion.top3emoLabel = cEmotionMap.get(top3);
                        }
                        
                        decimal top4 = cList[0];
                        if(top4!=null){
                            cEmotion.top4emo = top4.setScale(2);
                            cEmotion.top4emoLabel = cEmotionMap.get(top4);  
                        }*/
                    }
                }
                customerAI.cSentiment = cSentiment;
                customerAI.cEmotion = cEmotion;
            }        
            //Agent Sentiment and Emotion
            Set<Id> qaAIAgentSet = new set<id>();
            for(QA_AI__c qaAI: c.QA_AI__r){
                if(qaAI.Source__c == 'Agent')qaAIAgentSet.add(qaAI.id);
            } 
            system.debug('empId: '+ empId);
            system.debug('qaAICustomerSet: '+ qaAIAgentSet);
            List<AggregateResult> agentQAList = [select id, AVG(AI_Score__c),
                                                 AVG(Anger_Score__c), AVG(Concern_Score__c),
                                                 AVG(Happiness_Score__c), AVG(Neutral_Score__c), AVG(Frustration_Score__c),
                                                 AVG(Urgeny_Score__c) from QA_AI__c
                                                 where id in :qaAIAgentSet and Source__c = 'Agent' 
                                                 and Employee__c = :empId group by id];  
            system.debug('agentQAList: '+ agentQAList);
            
            for(AggregateResult q : agentQAList){
                
                /*Decimal aPSentiment = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                Decimal aNSentiment = (Decimal)q.get('expr1')!=null ? ((Decimal)q.get('expr1')).setScale(2) : 0.00;
                Decimal aNeuSentiment = (Decimal)q.get('expr2')!=null ? ((Decimal)q.get('expr2')).setScale(2) : 0.00;
                Decimal aSentScore = Math.max(aPSentiment, Math.max(aNSentiment, aNeuSentiment));
                if(aSentScore!=null) aSentScore = aSentScore.setScale(2);
                String aSentLabel = 'None';
                if(Math.max(aPSentiment, Math.max(aNSentiment, aNeuSentiment)) == aPSentiment) aSentLabel = 'Positive';
                else if(Math.max(aPSentiment, Math.max(aNSentiment, aNeuSentiment)) == aNSentiment) aSentLabel = 'Negative';
                else if(Math.max(aPSentiment, Math.max(aNSentiment, aNeuSentiment)) == aNeuSentiment) aSentLabel = 'Neutral';*/
                
                Decimal aSentScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                    String aSentLabel = '';
                    Decimal actualScore = aSentScore * 3;
                    if(0 <= actualScore &&  actualScore < 0.5) aSentLabel = 'Very Negative';
                    else if(0.5 <= actualScore && actualScore < 1.5) aSentLabel = 'Negative';
                    else if(1.5 <= actualScore && actualScore < 2.5) aSentLabel = 'Neutral';
                    else if(2.5 <= actualScore && actualScore < 3.5) aSentLabel = 'Positive';
                    else if(3.5 <= actualScore) aSentLabel = 'Very Positive';
                
                AgentSentiment agentSent = new AgentSentiment();
                //agentSent.aPSentiment = aPSentiment;
                //agentSent.aNSentiment = aNSentiment;
                //agentSent.aNeuSentiment = aNeuSentiment;
                agentSent.aSentScore = aSentScore;
                agentSent.aSentLabel = aSentLabel;
                
                AgentEmotion aEmotion = new AgentEmotion();
                Map<Decimal, String> aEmotionMap = new Map<Decimal, String>();
                
                aEmotionMap.put((Decimal)q.get('expr1'),'Anger');
                aEmotionMap.put((Decimal)q.get('expr2'),'Concern');
                aEmotionMap.put((Decimal)q.get('expr3'),'Happiness');
                aEmotionMap.put((Decimal)q.get('expr4'),'Neutral');
                aEmotionMap.put((Decimal)q.get('expr5'),'Frustration');
                aEmotionMap.put((Decimal)q.get('expr6'),'Urgency');
                if(!aEmotionMap.isEmpty()){
                    List<Decimal> aList = new List<Decimal>();
                    aList.addAll(aEmotionMap.keySet());
                    aList.sort();
                    if(!aList.isEmpty() && aList.size() > 0){
                        
                        decimal top1 = aList[aList.size()-1];
                        if(top1!=null){
                            aEmotion.top1Aemo = top1.setScale(2);
                            aEmotion.top1AemoLabel = aEmotionMap.get(top1);
                        }
                        
                        /*decimal top2 = aList[2];
                        if(top2!=null){
                            aEmotion.top2Aemo = top2.setScale(2);
                            aEmotion.top2AemoLabel = aEmotionMap.get(top2); 
                        }
                        
                        decimal top3 = aList[1];
                        if(top3!=null){
                            aEmotion.top3Aemo = top3.setScale(2);
                            aEmotion.top3AemoLabel = aEmotionMap.get(top3);
                        }
                        
                        decimal top4 = aList[0];
                        if(top4!=null){
                            aEmotion.top4Aemo = top4.setScale(2);
                            aEmotion.top4AemoLabel = aEmotionMap.get(top4);  
                        }*/
                    }
                }
                agentAI.aSentiment = agentSent;
                agentAI.aEmotion = aEmotion;
            }                  
        }
        if(qa.High_Risk_Agent_Comms__c || qa.Low_Risk_Agent_Comms__c || qa.To_Investigate_Agent_Comms__c
           || qa.High_Risk_Customer_Comms__c || qa.Low_Risk_Customer_Comms__c || qa.To_Investigate_Customer_Comms__c) aiComp = true;
    }
    
    //Agent AI Data   
    public class AgentAI{
        public String agentName {get;set;}
        public AgentSentiment aSentiment {get; set;}
        public AgentEmotion aEmotion {get; set;}
    }
    public class AgentSentiment{
        
        public Decimal aSentScore {get; set;}
        public String aSentLabel {get; set;}
        
        public Decimal aPSentiment {get; set;}
        public Decimal aNSentiment {get; set;}
        public Decimal aNeuSentiment {get; set;}
    }
    public class AgentEmotion{
        
        public String top1AemoLabel {get; set;}
        public String top2AemoLabel {get; set;}
        public String top3AemoLabel {get; set;}
        public String top4AemoLabel {get; set;}
        public Decimal top1Aemo {get; set;}
        public Decimal top2Aemo {get; set;}
        public Decimal top3Aemo {get; set;}
        public Decimal top4Aemo {get; set;}   
    }  
    
    //Customer AI Data   
    public class CustomerAI{
        public CustomerSentiment cSentiment {get; set;}
        public CustomerEmotion cEmotion {get; set;}
    }
    public class CustomerSentiment{
        
        public Decimal cSentScore {get; set;}
        public String cSentLabel {get; set;}
        
        public Decimal cPSentiment {get; set;}
        public Decimal cNSentiment {get; set;}
        public Decimal cNeuSentiment {get; set;}
    }
    public class CustomerEmotion{
        
        public String top1emoLabel {get; set;}
        public String top2emoLabel {get; set;}
        public String top3emoLabel {get; set;}
        public String top4emoLabel {get; set;}
        public Decimal top1emo {get; set;}
        public Decimal top2emo {get; set;}
        public Decimal top3emo {get; set;}
        public Decimal top4emo {get; set;}   
    }  
    //AI
    
}