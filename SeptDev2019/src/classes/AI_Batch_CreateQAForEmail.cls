global class AI_Batch_CreateQAForEmail implements Database.Batchable<sObject>, Database.Stateful{
    
    global final String query;
    global final set<id> emailIds;
    global final set<id> eBatchIds;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    decimal aiQAFrequency = setting.QA_AI_Batch_Frequency__c;
    boolean batchQAAI = setting.Reduce_QA_AI_Procesing__c;
    boolean qaAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
   	boolean qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
    
    
    global AI_Batch_CreateQAForEmail(set<id> emailIds){
        this.emailIds = emailIds;
        this.query = getEmailQuery() + ' where id in :emailIds';
    }
    
    global AI_Batch_CreateQAForEmail(string query){
        eBatchIds = new set<Id>();
        /*DateTime startDT = DateTime.now();
        DateTime closeDT = startDT.addHours(-(aiQAFrequency.intValue()+1));
        string startDateTime = startDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        string closeDateTime = closeDT.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');*/
        
        Integer batchFrequency = aiQAFrequency.intValue() + 1;
        Datetime startDT = Datetime.now();    
        DateTime closeDT = startDT.addHours(-batchFrequency);
        system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
        
        String startDateTime = startDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        String closeDateTime = closeDT.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        system.debug('startDateTime: '+startDateTime+ ' closeDateTime: '+closeDateTime);
        
        list<QA_AI__c> qaAIList = [SELECT Email_Message_Id__c FROM QA_AI__c where CreatedDate >= :closeDT AND CreatedDate < :startDT]; 
        for(QA_AI__c qa: qaAIList){
            if(qa.Email_Message_Id__c!=null) eBatchIds.add(qa.Email_Message_Id__c);
        }
        this.query = 'select id, TextBody, Incoming, CreatedById, CreatedBy.Employee_ID__c, ParentId, Parent.AI_Language__c, Parent.Employee__c, Parent.Source_Email_Id__c, Parent.Outgoing_is_First_Email__c, Parent.Origin, Parent.Type from EmailMessage where id NOT IN :eBatchIds AND CreatedDate >=' + closeDateTime + 'AND CreatedDate <' + startDateTime;		
    }
    
    private string getEmailQuery(){
        return 'select id, TextBody, Incoming, CreatedById, CreatedBy.Employee_ID__c, ParentId, Parent.AI_Language__c, Parent.Employee__c, Parent.Source_Email_Id__c, Parent.Outgoing_is_First_Email__c, Parent.Origin, Parent.Type from EmailMessage';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<QA_AI__c> qaAIList = new List<QA_AI__c>();
        Set<Id> caseIds = new Set<Id>();
        List<EmailMessage> emailList = (List<EmailMessage>)scope;
        for(EmailMessage e :emailList){
            if(checkIfEligible(e)){
                caseIds.add(e.ParentId);
            } 
        }
        Map<Id, Id> empQAFMap = AIUtilityClass.getCaseWithQAFMap(caseIds);
        for(QA_AI__c q :qaAIList){
            system.debug('empQAFMap contains the key: '+ empQAFMap.containsKey(q.Employee__c));
            if(!empQAFMap.isEmpty() && empQAFMap.containsKey(q.Employee__c)){
                q.Quality_Assessment_Form__c = empQAFMap.get(q.Employee__c);
            }               
        }
        map<Id,sObject> recordTypeEligibleMap = AIUtilityClass.getRecordTypeEligibleMap(caseIds);
        for(EmailMessage e :emailList){
            boolean forSE = checkIfAISEEligible(e, recordTypeEligibleMap);
            if(checkIfEligible(e) && forSE && e.Parent.in_gage__AI_Language__c!=null){
                system.debug('e.TextBody: '+ e.TextBody);
                //clean description on case
                String processedData = getProcessedData(e.TextBody);
                system.debug('processedData: '+ processedData);
                caseIds.add(e.ParentId);
                //create new QA_AI record
                String emailSource = 'Agent';
                if(e.Incoming)emailSource = 'Customer';
                String qafId = '';
                if(!empQAFMap.isEmpty() && empQAFMap.containsKey(e.CreatedBy.Employee_ID__c)){
                    qafId = empQAFMap.get(e.CreatedBy.Employee_ID__c);
                } 
                QA_AI__c qaAI = new QA_AI__c(Parent_Case__c = e.ParentId
                                                               ,Email_Message_Id__c = e.Id
                                                               ,Text_to_Analyse__c = processedData
                                                               ,Language__c = e.Parent.in_gage__AI_Language__c
                                                               ,Source__c = emailSource
                                                               ,For_SE__c = forSE
                                                               ,Employee__c = e.CreatedBy.Employee_ID__c);
                if(String.isNotBlank(qafId)) qaAI.in_gage__Quality_Assessment_Form__c = qafId;
                qaAIList.add(qaAI);
                system.debug('qaAIList: '+ qaAIList);
            } 
        } 
        if(!qaAIList.isEmpty()) insert qaAIList;
    }
    private String getProcessedData(String description){
        String systemCleansed = '';
        String replacedData = '';
        if(String.isNotBlank(description)){
            systemCleansed = removeSystemGeneratedData(description); 
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
            String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
            replacedData = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
            system.debug('replacedData: '+ replacedData);
        }
        if(String.isNotBlank(replacedData))return replacedData.replaceAll('[0-9]', '').substringBefore('\n> ');   
        else return systemCleansed.replaceAll('[0-9]', ''); 
    }
    
    global void finish(Database.BatchableContext BC){
        
        //run QA Prediction batch
        if(qaSEAIEnabled){
            if(batchQAAI && aiQAFrequency!=null){
                /*DateTime startDT = DateTime.now();
                DateTime closeDT = startDT.addHours(-(aiQAFrequency.intValue()+1));*/
                
                Integer batchFrequency = aiQAFrequency.intValue() + 1;
                Datetime startDT = Datetime.now();    
            	DateTime closeDT = startDT.addHours(-batchFrequency);
            	system.debug('startDT: '+ startDT + ' closeDT: '+closeDT);
                
                List<QA_AI__c>	qaAIList = [select id from QA_AI__c where Categorisation_Only__c = false and Prediction_Completed__c = false and CreatedDate >= :closeDT AND CreatedDate <=:startDT];  
                set<Id> qaAIIds = new set<Id>();
                for(QA_AI__c qa: qaAIList)qaAIIds.add(qa.id);
                if(qaAIIds!=null && !qaAIIds.isEmpty()) database.executebatch(new AI_Batch_QAPrediction(qaAIIds),100);
            }else{
                List<QA_AI__c> qaAIList = [select id from QA_AI__c where Email_Message_Id__c in :emailIds];      
                set<Id> qaAIIds = new set<Id>();
                for(QA_AI__c qa: qaAIList)qaAIIds.add(qa.id);
                if(qaAIIds!=null && !qaAIIds.isEmpty()) database.executebatch(new AI_Batch_QAPrediction(qaAIIds),1);
            }
            system.debug('batch finished');  
        }
    } 
    
    private String removeSystemGeneratedData(String content){
        map<string,Email_For_Processing_Data__c> domainMap = Email_For_Processing_Data__c.getall();
        for(string dName : domainMap.keyset()) content = content.substringBefore('@'+ dName).substringBefore('--------------- Original Message');
        
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    private boolean checkIfEligible(EmailMessage e){
        boolean notSystemGenerated = false;
        boolean ignoreSettingEligible = true;
        
        if(!e.Incoming){
            Case_Ignore_Status__c ignoreSetting;
            if(string.valueOf(e.CreatedById).left(3) == '005') ignoreSetting = Case_Ignore_Status__c.getValues(e.CreatedById);
            if(ignoreSetting!= null) ignoreSettingEligible = false;
        } 
        
        if(string.valueOf(e.ParentId).left(3) == '500' && ignoreSettingEligible && e.TextBody!= null && (e.Id != e.Parent.in_gage__Source_Email_Id__c || e.Parent.in_gage__Outgoing_is_First_Email__c || (qaSEAIEnabled && !qaAIEnabled))){
            notSystemGenerated = true;
        }
        return notSystemGenerated;
        
    }
    
    private boolean checkIfAISEEligible(EmailMessage e, map<Id,sObject> recordTypeMap){
        boolean eligible = true;
        map<string,Languages_to_Support_for_AI_SE__c> langSettingMap = Languages_to_Support_for_AI_SE__c.getall();
        map<string,AI_SE_Ignore_Case_Origin__c> orignSettingMap = AI_SE_Ignore_Case_Origin__c.getall();
        map<string,AI_SE_Ignore_Case_RType__c> rTypeSettingMap = AI_SE_Ignore_Case_RType__c.getall();
        map<string,AI_SE_Ignore_Case_Type__c> typeSettingMap = AI_SE_Ignore_Case_Type__c.getall();

        if(e.Parent.in_gage__AI_Language__c!=null && !langSettingMap.isEmpty() && !langSettingMap.containsKey(e.Parent.in_gage__AI_Language__c)){
        	eligible = false;
        }
        if(e.Parent.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(e.Parent.Origin)){
        	eligible = false;
        }
        if(e.Parent.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(e.Parent.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(e.Parent.id)){
        	string recordTypeId = (String)recordTypeMap.get(e.Parent.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
    
    
}