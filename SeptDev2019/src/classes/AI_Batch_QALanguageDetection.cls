global class AI_Batch_QALanguageDetection implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global final String query;
    global final set<id> caseIDs;
    private boolean clAutoDetectEnabled = FeatureConsoleAPI.caseLangAutoDetectEnabled();

    global AI_Batch_QALanguageDetection(set<id> caseIDs){
        this.caseIDs = caseIDs;
        query = getCaseQuery() + ' where id in :caseIDs';
    }

    global AI_Batch_QALanguageDetection(string query){
        this.query = query;
    }
    private string getCaseQuery(){
        return 'select id, Origin, Type, Description, AI_Language__c from Case';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Case> caseList = (List<Case>)scope;
        List<Case> caseWithLangList = new List<Case>();
        map<Id,sObject> recordTypeEligibleMap = AIUtilityClass.getRecordTypeEligibleMap(caseIds);
        for(Case c :caseList){
            boolean forLAD = checkIfAILADEligible(c, recordTypeEligibleMap); 
            system.debug('forLAD: '+ forLAD);
            if(forLAD && c.Description != null){
                String processedText = getProcessedData(c.Description);
                HttpRequest req = new HttpRequest();
                String endPoint = 'https://ingage-ai.co.uk/language/predict';
                system.debug('Language Detection EndPoint: '+ endPoint);
                req.setEndpoint(endPoint);
                req.setTimeout(120000);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(stringToJSONforLangDetection(processedText));
                req.setMethod('POST');
                system.debug('Request sent: '+ req);
                Http http = new Http();
                HTTPResponse res = http.send(req);
                if (res.getStatusCode() != 200) {
                    System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                }else {
                    System.debug('The response: '+ res.getBody());
                    String langDetected = (String)JSON.deserialize(res.getBody(), String.class);
                    c.in_gage__AI_Language__c = langDetected;
                    caseWithLangList.add(c);
                }
            }
        }
        system.debug('caseWithLangList: '+ caseWithLangList);
        if(!caseWithLangList.isEmpty()) update caseWithLangList;
    }
    global void finish(Database.BatchableContext BC){
        //run Create QA for Case Batch        
        if(caseIDs.size() > 0 && clAutoDetectEnabled){
			system.debug('executing AI_Batch_CreateQAForCase');
            database.executebatch(new AI_Batch_CreateQAForCase(caseIDs),1);
        }
    }
    
    //Helper methods
    private String getProcessedData(String description){
        String systemCleansed = '';
        String replacedData = '';
        if(String.isNotBlank(description)){
            systemCleansed = removeSystemGeneratedData(description); 
            system.debug('systemCleansed: '+ systemCleansed);
            String url_pattern = 'https?://.*?\\s+';
            String EMAIL_PATTERN = '([^.@\\s]+)(\\.[^.@\\s]+)*@([^.@\\s]+\\.)+([^.@\\s]+)';                
            replacedData = systemCleansed.replaceAll(url_pattern, '').replaceAll(EMAIL_PATTERN, ''); 
            system.debug('replacedData: '+ replacedData);
        }
        if(String.isNotBlank(replacedData))return replacedData.replaceAll('[0-9]', '').substringBefore('\n> ');   
        else return systemCleansed.replaceAll('[0-9]', '');
    }
    
    private static String stringToJSONforLangDetection(String description) {
        if(description.length() >= 24) description =+ description.substring(0, 24);
        system.debug('description: '+ description);
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', description);
        generator.writeEndObject();
        String jsonString =  generator.getAsString();
        system.debug('jsonString: '+ jsonString);
        return jsonString;
    }
    private String removeSystemGeneratedData(String content){
        map<string,Email_For_Processing_Data__c> domainMap = Email_For_Processing_Data__c.getall();
        for(string dName : domainMap.keyset()) content = content.substringBefore('@'+ dName).substringBefore('--------------- Original Message');
        
        map<string,AI_Ignore_Texts__c > ignoreTextMap = AI_Ignore_Texts__c.getall();
        for(string t : ignoreTextMap.keyset()) content = content.replaceAll(t, '');
        system.debug('content: '+ content);
        return content;
    }
    private boolean checkIfAILADEligible(Case c, map<Id,sObject> recordTypeMap){
        boolean eligible = true;
        map<string,Case_Origins_to_Ignore_AI_LAD__c> orignSettingMap = Case_Origins_to_Ignore_AI_LAD__c.getall();
        map<string,Case_Types_to_Ignore_AI_LAD__c> typeSettingMap = Case_Types_to_Ignore_AI_LAD__c.getall();
        map<string,Case_RTypes_to_Ignore_AI_LAD__c> rTypeSettingMap = Case_RTypes_to_Ignore_AI_LAD__c.getall();
        if(c.Origin!=null && !orignSettingMap.isEmpty() && orignSettingMap.containsKey(c.Origin)){
        	eligible = false;
        }
        if(c.Type!=null && !typeSettingMap.isEmpty() && typeSettingMap.containsKey(c.Type)){
         	eligible = false;
        }
        if(recordTypeMap != null && recordTypeMap.containsKey(c.id)){
        	string recordTypeId = (String)recordTypeMap.get(c.Id).get('RecordTypeId');
            if(recordTypeId!=null && !rTypeSettingMap.isEmpty() && rTypeSettingMap.containsKey(recordTypeId)) eligible = false; 
        }
        return eligible;
    }
}