global class UpdateUserPhotoSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {

		// this schedulable class updates all Employees (max 10000). The update operation will fire the
		// Employee Trigger which in turn will bring User.SmallPhotoURL copied to Employee record.
		// User.SmallPhotoURL field is not available in the formulas currently -:)

		List<Employee__c> allEmployees=[Select ID from Employee__c where user_record__r.isactive=:true Limit 10000];
		update allEmployees;
	}
}