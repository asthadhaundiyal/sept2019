global class Batch_CalculateCoachingForm implements 
    Database.Batchable<sObject>, Database.Stateful{

    global final String query;
    global final id goalId;
    global string goaltype;
    global decimal val1,val2;
    global set<id> agentIds,cIds;
    global final string agentField;
    global final map<string,decimal> multiplierMap = new map<string,decimal>{'NPS' => 100,'QAF' => 1,'EES' => 1};
    global boolean bulkRun=false;
    global map<id,set<id>> mgr2agentsMap = new map<id,set<id>>();
    global final map<string,Custom_Goal_Calculation__c> cgcMap = new map<string,Custom_Goal_Calculation__c>();
    
    global Batch_CalculateCoachingForm(String q, id gId, string t, string aField, string aId){
        query= q; // + ' limit 200';
        goalId = gId;
        goaltype = t;
        agentField = aField;
        //if(goalType == 'FCR') query += ' limit 2000';
        val1 = 0; // used for trueCount & for sum values
        val2 = 0; // used for falseCount & for count values
        if(agentField != null){ agentIds = EventHandler.getAgentIdsWithHierarchy(new set<id>{aId});
        }else{ agentIds = new set<id>{aId};
        }
    }
    
    global Batch_CalculateCoachingForm(String q,set<id> cfIds){
        query= q;
        cIds = cfIds;  // used with query SOQL by the batch execute method.  Called mainly by the UI
        bulkRun = true;
        
        getVariables();
    }
    
    global Batch_CalculateCoachingForm(String q){
        // for bulk handing CoachingForms
        query= q;
        bulkRun = true;
        
        getVariables();
                
    }
    
    private void getVariables(){
    
        // get Custom Goal settings
        for(Custom_Goal_Calculation__c cgc : Custom_Goal_Calculation__c.getAll().values()){
            cgcMap.put(cgc.Name, cgc);
        }
        
        // blankGoals
        if(cIds != null) EventHandler.blankGoalRecords(cIds,cgcMap.keySet());
        
        // get manager Ids for the query period
        set<id> mgrIds = new set<id>();
        string mgrQuery = 'SELECT in_gage__Manager__c FROM Coaching_Form__c WHERE in_gage__Manager__c != null AND (Performance_End_Date__c = null OR Performance_End_Date__c >= LAST_N_DAYS:30) AND Performance_Start_Date__c != null ';
        if(cIds != null) mgrQuery += ' AND id in: cIds';
        mgrQuery += ' group by in_gage__Manager__c';
        for (aggregateResult ar : database.query(mgrQuery)){
            mgrIds.add(string.valueOf(ar.get('in_gage__Manager__c')));
        }
        // get managers Employees in Hierarchy
        for(Employee__c e : [select id, Employees_in_Hierarchy__c from Employee__c]){
            set<id> agents = new set<id>();
            //if(e.Employees_in_Hierarchy__c != null) agents.addAll((Set<id>) JSON.deserialize(e.Employees_in_Hierarchy__c, Set<id>.class));
            if(e.Employees_in_Hierarchy__c != null){
                set<id> deserialised = new set<id>();
                deserialised = (Set<id>) JSON.deserialize(e.Employees_in_Hierarchy__c, Set<id>.class);
                if(deserialised != null) agents.addAll(deserialised);
            }
            mgr2agentsMap.put(e.id,agents);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(bulkRun) return Database.getQueryLocator(query);
        else return Database.getQueryLocator([select id from Employee__c where id IN: agentIds]);
    }
    
    //global void execute(Database.BatchableContext BC, List<AggregateResult> arList){
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        if(scope.size()==1 && !bulkRun){ // called in batches of 1.
            string agentBasedQuery = query;
            
            id agentId;
            for(sObject s : scope){
                Employee__c emp = (Employee__c)s;
                agentId = emp.id;
            }
            
            system.debug('agentField = ' + agentField);
            if(agentField != null){
                list<string> qSplit = agentBasedQuery.split('GROUP BY');
                agentBasedQuery = qSplit[0] + ' AND '+ agentField + ' = \'' + agentId + '\' ';
                if(qSplit.size() > 1){ agentBasedQuery += ' GROUP BY ' + qSplit[1];
                }else{ agentBasedQuery += ' GROUP BY ' + agentField;
                }
            }
            
            //agentBasedQuery += ' limit 2000';
            system.debug('agentBasedQuery = '+ agentBasedQuery);
            
            if(!agentBasedQuery.contains('>= null') && !agentBasedQuery.contains('<= null')){
                for(AggregateResult ar: database.query(agentBasedQuery)){
                
                    if(goaltype == 'FCR'){
                        if(ar.get('in_gage__First_Case_Resolution__c') == true) val1 +=  decimal.valueOf(''+ar.get('expr0'));
                        if(ar.get('in_gage__First_Case_Resolution__c') == false) val2 += decimal.valueOf(''+ar.get('expr0'));
                    }else{
                        val1 += decimal.valueOf(''+ar.get('sum'));
                        val2 += decimal.valueOf(''+ar.get('ct'));
                    }
                }   
            }
        }else{  // scope.size >1
            
            list<Coaching_Form__c> cfList = (list<Coaching_Form__c>)scope;
            list<Coaching_Form__c> cfUpdate = new list<Coaching_Form__c>();
            list<goals__c> updateGoals = new list<goals__c>();
            
            for(Coaching_Form__c cf : cfList){
                system.debug('Coaching Form = ' +cf);
                
                if(cf.Performance_Start_Date__c != null && cf.Agent__r.User_Record__c != null){
                        
                    decimal performance =0;
                    decimal sumValue =0;
                    decimal ctValue =0;
                    decimal mapValue = 0;
                    
                    // get agent Ids
                    set<id> agentIds = new set<id>();
                    if(mgr2agentsMap.containsKey(cf.Agent__c)) agentIds = mgr2agentsMap.get(cf.Agent__c);
                    agentIds.add(cf.Agent__c);
                    
                    // get owner Ids
                    set<id> ownerIds = new set<id>();
                    for(Employee__c e :[SELECT User_Record__c FROM Employee__c where id in :agentIds and User_Record__c != null]){
                        ownerIds.add(e.User_Record__c);
                    }
                    
                    list<aggregateResult> results = new list<aggregateResult>();
                    map<string,goals__c> gMap = new map<string,goals__c>();
                    for(goals__c g : cf.Goals__r){
                        gMap.put(g.Goal__c,g);
                    }
                    goals__c goal = new goals__c();
                    string queryString = '';
                    string startDateTime = EventHandler.dt(cf.Performance_Start_Date__c);
                    string endDateTime = cf.Performance_End_Date__c != null ? EventHandler.dt(cf.Performance_End_Date__c.addDays(1)) : EventHandler.dt(date.today().addDays(1));
                    string startDate = string.valueOf(cf.Performance_Start_Date__c);
                    string endDate = cf.Performance_End_Date__c != null ? string.valueOf(cf.Performance_End_Date__c) : string.valueOf(date.today());
                    system.debug('startDate = ' + startDate);
                    system.debug('endDate = ' + endDate);
                    
                    // get FCR
                    if(gMap.containsKey('First Contact Resolution %')){
                        goal = gMap.get('First Contact Resolution %');
                        queryString = 'SELECT Count(Id),in_gage__First_Case_Resolution__c FROM Case WHERE ClosedDate >= '+ startDateTime+' and ClosedDate <= ' +endDateTime+ ' and isClosed = true and First_Employee__c in :agentIds GROUP BY First_Case_Resolution__c';
                        results = database.query(queryString);
                        
                        decimal trueCount =0;
                        decimal falseCount =0;
                        for(AggregateResult ar: results){
                            if(ar.get('in_gage__First_Case_Resolution__c') == true) trueCount +=  decimal.valueOf(''+ar.get('expr0'));
                            if(ar.get('in_gage__First_Case_Resolution__c') == false) falseCount += decimal.valueOf(''+ar.get('expr0'));
                        }
                        performance =0;
                        if(trueCount >0) performance = (trueCount / (trueCount + falseCount)) *100;
                        goal.Performance__c = performance;
                        goal.CalculationProgressWeight__c = null;
                        updateGoals.add(goal);
                        
                    }
                    
                    // get NPS
                    if(gMap.containsKey('NPS %')){
                        goal = gMap.get('NPS %');
                        //queryString = 'SELECT SUM(NPS_Comp_Value__c) sum, COUNT(NPS_Comp_Value__c) ct FROM NPS__c WHERE CreatedDate >= '+startDateTime+' and CreatedDate <= '+endDateTime+ ' and Employee__c in: agentIds';
                        // 23/1/2017 - created date changed to Completed_Date__c as per Rod
                        queryString = 'SELECT SUM(NPS_Comp_Value__c) sum, COUNT(NPS_Comp_Value__c) ct FROM NPS__c WHERE Completed_Date__c >= '+startDate+' and Completed_Date__c <= '+endDate+ ' and Employee__c in: agentIds';
                        results = database.query(queryString);
                        
                        sumValue =0;
                        ctValue =0;
                        for(AggregateResult ar: results){
                            sumValue += ar.get('sum') != null ? decimal.valueOf(''+ar.get('sum')) : 0;
                            ctValue += ar.get('ct') != null ? decimal.valueOf(''+ar.get('ct')) : 0;
                        }
                        performance =0;
                        decimal multiplier = 100; // 'NPS' => 100,'QAF' => 1,'EES' => 1
                        if(ctValue >0) performance = (sumValue / ctValue) * multiplier;
                        goal.Performance__c = performance;
                        goal.CalculationProgressWeight__c = null;
                        updateGoals.add(goal);
                        
                    }
                    
                    // get QAF
                    if(gMap.containsKey('Quality %')){
                        goal = gMap.get('Quality %');
                        queryString = 'SELECT SUM(Percentage_Completed__c) sum, COUNT(Percentage_Completed__c) ct FROM Quality_Assessment_Form__c WHERE Submitted_Form_Date__c >= '+startDate+' and Submitted_Form_Date__c <= '+endDate+' AND Submit_Form__c = true and Agent__c in :agentIds';  
                        results = database.query(queryString);
                        
                        sumValue =0;
                        ctValue =0;
                        for(AggregateResult ar: results){
                            sumValue += ar.get('sum') != null ? decimal.valueOf(''+ar.get('sum')) : 0;
                            ctValue += ar.get('ct') != null ? decimal.valueOf(''+ar.get('ct')) : 0;
                        }
                        performance =0;
                        decimal multiplier = 1; // 'NPS' => 100,'QAF' => 1,'EES' => 1
                        if(ctValue >0) performance = (sumValue / ctValue) * multiplier;
                        goal.Performance__c = performance;
                        goal.CalculationProgressWeight__c = null;
                        updateGoals.add(goal);
                        
                    }
                    // get EES
                    if(gMap.containsKey('Employee Engagement Survey')){
                        goal = gMap.get('Employee Engagement Survey');
                        queryString = 'SELECT SUM(Average_Score__c) sum, COUNT(Average_Score__c) ct FROM Employee_Survey_Anonymous__c WHERE Started_Date__c >= '+startDate+' and Started_Date__c <= '+endDate+' AND OwnerId in :ownerIds GROUP BY OwnerId';
                        results = database.query(queryString);
                    
                        sumValue =0;
                        ctValue =0;
                        for(AggregateResult ar: results){
                            sumValue += ar.get('sum') != null ? decimal.valueOf(''+ar.get('sum')) : 0;
                            ctValue += ar.get('ct') != null ? decimal.valueOf(''+ar.get('ct')) : 0;
                        }
                        performance =0;
                        decimal multiplier = 1; // 'NPS' => 100,'QAF' => 1,'EES' => 1
                        if(ctValue >0) performance = (sumValue / ctValue) * multiplier;
                        performance = performance.setScale(2, System.RoundingMode.HALF_EVEN);
                        goal.Performance_Text__c = string.valueOf(performance);
                        goal.CalculationProgressWeight__c = null;
                        updateGoals.add(goal);
                        
                    }
                    
                    // calculate Custom Goals - for each of the custom goals in the Custom Goal Calculation custom setting
                    for(string goalQuery : cgcMap.keySet()){
                        // if it exists in the Coaching form
                        if(gMap.containsKey(goalQuery)){
                            // get and format the SOQL query string
                            queryString = 'SELECT ' + cgcMap.get(goalQuery).SOQL_Select__c;
                            queryString += ' FROM ' +cgcMap.get(goalQuery).SOQL_From__c;
                            queryString += ' WHERE ' +cgcMap.get(goalQuery).SOQL_Where__c;
                            if(cgcMap.get(goalQuery).SOQL_GroupBy__c != null) queryString += ' GROUP BY ' +cgcMap.get(goalQuery).SOQL_GroupBy__c;
                            queryString = queryString.replace('<startDateTime>',startDateTime);
                            queryString = queryString.replace('<endDateTime>',endDateTime);
                            queryString = queryString.replace('<startDate>',startDate);
                            queryString = queryString.replace('<endDate>',endDate);
                            //queryString = queryString.replace('<agentId>','\''+cf.Agent__c+'\'');  // ==>  in: agentIds
                            //queryString = queryString.replace('<ownerId>','\''+cf.Agent__r.User_Record__c+'\'');  // ==> in: ownerIds
                            
                            system.debug('goalQuery = '+goalQuery+'. queryString = '+queryString+'. todo = '+cgcMap.get(goalQuery).Calculation_Type__c+' . performance as percent = '+cgcMap.get(goalQuery).Display_Performance_as_Percent__c);
                            
                            // recalculate the goal
                            updateGoals.add(calculateCustomGoal(gMap.get(goalQuery),queryString,agentIds,ownerIds,cgcMap.get(goalQuery).Calculation_Type__c, cgcMap.get(goalQuery).Display_Performance_as_Percent__c));
                        }
                    }
                }
                cf.isCalculating__c = false;
                cf.Recalculate__c = false;
                cfUpdate.add(cf);
                system.debug('Processed CF = '+cf);
            }
            if(!updateGoals.isEmpty()) database.update(updateGoals,false);
            database.update(cfUpdate,true);
        }
    }
    
    
    private goals__c calculateCustomGoal(goals__c goal, string queryString, set<id> agentIds, set<id> ownerIds, string todo, boolean perfAspercent){
        // query the database and calculate each goal
        
        string performanceString;
        
        try{
            
            decimal performance=0,e0=0,e1=0,trueCount=0,falseCount=0;
            list<aggregateResult> results = new list<aggregateResult>();
            results = database.query(queryString);
        
            for(AggregateResult ar: results){
                if(todo == '0/1' || todo == '1-(0/1)'){
                    e0 += decimal.valueOf(''+ar.get('expr0'));
                    e1 += decimal.valueOf(''+ar.get('expr1'));
                }
                else if(todo == 't/ct'){        
                    if(ar.get('field') == true) trueCount +=  decimal.valueOf(''+ar.get('expr0'));
                    if(ar.get('field') == false) falseCount += decimal.valueOf(''+ar.get('expr0'));
                }else{
                    if(ar.get('field') != null && String.valueOf(ar.get('field')) == todo){
                        trueCount +=  decimal.valueOf(''+ar.get('expr0'));
                    } 
                }
        
            }
            if(todo == '0/1'){
                if(perfAspercent)performance = e1 > 0 ? (e0/e1) *100 : 0;
                else performance = e1 > 0 ? (e0/e1) : 0;
            }else if(todo == '1-(0/1)'){
                if(perfAspercent)performance = e1 > 0 ? 100 - ((e0/e1) *100) : 0;
                else performance = e1 > 0 ? 1 - (e0/e1) : 0;
            }else if(todo == 't/ct'){
                if(perfAspercent)performance = trueCount>0 ? (trueCount / (trueCount + falseCount)) *100 : 0;
                else performance = trueCount>0 ? (trueCount / (trueCount + falseCount)) : 0;              
            }else{
                performance = trueCount;
            }
            
            system.debug('Performance check: e0 ='+e0+'. e1 = '+e1+'. trueCount = '+trueCount+'. falseCount = '+falseCount +'. Performance = '+performance);
            
            performance = performance.setScale(0, System.RoundingMode.HALF_EVEN);
            performanceString = (todo == '0/1' || todo == 't/ct' || todo == '1-(0/1)') && perfAspercent ? string.valueOf(performance) + '%' : string.valueOf(performance);
            
        }catch(exception e){
            system.debug('CUSTOM GOAL ERROR:');
            system.debug('CUSTOM GOAL:' + goal);
            system.debug('CUSTOM GOAL ERROR MESSAGE: '+ e.getMessage());
        }
        goal.Performance_Text__c = performanceString;
        goal.CalculationProgressWeight__c = null;
        
        return goal;
    }
    
    
    global void finish(Database.BatchableContext BC){
        
        if(goalId != null){  // Batch of 1.
        
            system.debug('Goal Id = '+ goalId + '. Goal Type = '+ goaltype + '.');
            system.debug('Val1 {trueCount || sum} = '+ val1);
            system.debug('Val2 {falseCount || count} = '+ val2);    
            
            decimal performance = 0;
            if(goaltype == 'FCR'){
                if(val1 >0) performance = (val1 / (val2 + val1)) *100;
            }else{
                decimal multiplier = 1;
                if(multiplierMap.containsKey(goaltype)) multiplier = multiplierMap.get(goaltype);
                if(val2 >0) performance = (val1 / val2) * multiplier;
            }
            
            //Goals__c goal = new Goals__c(Id = goalId, CalculationProgressWeight__c = null);
            Goals__c goal;
            goal = [select id, Coaching_Form__c,Performance_Text__c, Performance__c, CalculationProgressWeight__c from Goals__c where id =: goalId ];
            goal.CalculationProgressWeight__c = null;
            if(goaltype == 'EES'){ goal.Performance_Text__c = string.valueOf(performance.setScale(2, System.RoundingMode.HALF_EVEN));
            }else{ goal.Performance__c = performance;
            }
            update goal;
            
            // check Coaching_Form__c progress
            Coaching_Form__c cf;
            cf = [select id,CalculatingProgress__c, Recalculate__c, isCalculating__c from Coaching_Form__c where id = :goal.Coaching_Form__c limit 1];
            if(cf.CalculatingProgress__c <= 1){
                cf.isCalculating__c = false;
                cf.Recalculate__c = false;
                update cf;
            }
        }else{ // Batch >1          
        }
        
    }
    
}