global class IngagePostInstallation implements InstallHandler {
  global void onInstall(InstallContext context) {
    if(context.previousVersion() == null) {// install
    	
		scheduleEmployeeSurvey();
		scheduleInGageMaintenance();
		ingageDefaultData.insertDefaultCustomSettingValues();
		ingageDefaultData.insertNPStypeRecords();
		ingageDefaultData.insertDefaultQuestions();
		//ingageDefaultData.insertDefaultSurveys();
		try{
			createChatterGroup();
		}catch(exception e){
			system.debug('In-Gage Installation - Chatter Group failed to create.  '+ e);
		}
		
	}
    else
	{// upgrade

	}

  }
  
  @testVisible
  private void scheduleEmployeeSurvey(){
  	SendEmployeeSurveySchedule survey=new SendEmployeeSurveySchedule();
	//seconds, minutes, hour of day, day of month, month, day of week, Year
	String schedule = '0 0 10 ? * MON-SUN'; // run the schedule job Mon--SUN  at 10Am
	system.schedule('In-gage Employee Survey Schedule', schedule , survey);
  
  }
  
  public void populateEmployeeTable(){
  	map<id,user> userMap = new map<id,user>([select id from User where isActive = true]);
	User_Trigger.createEmployeeRecord(userMap.keySet());
  }
  
  @testVisible
  private void scheduleInGageMaintenance(){
  	Sch_InGage_Maintenance igm = new Sch_InGage_Maintenance();
  	//seconds, minutes, hour of day, day of month, month, day of week, Year
	String schedule = '0 0 22 ? * MON-SUN'; // run the schedule job Mon--SUN  at midnight
	system.schedule('In-gage Maintenance Schedule', schedule , igm);
  }
  
  @testVisible
  private void scheduleNPSchatterPost(){
	Sch_NPSchatterPost positiveNPS = new Sch_NPSchatterPost();
	datetime dt = datetime.now();
	dt = dt.addHours(1);
	String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
	system.schedule('In-gage Positive NPS Schedule', schedule , positiveNPS);
  }
  
  @testVisible
  private void createChatterGroup() {
  	id owner = Userinfo.getUserId();
  	CollaborationGroup chatterGroup = new CollaborationGroup(Name = 'Legendary Customer Service', CollaborationType='Public', OwnerId = owner);
	insert chatterGroup;
  }
  
}