/**
 * Test Class for Employee_Trigger
 */
@isTest
private class Employee_Trigger_T {

    /**
     *
     */

    static testMethod void test() {
		
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Id empRecordType = Schema.SObjectType.Employee__c.getRecordTypeInfosByName().get('Create New Employee').getRecordTypeId();
		Id mgrRecordType = Schema.SObjectType.Employee__c.getRecordTypeInfosByName().get('Create New Manager').getRecordTypeId();
        List<Employee__c> empList = new List<Employee__c>();
        Employee__c emp = new Employee__c(First_Name__c = 'TestEmployee', recordtypeid = empRecordType);
        Employee__c emp1 = new Employee__c(First_Name__c = 'TestManager', recordtypeid = mgrRecordType);
        empList.add(emp);
        empList.add(emp1);
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            FeatureManagement.setPackageBooleanValue('QA_Feature_Activated', true);
        	FeatureConsoleAPI.enableQA();
            FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated', true);
        	FeatureConsoleAPI.enableFCR();
            FeatureManagement.setPackageBooleanValue('EE_Feature_Activated', true);
        	FeatureConsoleAPI.enableEE();
                Test.startTest();
                
                    insert empList;
                    
                    update empList;
                    
                    delete empList;
                    
                    undelete empList;
                
                Test.stopTest();
            }
         }
}