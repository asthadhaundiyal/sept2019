@isTest
public class ingageTest2 {

    static testMethod void test1(){
        
        integer recordsNeeded = 200;
        list<account> aList = new list<account>();
        list<contact> conList = new list<contact>();
        list<case> cList = new list<case>();
        
        
        for(integer i =0; i<recordsNeeded ; i++){
            aList.add(new account(Name = 'testAccount'));                   
        }
        insert aList;
        
         for(integer i =0; i<recordsNeeded ; i++){
            conList.add(new contact(firstname ='ingage', lastname ='test', email ='roy@in-gage.co.uk', accountId=aList[i].id));
        }
        insert conList;
        
        for(integer i=0; i<recordsNeeded; i++){
            cList.add(new case(Subject ='test', AccountId=aList[i].id, ContactId=conList[i].id));
        }
        insert cList;
            
        for(integer i=0; i<recordsNeeded; i++){
            cList[i].Status = 'Closed';
            cList[i].Survey_Reminder_Date__c = date.today();
        }
        update cList;

        test.startTest();
		string d = string.valueOf(date.today());
		string q='SELECT Id,OwnerId FROM Case WHERE Survey_Reminder_Date__c <='+d;
		database.executeBatch(new Batch_Send_Survey_Reminders(q),200);        
        test.stopTest();
    }
    
}