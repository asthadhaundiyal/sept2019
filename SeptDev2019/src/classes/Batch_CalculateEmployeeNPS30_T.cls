@istest
public class Batch_CalculateEmployeeNPS30_T {
	
	
	private static Employee__c emp;
    private static account a;
	private static contact con;
	private static case c;
	private static list<NPS__c> npsList = new list<NPS__c>();
	
	private static void setupData(){

		emp = new Employee__c(First_Name__c = 'Test', Has_In_Gage_Licence__c = true);
		insert emp;
		
		system.assertNotEquals(null,emp.id);
        a = new account(name = 'test');
		insert a;
		con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
		insert con;
		
		c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'Closed');
		insert c;
        
		npsList.add(new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English'));
		npsList.add(new NPS__c(Employee__c = emp.id, Account__c = a.id, Case__c = c.id, Answer_1__c = 10, Answer_2__c = 10, Feedback_Comment__c = 'fee is high', Language__c = 'English'));
		insert npsList;
		
		for(NPS__c nps : [select Completed_Date__c from NPS__c]){
			system.assertNotEquals(null,nps.Completed_Date__c);
		}
		
	}
	private static string getEmployeeQuery(integer npsDays){
        date mindate = Date.today().addDays(npsDays);
        date maxdate = Date.today().addDays(1);
        return 'select id,Surveys_in_Last_30_Days__c,Surveys_in_Last_60_Days__c ,Surveys_in_Last_90_Days__c , (select id from NPS__r where Completed_Date__c >='+mindate+' and Completed_Date__c <='+maxdate+') from Employee__c where Has_In_Gage_Licence__c = true';
    }
	private static string dt(integer i){
	    string s = string.valueOf(i);
	    if(i<10) s='0'+s;
	    return s;
	}
	
	
	private static testmethod void test30NPS() {
		
		setupData();
        
        /*list<Employee__c> empList = database.query(getEmployeeQuery(EventHandler.SURVEY_30_DAYS) + ' and id = :empId limit 1');
        system.assertNotEquals(null,empList);
		system.assertNotEquals(null,empList[0].NPS__r);*/
                                                           
		/*emp.Surveys_in_Last_30_Days__c = null;
		update emp;*/
	
		Test.startTest();
		
		// fire the batch process
		database.executebatch(new Batch_CalculateEmployeeNPS30());
		
		Test.stopTest();
	}
   private static testmethod void test60NPS() {
		
		setupData();
        
        /*list<Employee__c> empList = database.query(getEmployeeQuery(EventHandler.SURVEY_60_DAYS));
        system.assertNotEquals(null,empList);
		system.assertNotEquals(null,empList[0].NPS__r);*/
                                                           
		emp.Surveys_in_Last_60_Days__c = null;
		update emp;
	
		Test.startTest();
		
		// fire the batch process
		database.executebatch(new Batch_CalculateEmployeeNPS30());
		
		Test.stopTest();
	}
    private static testmethod void test90NPS() {
		
		setupData();
        
        /*list<Employee__c> empList = database.query(getEmployeeQuery(EventHandler.SURVEY_90_DAYS));
        system.assertNotEquals(null,empList);
		system.assertNotEquals(null,empList[0].NPS__r);*/
                                                           
		emp.Surveys_in_Last_90_Days__c = null;
		update emp;
	
		Test.startTest();
		
		// fire the batch process
		database.executebatch(new Batch_CalculateEmployeeNPS30());
		
		Test.stopTest();
	}
    
    private static testmethod void testNewEmployees() {
        // split to two testMethods because "System.UnexpectedException: No more than one executeBatch can be called from within a test method. Please make sure the iterable returned from your start method matches the batch size, resulting in one executeBatch invocation."
		
		setupData();
	
		Test.startTest();

		// schedule the maintenance job
		datetime dt = datetime.now();
		dt = dt.addHours(1);
		String schedule = '0 ' + dt.minute() + ' ' +dt.hour()+ ' ' +dt.day()+ ' ' + dt.month() + ' ?';
		Sch_InGage_Maintenance schMain = new Sch_InGage_Maintenance();
		String jobId = system.schedule('In-gage Maintenance Schedule '+string.valueOf(date.today()), schedule, schMain);
		
		Test.stopTest();
	}
}