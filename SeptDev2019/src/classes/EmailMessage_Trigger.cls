public with sharing class EmailMessage_Trigger extends TriggerHandler {
    
    private boolean qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
    private ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    
    public EmailMessage_Trigger() {}
    public override void beforeInsert(List<SObject> newObjects) {}
    
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        system.debug('qa se enabled: '+ FeatureConsoleAPI.qaSEWithAIEnabled());
        system.debug('reduced ai processing: '+ setting.Reduce_QA_AI_Procesing__c);
        if(qaSEAIEnabled && !setting.Reduce_QA_AI_Procesing__c) createAIEvents((List<EmailMessage>) newObjects);
    }
    
    private void createAIEvents(List<EmailMessage> newObjects) { 
        system.debug('createEmailAIEvents');
        boolean createEvent = true;
        
        set<Id> caseIds = new set<Id>();
        for(EmailMessage e : newObjects){
            if(string.valueOf(e.ParentId).left(3) == '500') caseIds.add(e.ParentId);
        }
        if(!caseIds.isEmpty()){
            List<Case> caseList = [select id, in_gage__AI_Language__c from Case where id IN :caseIds];
            Map<Id, String> caseLangMap = new Map<Id, String>();
            for(Case c: caseList) caseLangMap.put(c.id, c.in_gage__AI_Language__c);
            for(EmailMessage e : newObjects){
                if(!e.Incoming){
                    Case_Ignore_Status__c ignoreSetting;
                    if(string.valueOf(e.CreatedById).left(3) == '005') ignoreSetting = Case_Ignore_Status__c.getValues(e.CreatedById);
                    if(ignoreSetting != null) createEvent = false;
                }
                if(string.valueOf(e.ParentId).left(3) == '500' && caseLangMap.get(e.ParentId)!=null && createEvent && e.TextBody!= null){
                    system.debug('events: '+ events); 
                    events.add(new Event(Event.EventType.CREATE_QA_AI_EMAIL, e.id));                
                }
            }   
        }        
    }    
}