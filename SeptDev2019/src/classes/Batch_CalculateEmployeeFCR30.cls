global class Batch_CalculateEmployeeFCR30 implements Database.Batchable<sObject>{
    // called in batch size of 1
    
    global final String Query;
    global final Integer FCR_DAYS = EventHandler.FCR_DAYS;
    global final set<id> employeeIds;


    global Batch_CalculateEmployeeFCR30(set<id> eIds){
        employeeIds = eIds;
        Query = getQuery() + ' where id in :employeeIds';
    }

    global Batch_CalculateEmployeeFCR30(String q){
        Query = q;
    }

    global Batch_CalculateEmployeeFCR30(){
        Query = getQuery() + ' where Has_In_Gage_Licence__c = true';
    }

    private string getQuery(){
        return 'select id from Employee__c';
    }

    public string getEmployeeQuery(){
        date mind = Date.today().addDays(FCR_DAYS);
        date maxd = Date.today().addDays(1);
        string mindate = mind.year()+'-'+dt(mind.month())+'-'+dt(mind.day())+'T00:00:00Z';
        string maxdate = maxd.year()+'-'+dt(maxd.month())+'-'+dt(maxd.day())+'T00:00:00Z';
        return 'select id,FCR_30_days__c,FCR_30_Day_Case_Count__c, x30_Day_Case_Count__c, NON_FCR_30_day__c, (select id, ClosedDate, First_Case_Resolution__c from CasesFirstEmployee__r where ClosedDate >='+mindate+' and ClosedDate <='+maxdate+') from Employee__c where Has_In_Gage_Licence__c = true';
    	//return 'select id,FCR_30_days__c,FCR_30_Day_Case_Count__c, x30_Day_Case_Count__c, NON_FCR_30_day__c, (select id, ClosedDate, First_Case_Resolution__c from CasesFirstEmployee__r where ClosedDate >='+mindate+' and ClosedDate <='+maxdate+') from Employee__c';
    }
    
    private string dt(integer i){
        string s = string.valueOf(i);
        if(i<10) s='0'+s;
        return s;
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        id empId;
        for(sObject s: scope){
            Employee__c e = (Employee__c)s;
            empId = e.id;
        }
        
        if(empId != null){
            Employee__c employee;
            try{
            	employee = database.query(getEmployeeQuery() + ' and id = :empId limit 1');
            }catch(exception e){}  // failure expected when employee doesnt have In-Gage licence
            
            if(employee != null){
            	// reset values prior to recalculation
            	employee.FCR_30_days__c = 0;
                employee.FCR_30_Day_Case_Count__c = 0;
                employee.x30_Day_Case_Count__c = 0;
                employee.NON_FCR_30_day__c = 0;
                
                // recalculate employee
	            employee = EventHandler.calculateEmployeeFCR(employee);
	            database.update(employee,false);
            }
        }
        
        /*
        list<Employee__c> updateEmployeeList = new list<Employee__c>();
        for(sobject s : scope){
            Employee__c e = (Employee__c)s;
            updateEmployeeList.add(EventHandler.calculateEmployeeFCR(e));
        }
        if(!updateEmployeeList.isEmpty()) database.update(updateEmployeeList,false);
        */
        
    }

    global void finish(Database.BatchableContext BC){
    }
}