@isTest


public class TestCloneWithItemsController {

    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Coaching_Form__c po {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}

    // initialize the controller
    public TestCloneWithItemsController(ApexPages.StandardController controller) {

        //initialize the stanrdard controller
        this.controller = controller;
        // load the current record
        po = (Coaching_Form__c)controller.getRecord();

    }

    // method called from the VF's action attribute to clone the coaching form goals
    public PageReference cloneWithItems() {

         // setup the save point for rollback
         Savepoint sp = Database.setSavepoint();
         Coaching_Form__c newPO;

         try {

              //copy the coaching form - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             po = [select Id, Agent__c, Manager__c from Coaching_Form__c where id = :po.id];
             newPO = po.clone(false);
             insert newPO;

             // set the id of the new po created for testing
               newRecordId = newPO.id;

             // copy over the line items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             List<Goals__c> items = new List<Goals__c>();
             for (Goals__c pi : [Select p.Id, p.Coaching_Form__c, Goal_Description__c,Performance__c,Target__c,Weighted_Performance__c,Weighted_Target__c,Weighting__c From Goals__c p where Coaching_Form__c = :po.id]) 
             {
                  Goals__c newPI = pi.clone(false);
                  newPI.Coaching_Form__c = newPO.id;
                  items.add(newPI);
             }
             
             
             // copy over the line items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
             
             
             List<Coaching_Promise__c> promiseitems = new List<Coaching_Promise__c>();
             for (Coaching_Promise__c pi : [Select p.Id, p.Coaching_Form__c, Comments__c, Completed__c, Completion_Date__c, Rating__c, Subject__c From Coaching_Promise__c p where Coaching_Form__c = :po.id])
             
             {
                 IF (pi.Completed__c == FALSE)
                 {
                      Coaching_Promise__c newPI = pi.clone(false);
                      newPI.Coaching_Form__c = newPO.id;
                      promiseitems.add(newPI);
                 }
             }           
             
             insert items;
             insert promiseitems;
             

         } catch (Exception e){
             // roll everything back in case of error
            Database.rollback(sp);
            ApexPages.addMessages(e);
            return null;
         }

        return new PageReference('/'+newPO.id+'/e?retURL=%2F'+newPO.id);
    }

}