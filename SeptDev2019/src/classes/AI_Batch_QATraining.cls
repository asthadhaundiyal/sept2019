global class AI_Batch_QATraining implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    // called in batch size of 1
    
    global final String query;
    global final set<id> qaFBIds;
    ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();


    global AI_Batch_QATraining(set<id> qaFBIds){
        this.qaFBIds = qaFBIds;
        query = getQAQuery() + ' where id in :qaFBIds and Category_Sent_for_Training__c = false';
    }

    global AI_Batch_QATraining(string query){
        this.query = query;
    }
    
	private string getQAQuery(){
        return 'select id, Text_for_Training__c, Case_Subject__c, Category__c, Sub_Category__c, Emotion__c , Sentiment__c, Language__c, Category_Sent_for_Training__c, Emotion_Sent_for_Training__c, Sentiment_Sent_For_Training__c from QA_AI_Feedback__c';
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<QA_AI_Feedback__c> qaAIList = (List<QA_AI_Feedback__c >)scope;
        List<QA_AI_Feedback__c> processedQAAIList = new List<QA_AI_Feedback__c>();
        for(QA_AI_Feedback__c qaAI :qaAIList){
            String language = qaAI.Language__c;
            String textForTraining = qaAI.Text_for_Training__c;
            system.debug('textForTraining: '+ textForTraining);
            
            if(String.isNotBlank(textForTraining) && String.isNotBlank(language)){
                system.debug('inside: ');
                String customerId = setting.AI_Customer_ID__c;
                String aiLang = language.toLowerCase();
                
                //Categorisation Training
                if(qaAI.Category__c!=null && qaAI.Sub_Category__c!=null){
                    HttpRequest req = new HttpRequest();
                    String endPoint = 'https://ingage-ai.co.uk/api/category/training/'+ aiLang + '/'+ customerId;
                	System.debug('QA Training for category EndPoint: '+ endPoint);
                	req.setEndpoint(endPoint);
                    req.setTimeout(120000);
                    req.setMethod('POST');
                    req.setHeader('Content-Type', 'application/json');
                    req.setBody(stringToJSONforTraining(qaAI.Text_for_Training__c, qaAI.Category__c, qaAI.Sub_Category__c, qaAI.Case_Subject__c));
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    
                    if (res.getStatusCode() != 200) {
                        System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                    }else {
                        System.debug('The training data was added successfully');
                        qaAI.Category_Sent_for_Training__c = true;

                    }    
                }
                //Emotion Training
                if(qaAI.Emotion__c!=null){
                    HttpRequest req = new HttpRequest();
                    String endPoint = 'https://ingage-ai.co.uk/api/emotions/training/'+ aiLang;
                	System.debug('QA Training for emotion EndPoint: '+ endPoint);
                	req.setEndpoint(endPoint);
                    req.setTimeout(120000);
                    req.setMethod('POST');
                    req.setHeader('Content-Type', 'application/json');
                    req.setBody(stringToJSONforTraining(qaAI.Text_for_Training__c, qaAI.Emotion__c, null,null));
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    
                    if (res.getStatusCode() != 200) {
                        System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                    }else {
                        System.debug('The training data was added successfully');
                        qaAI.Emotion_Sent_for_Training__c  = true;
                    }    
                }
                //Sentiment Training
                if(qaAI.Sentiment__c!=null){
                    HttpRequest req = new HttpRequest();
                    String endPoint = 'https://ingage-ai.co.uk/api/sentiment/training/'+ aiLang;
                	System.debug('QA Training for sentiment EndPoint: '+ endPoint);
                	req.setEndpoint(endPoint);
                    req.setTimeout(120000);
                    req.setMethod('POST');
                    req.setHeader('Content-Type', 'application/json');
                    req.setBody(stringToJSONforTraining(qaAI.Text_for_Training__c, qaAI.Sentiment__c,null,null));
                    Http http = new Http();
                    HTTPResponse res = http.send(req);
                    
                    if (res.getStatusCode() != 200) {
                        System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                    }else {
                        System.debug('The training data was added successfully');
                        qaAI.Sentiment_Sent_For_Training__c  = true;
                    }    
                }
                
                processedQAAIList.add(qaAI);
                
            }
        }
        if(!processedQAAIList.isEmpty()) update processedQAAIList;
    }
    
   	private static String stringToJSONforTraining(String content, String category, String subcategory, String subject) {
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', content);
        generator.writeStringField('category', category);
        if(subcategory!=null) generator.writeStringField('sub_category', subcategory);
        if(subject!=null) generator.writeStringField('subject ', subject);
        generator.writeEndObject();
        system.debug('json sent to AI for training: '+ generator.getAsString());
		return generator.getAsString();
	}
    
    global void finish(Database.BatchableContext BC){
    } 
}