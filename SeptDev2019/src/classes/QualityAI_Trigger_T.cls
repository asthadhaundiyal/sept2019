/**
 * Test Class for QualityAI_Trigger
 */
@isTest
private class QualityAI_Trigger_T {

    	
    private static Employee__c emp;
	private static account a;
	private static contact con;
	private static case c;
    private static QA_AI__c qaAI;
    private static QA_AI__c qaAI1;
    static in_gage__Quality_Assessment_Form__c qaf;
    
    static testMethod void testAI() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
            FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
			FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAWithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
        
            emp = new employee__c(User_Record__c = thisUser.id, First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            
            a = new account(name = 'test');
            insert a;
            con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            c = new case(accountId = a.id, contactId = con.id, in_gage__Employee__c = emp.id, subject ='test',type = 'Other', status = 'New', in_gage__AI_Language__c = 'English');
            insert c;
            
            qaf = new in_gage__Quality_Assessment_Form__c(Agent__c = emp.id, Case__c = c.id);
            insert qaf;
            
            c.AccountId = a.id;
            update c;
            
            qaAI = new QA_AI__c(Text_to_Analyse__c = 'test', Text_to_Analyse_Subject__c = 'test', Language__c = 'English', Parent_Case__c = c.id, 
                                        Source__c = 'Customer', Categorisation_Only__c=true, For_SE__c = true, For_CC__c= true, AI_Score__c = 0.5);
            insert qaAI;
            
            qaAI1 = new QA_AI__c(Text_to_Analyse__c = 'test', Text_to_Analyse_Subject__c = 'test', Language__c = 'English', Parent_Case__c = c.id, 
                                        Source__c = 'Agent', AI_Score__c = 0.5);
            insert qaAI1;
            
            Test.startTest();
            string jsonResponse1 = '{"sentences": [{ "confidence": 0.6,"sentence": "I really happy about this transaction.", "sentiment": {"classification": "Positive","confidence": 0.5478095781876149}, "emotions": [{"classification": "Concern","confidence": 0.3862179302537082}] }] }';
            qaAI.FirstTopCategoryLabel__c = 'testCategory';
            qaAI.AI_Sentiment_Response__c = jsonResponse1;
            qaAI1.in_gage__Quality_Assessment_Form__c = qaf.id;          
            update qaAI;  
            update qaAI1;
            Test.stopTest();
        }
            
    }
}