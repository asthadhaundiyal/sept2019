/**
 *
 */
public with sharing class PurchaseOrderCloneWithItemsController {

    // add the instance for the variables being passed by id on the url
    private ID poID { get; set; }
    // cloned po
    public in_gage__Coaching_Form__c poCloned { get{
    
        if(this.poCloned == null) {
        
            //copy the coaching form - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            this.poCloned = [select Id, in_gage__Agent__c, in_gage__Manager__c, in_gage__Previous_Coaching_Form__c,in_gage__Performance_Start_Date__c from in_gage__Coaching_Form__c where id = : this.poId].get(0).clone(false);
        }
        
        return this.poCloned;
    } set; }
    
    // objects describes
    private static Schema.DescribeSObjectResult d_CF = in_gage__Coaching_Form__c.sObjectType.getDescribe();
    private static Map<String, Schema.SObjectField> fd_CF = d_CF.Fields.getMap();
    private static Schema.DescribeSObjectResult d_G = in_gage__Goals__c.sObjectType.getDescribe();
    private static Map<String, Schema.SObjectField> fd_G = d_G.Fields.getMap();
    private static Schema.DescribeSObjectResult d_CP = in_gage__Coaching_Promise__c.sObjectType.getDescribe();
    private static Map<String, Schema.SObjectField> fd_CP = d_CP.Fields.getMap();

    // initialize the controller
    public PurchaseOrderCloneWithItemsController(ApexPages.StandardController controller) {

        // load the current record
        this.poId = controller.getId();
    }

    // method called from the VF's action attribute to clone the coaching form goals
    public PageReference cloneWithItems() {

        // setup the save point for rollback
        Savepoint sp = Database.setSavepoint();

        try {
            
            // if user has enough access
            checkSecurityAccessToObject();

            insert this.poCloned;

            // copy over the line items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            List < in_gage__Goals__c > items = new List < in_gage__Goals__c > ();
            for (in_gage__Goals__c g: [Select Id, in_gage__Coaching_Form__c, in_gage__Goal_Description__c, in_gage__Performance__c, in_gage__Target__c, in_gage__Weighting__c, in_gage__Performance_Text__c, in_gage__Target_Text__c,in_gage__Goal_Name__c From in_gage__Goals__c where in_gage__Coaching_Form__c = : this.poID]) {
                
                decimal performance = g.in_gage__Goal_Name__c == null ? g.in_gage__Performance__c : 0 ;
                string performanceT = g.in_gage__Goal_Name__c == null ? g.in_gage__Performance_Text__c : null ;
                
                in_gage__Goals__c newG = new in_gage__Goals__c();
                setFieldValue(newG, fd_G.get('in_gage__Coaching_Form__c').getDescribe(), this.poCloned.id);
                setFieldValue(newG, fd_G.get('in_gage__Goal_Description__c').getDescribe(), g.in_gage__Goal_Description__c);
                setFieldValue(newG, fd_G.get('in_gage__Goal_Name__c').getDescribe(), g.in_gage__Goal_Name__c);
                setFieldValue(newG, fd_G.get('in_gage__Performance__c').getDescribe(), performance);
                setFieldValue(newG, fd_G.get('in_gage__Target__c').getDescribe(), g.in_gage__Target__c);
                setFieldValue(newG, fd_G.get('in_gage__Performance_Text__c').getDescribe(), performanceT);
                setFieldValue(newG, fd_G.get('in_gage__Target_Text__c').getDescribe(), g.in_gage__Target_Text__c);
                setFieldValue(newG, fd_G.get('in_gage__Weighting__c').getDescribe(), g.in_gage__Weighting__c);
                items.add(newG);
            }
            
            if(items.size() > 0){
    
                insert items;
            }
            
            // copy over the line items - ONLY INCLUDE THE FIELDS YOU WANT TO CLONE
            List < in_gage__Coaching_Promise__c > promiseitems = new List < in_gage__Coaching_Promise__c > ();
            for (in_gage__Coaching_Promise__c cp: [Select Id, in_gage__Coaching_Form__c, in_gage__Comments__c, in_gage__Completed__c, in_gage__Completion_Date__c, in_gage__Rating__c, in_gage__Subject__c From in_gage__Coaching_Promise__c where in_gage__Coaching_Form__c = : this.poID AND in_gage__Completed__c = false]) {
                                    
                in_gage__Coaching_Promise__c newCP = new in_gage__Coaching_Promise__c();
                setFieldValue(newCP, fd_CP.get('in_gage__Coaching_Form__c').getDescribe(), this.poCloned.id);
                setFieldValue(newCP, fd_CP.get('in_gage__Comments__c').getDescribe(), cp.in_gage__Comments__c);
                setFieldValue(newCP, fd_CP.get('in_gage__Completed__c').getDescribe(), cp.in_gage__Completed__c);
                setFieldValue(newCP, fd_CP.get('in_gage__Completion_Date__c').getDescribe(), cp.in_gage__Completion_Date__c);
                setFieldValue(newCP, fd_CP.get('in_gage__Rating__c').getDescribe(), cp.in_gage__Rating__c);
                setFieldValue(newCP, fd_CP.get('in_gage__Subject__c').getDescribe(), cp.in_gage__Subject__c);
                promiseitems.add(newCP);
            }
            
            if(promiseitems.size() > 0){
            
                insert promiseitems;
            }
            
            // to re-trigger the eventHandler
            EventHandler.PREVENT_RECURSIVE_EXECUTION = false;
            in_gage__Coaching_Form__c poCloned2 = new in_gage__Coaching_Form__c(id = poID);
            upsert poCloned2;
                        
        } catch (Exception e) {

            Database.rollback(sp);
            this.poCloned = null;
            ApexPages.addMessages(e);
            return null;
        }
        
        return new PageReference('/' + this.poCloned.id);
    }
    
    /**
     *
     */
    private void checkSecurityAccessToObject(){
        
        // in_gage__Coaching_Form__c
        if((d_CF.isQueryable() && d_CF.isAccessible() && d_CF.isCreateable() && d_CF.isUpdateable()) == false){
            
            throw new SecurityException('Sorry but you dont have enough access to the records. Please contact your administrator.');
        }
        
        // in_gage__Goals__c
        if((d_G.isQueryable() && d_G.isAccessible() && d_G.isCreateable() && d_G.isUpdateable()) == false){
            
            throw new SecurityException('Sorry but you dont have enough access to the records. Please contact your administrator.');
        }
    
        // in_gage__Coaching_Promise__c
        if((d_CP.isQueryable() && d_CP.isAccessible() && d_CP.isCreateable()) == false){
            
            throw new SecurityException('Sorry but you dont have enough access to the records. Please contact your administrator.');
        }
    }
    
    /**
     *
     */
    private void setFieldValue(sObject sObj, Schema.DescribeFieldResult fieldDR, Object fieldValue){

        if(fieldDR.isAccessible() && fieldDR.isCreateable()){
            
            updateField(sObj, fieldDR.getName(), fieldValue, fieldDR.getType().Name());
        }else{
            
            throw new SecurityException('Sorry but you dont have enough access to the records fields. Please contact your administrator.');
        }
    }
    
   /**
     * 
     */
    private static void updateField(sObject sObj, String fieldName, Object fieldValue, String fieldType){
        
        if(fieldValue == null){
            
            sObj.put(fieldName, null);
        }else if(fieldType == 'DATE'){ 
            
            sObj.put(fieldName, (fieldValue == null ? null : Date.valueOf(fieldValue)));
        }else if(fieldType == 'DATETIME'){
            
            sObj.put(fieldName, (fieldValue == null ? null : Datetime.valueOf(fieldValue)));
        }else if(fieldType == 'DECIMAL' || fieldType == 'CURRENCY'){
            
            sObj.put(fieldName, (fieldValue == null ? null : Decimal.valueOf(String.valueOf(fieldValue))));
        }else if(fieldType == 'INTEGER'){
            
            sObj.put(fieldName, (fieldValue == null ? null : Integer.valueOf(fieldValue)));
        }else if(fieldType == 'LONG'){
            
            sObj.put(fieldName, (fieldValue == null ? null : Long.valueOf(String.valueOf(fieldValue))));
        }else if(fieldType == 'DOUBLE'){
            
            sObj.put(fieldName, (fieldValue == null ? null : Double.valueOf(fieldValue)));
        }else if(fieldType == 'BOOLEAN'){
            
            sObj.put(fieldName, (String.valueOf(fieldValue).toUpperCase() == 'TRUE'));
        }else{// String
            
            sObj.put(fieldName, fieldValue);
        }
    }
}