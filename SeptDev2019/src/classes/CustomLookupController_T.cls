@isTest
private class CustomLookupController_T {

	  static testMethod void CallCustomLookupController_Test() {
			Employee__c emp = new Employee__c();
			emp.First_Name__c = 'Test';
			insert emp;
			
			in_gage__Quality_Assessment_Form__c q = new in_gage__Quality_Assessment_Form__c();
			q.Agent__c = emp.id;
			q.Submitted_Form_Date__c = system.today();
			insert q;
			ApexPages.StandardController stdCtl = new ApexPages.StandardController(q);
			CallCustomLookController ctl=new CallCustomLookController(stdCtl);
			//ctl.saveCase();
			//ctl.editCase();

		}
	  static testMethod void CustomLookupController_Test() {

		PageReference pageRef = Page.CustomLookup;
		Test.setCurrentPage(pageRef);

		Employee__c emp = new Employee__c();
		emp.First_Name__c = 'Test';
		insert emp;

		in_gage__Quality_Assessment_Form__c q = new in_gage__Quality_Assessment_Form__c();
		q.Agent__c = emp.id;
		q.Submitted_Form_Date__c = system.today();
		insert q;
		ApexPages.StandardController stdCtl = new ApexPages.StandardController(q);

		CustomLookupController ctl=new CustomLookupController(stdCtl);
		ctl.search();
		//ctl.saveCase();
		ctl.getFormTag();
		ctl.getTextBox();
	}
}