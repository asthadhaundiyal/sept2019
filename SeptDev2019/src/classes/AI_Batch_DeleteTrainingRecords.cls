global class AI_Batch_DeleteTrainingRecords implements Database.Batchable<sObject>, Database.Stateful{
    
    global final String query;
    global AI_Batch_DeleteTrainingRecords(String q){
        query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
    }
    global  void execute(Database.BatchableContext BC,List<SObject> scope){
        if(!scope.isEmpty()) delete scope;
    }
    global void finish(Database.BatchableContext BC){
    }
    
}