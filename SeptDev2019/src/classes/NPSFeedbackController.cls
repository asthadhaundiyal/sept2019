public with sharing class NPSFeedbackController{
    
    public NPS_AI_Feedback__c npsFB {get; set;}
    public list<NPS_AI_Feedback__c> listNPSFB {get; set;}
    public List <NPS_AI_Feedback__c> delNPSFBList {get;set;}
    
    public NPS_AI__c npsAI {get; set;}
    public String npsId {get; set;}
    public String npsLanguage {get; set;}
    public String npsText {get; set;}
    public boolean canEdit {get; set;}
    public List<SelectOption> npsCategories {get;set;}
    ApexPages.StandardController sc2;
    public Integer rowIndex {get;set;}
    public NPS_AI_Feedback__c del;

    public NPSFeedbackController(ApexPages.StandardController sc){
        
        this.npsFB= (NPS_AI_Feedback__c)sc.getRecord();
        listNPSFB = new list<NPS_AI_Feedback__c>();  
        canEdit = false;

        this.sc2 = sc;
        
        npsId = npsFB.NPS_AI__c;
        if(npsId!=null){
          npsAI = [select Text_to_Analyse__c, Language__c from NPS_AI__c where id= :npsId ];  
        }
        if(npsAI!=null){
        	npsText = npsAI.Text_to_Analyse__c;
        	npsLanguage = npsAI.Language__c;
        	npsFB.Language__c = npsLanguage;
        }

        listNPSFB.add(npsFB);
        
        npsCategories=new List<SelectOption>();
        setCategories();
        
        delNPSFBList = new List <NPS_AI_Feedback__c>();
    }
    
    public void  addNPSFB(){
        NPS_AI_Feedback__c npsFB1 = new NPS_AI_Feedback__c();
        npsFB1.NPS_AI__c = npsId;
        npsFB1.Language__c = npsLanguage;
        listNPSFB.add(npsFB1);
    }
    
    public PageReference saveNPSFB() {
        if(!listNPSFB.isEmpty()) insert listNPSFB;
        canEdit = true;
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'The feedback training data is successfully sent for retraining of the AI model!!'));
        return null;
    }
    
    public PageReference cancelNPSFB(){       
        String returnURL = apexPages.currentPage().getParameters().get('retURL');
        if(returnURL != null){
            pageReference pageRef = new pageReference(returnURL);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return sc2.cancel();
    }
    public void deleteRow() {
        rowIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowIndex'));
        del = listNPSFB.remove(rowIndex);
        delNPSFBList.add(del);
        
    }
    private List<SelectOption> setCategories(){
        Map<string,NPS_Feedback_Types__c> npsAICategoriesMap = NPS_Feedback_Types__c.getall();
         for(string category : npsAICategoriesMap.keyset()) {
             if(npsAICategoriesMap.get(category).Language__c == npsLanguage){
                npsCategories.add(new SelectOption(npsAICategoriesMap.get(category).Type__c,npsAICategoriesMap.get(category).Type__c));    
             }
         }
        return npsCategories;
    }
}