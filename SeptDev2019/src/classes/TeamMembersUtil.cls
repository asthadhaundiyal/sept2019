public with sharing class TeamMembersUtil {


public static Set<ID> memberAgentIDs {get;set;}
public static Set<ID> getUserIDs(String userId)
{
	Set<ID> memberUserIDs=new Set<ID>();

	List<Employee__c> emp=[SELECT 	User_Record__c from Employee__c where ID=:userId];

	List<Employee__c> teamMembers;

if(emp!=null && emp.size()>0)
{
	teamMembers=[SELECT User_Record__c from Employee__c
	where 		Manager_User__c=:emp[0].User_Record__c];
}

for(Employee__c employee: teamMembers)
{
	if(employee.User_Record__c!=null)
     memberUserIDs.add(employee.User_Record__c);
}


	return memberUserIDs;
}



public static Set<ID> getAgentIDs(String userId)
{
	Set<ID> memberUserIDs=new Set<ID>();

	List<Employee__c> emp=[SELECT 	User_Record__c from Employee__c where ID=:userId];

	List<Employee__c> teamMembers;

if(emp!=null && emp.size()>0)
{
	teamMembers=[SELECT ID, User_Record__c from Employee__c
	where 		Manager_User__c=:emp[0].User_Record__c];
}

for(Employee__c employee: teamMembers)
{

     memberUserIDs.add(employee.ID);
}


	return memberUserIDs;
}

}