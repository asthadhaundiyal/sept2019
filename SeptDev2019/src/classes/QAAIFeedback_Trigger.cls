public without sharing class QAAIFeedback_Trigger extends TriggerHandler {
	ingage_Application_Settings__c setting = ingage_Application_Settings__c.getOrgDefaults();
    public Boolean qaAIEnabled {get;set;}
    public Boolean qaSEAIEnabled {get;set;}
    public QAAIFeedback_Trigger() {
        this.qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        this.qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
    }

    public override void beforeInsert(List<SObject> newObjects) {
       if(qaAIEnabled) updateQAAIOriginalData((List<QA_AI_Feedback__c>) newObjects); 
    }
    
    public override void beforeUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldMap, Map<Id, SObject> newMap) {}
    

    public override void beforeDelete(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {}
    
    public override void afterInsert(List<SObject> newObjects, Map<Id, SObject> newObjectsMap) {
        if(qaAIEnabled) updateQAAIWithTraining((List<QA_AI_Feedback__c>) newObjects);
        if((qaAIEnabled || qaSEAIEnabled) && !setting.Reduce_QA_AI_Procesing__c) createAIEvents((List<QA_AI_Feedback__c>) newObjects);
    }
    public override void afterUpdate(List<SObject> oldObjects, List<SObject> newObjects, Map<Id, SObject> oldObjectsMap, Map<Id, SObject> newObjectsMap) {}

    public override void afterDelete(List<SObject> oldObjects, Map<Id, SObject> oldObjectsMap) {}
    
    public override void afterUndelete(List<SObject> objects, Map<Id, SObject> objectsMap) {}

    private void createAIEvents(List<QA_AI_Feedback__c> newObjects) { 
        system.debug('createAIEvents');
        for(QA_AI_Feedback__c qa : newObjects){ 
            if(qa.Text_for_Training__c != null && qa.Language__c != null){
				events.add(new Event(Event.EventType.QA_AI_TRAINING, qa.id));
             }
        }
    }
    private void updateQAAIWithTraining(List<QA_AI_Feedback__c> newObjects) {       
        set<id> qaAIIds = new set<id>();
        for(QA_AI_Feedback__c qaAIF : newObjects) qaAIIds.add(qaAIF.QA_AI__c);
        List<QA_AI__c> qaAIList = [select id, FirstTopCategoryLabel__c, FirstTopSubCategoryLabel__c, 
                                            (select id,Category__c, Sub_Category__c from QA_AI_Feedback__r) 
                                            from QA_AI__c where id in : qaAIIds];
        if(!qaAIList.isEmpty()){           
            for(QA_AI__c qaAI: qaAIList){
                for(QA_AI_Feedback__c qaAIF : qaAI.QA_AI_Feedback__r){
                   qaAI.FirstTopCategoryLabel__c = qaAIF.Category__c;
                   qaAI.FirstTopSubCategoryLabel__c = qaAIF.Sub_Category__c; 
                }                 
            }
         update qaAIList;
        }
    }
    private void updateQAAIOriginalData(List<QA_AI_Feedback__c> newObjects) { 
        set<id> qaAIIds = new set<id>();
        Map<Id, QA_AI__c> qaAIMap = new Map<Id, QA_AI__c>();
        for(QA_AI_Feedback__c qaAIF : newObjects) qaAIIds.add(qaAIF.QA_AI__c);
        List<QA_AI__c> qaAIList = [select id, FirstTopCategoryLabel__c, FirstTopSubCategoryLabel__c
                                            from QA_AI__c where id in : qaAIIds];
        for(QA_AI__c qaAI : qaAIList) qaAIMap.put(qaAI.Id, qaAI);
        for(QA_AI_Feedback__c qaAIF : newObjects){
            if(qaAIMap.containsKey(qaAIF.QA_AI__c)){
                String category = qaAIMap.get(qaAIF.QA_AI__c).FirstTopCategoryLabel__c;
                String subCategory = qaAIMap.get(qaAIF.QA_AI__c).FirstTopSubCategoryLabel__c;
                qaAIF.Original_AI_Category__c = category;
                qaAIF.Original_AI_Sub_Category__c = subCategory;
            } 
        }
    }
}