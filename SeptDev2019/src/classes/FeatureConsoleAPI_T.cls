@isTest
private class FeatureConsoleAPI_T {
    
    static testMethod void testNPS() {
        // reset the feature for the test execution.  
        resetNPSTracking();
        
        // test that we cannot enable the NPS  feature
        // before we've enabled the NPS_Feature_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableNPS();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the NPS feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.npsEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableNPS();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableNPS();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        
        // test that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.npsEnabled();
        System.assert(!enabled, 'NPS Feature is enabled and should not be.');
        
        // enable the feature and then assert that it is enabled
        FeatureConsoleAPI.enableNPS();
        
        enabled = FeatureConsoleAPI.npsEnabled();
        System.assert(enabled, 'NPS Feature is disabled and should not be.');
        
        // disable the feature and then assert that it is disabled
        FeatureConsoleAPI.disableNPS();
        
        enabled = FeatureConsoleAPI.npsEnabled();
        System.assert(!enabled, 'NPS Feature is enabled and should not be.');
    }
    
    static testMethod void testQA() {
        // reset the feature for the test execution.  
        resetQATracking();
        
        // test that we cannot enable the QA  feature
        // before we've enabled the QA_Feature_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableQA();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the QA feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('QA_Feature_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.qaEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableQA();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableQA();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        
        // test that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.qaEnabled();
        System.assert(!enabled, 'QA Feature is enabled and should not be.');
        
        // enable the feature and then assert that it is enabled
        FeatureConsoleAPI.enableQA();
        
        enabled = FeatureConsoleAPI.qaEnabled();
        System.assert(enabled, 'QA Feature is disabled and should not be.');
        
        // disable the feature and then assert that it is disabled
        FeatureConsoleAPI.disableQA();
        
        enabled = FeatureConsoleAPI.qaEnabled();
        System.assert(!enabled, 'QA Feature is enabled and should not be.');
    }
    
    static testMethod void testFCR() {
        // reset the feature for the test execution.  
        resetFCRTracking();
        
        // test that we cannot enable the FCR feature
        // before we've enabled the FCR_Feature_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableFCR();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the FCR feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.fcrEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableFCR();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableFCR();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        
        // test that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.fcrEnabled();
        System.assert(!enabled, 'FCR Feature is enabled and should not be.');
        
        // enable the feature and then assert that it is enabled
        FeatureConsoleAPI.enableFCR();
        
        enabled = FeatureConsoleAPI.fcrEnabled();
        System.assert(enabled, 'FCR Feature is disabled and should not be.');
        
        // disable the feature and then assert that it is disabled
        FeatureConsoleAPI.disableFCR();
        
        enabled = FeatureConsoleAPI.fcrEnabled();
        System.assert(!enabled, 'FCR Feature is enabled and should not be.');
    }
    
    static testMethod void testEE() {
        // reset the feature for the test execution.  
        resetEETracking();
        
        // test that we cannot enable the EE  feature
        // before we've enabled the EE_Feature_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableEE();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the EE feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('EE_Feature_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.eeEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableEE();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableEE();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        
        // test that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.eeEnabled();
        System.assert(!enabled, 'EE Feature is enabled and should not be.');
        
        // enable the feature and then assert that it is enabled
        FeatureConsoleAPI.enableEE();
        
        enabled = FeatureConsoleAPI.eeEnabled();
        System.assert(enabled, 'EE Feature is disabled and should not be.');
        
        // disable the feature and then assert that it is disabled
        FeatureConsoleAPI.disableEE();
        
        enabled = FeatureConsoleAPI.eeEnabled();
        System.assert(!enabled, 'EE Feature is enabled and should not be.');
    }

    static testMethod void testNPSAI() {
        // reset the feature for the test execution.  
        resetNPSAITracking();
        
        // test that we cannot enable the NPS AI feature
        // before we've enabled the NPS_with_AI_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableNPSwithAI();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the NPS AI feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.npsWithAIEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableNPSwithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableNPSWithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        
        // test that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.npsWithAIEnabled();
        System.assert(!enabled, 'Expense Tracking Feature is enabled and should not be.');
        
        // enable the feature and then assert that it is enabled
        FeatureConsoleAPI.enableNPSwithAI();
        
        enabled = FeatureConsoleAPI.npsWithAIEnabled();
        System.assert(enabled, 'NPS AI  Feature is disabled and should not be.');
        
        // disable the feature and then assert that it is disabled
        FeatureConsoleAPI.disableNPSwithAI();
        
        enabled = FeatureConsoleAPI.npsWithAIEnabled();
        System.assert(!enabled, 'NPS AI Feature is enabled and should not be.');
    }
    
    static testMethod void testQAAI() {
        // reset the feature for the test execution
        resetQAAITracking();
        
        // test that we cannot enable the qa ai feature until
        // we enable the QAAIPermitted boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableQAWithAI();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the QA AI feature.');
        caughtException = false;
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.qaWithAIEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableQAWithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableQAWithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
    
        // assert that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.qaWithAIEnabled();
        System.assert(!enabled, 'QA AI Feature is enabled and should not be.');
        
        // enable the feature and assert that it is now enabled
        FeatureConsoleAPI.enableQAWithAI();
        
        enabled = FeatureConsoleAPI.qaWithAIEnabled();
        System.assert(enabled, 'QA AI Feature is disabled and should not be.');
        
        // disable the feature and assert that it is disabled
        FeatureConsoleAPI.disableQAWithAI();
        
        enabled = FeatureConsoleAPI.qaWithAIEnabled();
        System.assert(!enabled, 'QA AI Feature is enabled and should not be.');
    }
    
    static testMethod void testQASEAI() {
        // reset the feature for the test execution
        resetQAAISETracking();
        
        // test that we cannot enable the QA SE feature until
        // we enable the QASEPermitted boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableQASEWithAI();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the QA SE AI feature.');
        caughtException = false;
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.qaSEWithAIEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableQASEWithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableQASEWithAI();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
    
        // assert that the feature is currently disabled
        boolean enabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        System.assert(!enabled, 'QA SE AI Feature is enabled and should not be.');
        
        // enable the feature and assert that it is now enabled
        FeatureConsoleAPI.enableQASEWithAI();
        
        enabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        System.assert(enabled, 'QA SE AI Feature is disabled and should not be.');
        
        // disable the feature and assert that it is disabled
        FeatureConsoleAPI.disableQASEWithAI();
        
        enabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        System.assert(!enabled, 'QA SE AI Feature is enabled and should not be.');
    }
    
    static testMethod void testCaseLangDetection() {
        // reset the feature for the test execution.  
        resetCaseLangDetectTracking();
        
        // test that we cannot enable the Case Language  feature
        // before we've enabled the Case_Language_Auto_Detect_Activated boolean param.
        boolean caughtException = false;
        try {
            FeatureConsoleAPI.enableCaseLangAutoDetect();
        }
        catch(FeatureAccessException e) {
            System.assert(e.getMessage().contains('not currently licensed'), 'Unexpected exception message: ' + e.getMessage());
            caughtException = true;
        }
        System.assert(caughtException, 'We should not be able to enable the Case Language Detection feature.');
        caughtException = false;
        
        // enable the param so we can continue our testing
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated',true);
        
        // test the access restriction exception handling
        try {
            FeatureConsoleAPI.caseLangAutoDetectEnabled();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        system.debug('caughtException: '+ caughtException);
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
            
        try {
            FeatureConsoleAPI.enableCaseLangAutoDetect();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }
        //System.assert(caughtException, 'Expected an access restriction.');
        caughtException = false;
        
        try {
            FeatureConsoleAPI.disableCaseLangAutoDetect();
        }
        catch(FeatureAccessException e) {
            caughtException = true;
        }

        boolean enabled = FeatureConsoleAPI.caseLangAutoDetectEnabled();
        System.assert(!enabled, 'Case Language Detection Feature is enabled and should not be.');
        
        FeatureConsoleAPI.enableCaseLangAutoDetect();
        
        enabled = FeatureConsoleAPI.caseLangAutoDetectEnabled();
        System.assert(enabled, 'Case Language Detection Feature is disabled and should not be.');
        FeatureConsoleAPI.disableNPS();
        
    }
	private static void resetNPSTracking() {
        FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', false);
        FeatureConsoleAPI.disableNPS();
    }

    private static void resetQATracking() {
        FeatureManagement.setPackageBooleanValue('QA_Feature_Activated', false);
        FeatureConsoleAPI.disableQA();
    }
	private static void resetFCRTracking() {
        FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated', false);
        FeatureConsoleAPI.disableFCR();
    }
    private static void resetEETracking() {
        FeatureManagement.setPackageBooleanValue('EE_Feature_Activated', false);
        FeatureConsoleAPI.disableEE();
    }
    private static void resetNPSAITracking() {
        FeatureManagement.setPackageBooleanValue('NPS_with_AI_Activated', false);
        FeatureConsoleAPI.disableNPSWithAI();
    }

    private static void resetQAAITracking() {
        FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', false);
        FeatureConsoleAPI.disableQAWithAI();
    }
	private static void resetQAAISETracking() {
        FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', false);
        FeatureConsoleAPI.disableQASEWithAI();
    }
    private static void resetCaseLangDetectTracking() {
        FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', false);
        FeatureConsoleAPI.disableCaseLangAutoDetect();
    } 
}