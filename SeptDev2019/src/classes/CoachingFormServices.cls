global class CoachingFormServices {
    
    webservice static boolean recalculate(id cfId){
    	return initiated(cfId);
    }
 
 	private static boolean initiated(id cfId){
 		Coaching_Form__c cf = new Coaching_Form__c(id = cfId);
 		
 		// ensure not already calculating.
 		Coaching_Form__c cForm;
 		cForm = [select isCalculating__c,IsValidForCalculating__c from Coaching_Form__c where id = :cfId];
 		boolean isCalculating = cForm.isCalculating__c;
 		
 		if(!isCalculating & cForm.IsValidForCalculating__c){
	    	cf.isCalculating__c = true;
	    	update cf;
	    	
	    	string batchQuery;
			batchQuery = 'select id, Performance_Start_Date__c,Performance_End_Date__c,Agent__c,Agent__r.User_Record__c, (select id,Goal__c,Performance__c,CalculationProgressWeight__c from Goals__r) from Coaching_Form__c where Submit_Coaching_Form__c = false AND id = \''+cfId+'\'';
			database.executeBatch(new Batch_CalculateCoachingForm(batchQuery, new set<id>{cfId}),1);
	 		return true;
 		}else{
 			return false;
 		}
 	}
    
}