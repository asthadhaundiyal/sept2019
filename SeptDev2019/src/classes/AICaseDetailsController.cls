public with sharing class AICaseDetailsController {
    
    public Case c {get; set;}
    public Boolean qaAIEnabled {get;set;}
    public Boolean qaSEAIEnabled {get;set;}
    ApexPages.StandardController sc2;
    
    public String top1Category {get;set;}
    public String top1SubCategory {get;set;}
    public Decimal top1CategoryScore {get;set;}

    public List<AgentAI> agentAIList {get; set;}
    public CustomerAI customerAI {get; set;}
    public boolean aiComp {get;set;}
    
    public AICaseDetailsController(ApexPages.StandardController sc) {
        if(!test.isRunningTest()){
            // System.SObjectException: You cannot call addFields when the data is being passed into the controller by the caller.
            list<string> addFields = new list<string>{'in_gage__Address__c', 'in_gage__Name__c', 'in_gage__DOB__c', 'in_gage__High_Risk_Agent_Comms__c',
                               'in_gage__High_Risk_Customer_Comms__c', 'in_gage__Low_Risk_Agent_Comms__c', 'in_gage__Low_Risk_Customer_Comms__c',
                               'in_gage__To_Investigate_Agent_Comms__c', 'in_gage__To_Investigate_Customer_Comms__c'};
                sc.addFields(addFields);
        }
        this.c= (Case)sc.getRecord();
        this.sc2=sc;
        this.qaAIEnabled = FeatureConsoleAPI.qaWithAIEnabled();
        this.qaSEAIEnabled = FeatureConsoleAPI.qaSEWithAIEnabled();
        this.aiComp = false;
        agentAIList = new List<AgentAI>();
        customerAI = new CustomerAI();
        if(qaAIEnabled || qaSEAIEnabled) processAIData(c.Id);
    }
    
    private void processAIData(String caseId){
        
        Case c = [select id, IsClosed,Address__c, Name__c , DOB__c, High_Risk_Agent_Comms__c, High_Risk_Customer_Comms__c,
                                        Low_Risk_Agent_Comms__c, Low_Risk_Customer_Comms__c, To_Investigate_Agent_Comms__c, To_Investigate_Customer_Comms__c, 
                  (select id, Employee__c, Employee__r.Full_Name__c, Source__c, FirstTopCategoryLabel__c
                                        ,FirstTopCategoryScore__c, FirstTopSubCategoryLabel__c, Categorisation_Only__c, For_CC__c
                                        from QA_AI__r)from Case where Id = :caseId];
        
        Set<Id> qaAICategorySet = new set<id>();
        if(c.QA_AI__r != null && c.QA_AI__r.size()>0){
            
            //AI Case Categorisation
        	for(QA_AI__c qaAI: c.QA_AI__r){
            if(qaAI.Categorisation_Only__c){
            	top1Category = qaAI.FirstTopCategoryLabel__c;
                top1SubCategory = qaAI.FirstTopSubCategoryLabel__c;
                top1CategoryScore = (qaAI.FirstTopCategoryScore__c);
                if(top1CategoryScore!=null) top1CategoryScore.setScale(2);               
                }
            }                
            if(c.IsClosed && qaSEAIEnabled){
                //Customer Sentiment and Emotion
                Set<Id> qaAICustomerSet = new set<id>();
                for(QA_AI__c qaAI: c.QA_AI__r){
                    if(qaAI.Source__c == 'Customer') qaAICustomerSet.add(qaAI.id);
                } 
                
                List<AggregateResult> customerQAList = [select Source__c, AVG(AI_Score__c),
                                                AVG(Anger_Score__c), AVG(Concern_Score__c),
                                                AVG(Happiness_Score__c),AVG(Neutral_Score__c), 
                                                AVG(Frustration_Score__c),
                                                AVG(Urgeny_Score__c) from QA_AI__c
                                                where id in :qaAICustomerSet and Source__c = 'Customer'and AI_Score__c!= null group by Source__c];
                
                system.debug('customerQAList size: '+ customerQAList.size());
                system.debug('customerQAList: '+ customerQAList);
        
                for(AggregateResult q : customerQAList){
                    
                    Decimal cSentScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                    String cSentLabel = '';
                    Decimal actualScore = cSentScore * 3;
                    if(0 <= actualScore &&  actualScore < 0.5) cSentLabel = 'Very Negative';
                    else if(0.5 <= actualScore && actualScore < 1.5) cSentLabel = 'Negative';
                    else if(1.5 <= actualScore && actualScore < 2.5) cSentLabel = 'Neutral';
                    else if(2.5 <= actualScore && actualScore < 3.5) cSentLabel = 'Positive';
                    else if(3.5 <= actualScore) cSentLabel = 'Very Positive';
                    
                    CustomerSentiment cSentiment = new CustomerSentiment();
                    cSentiment.cSentScore = cSentScore;
                    cSentiment.cSentLabel = cSentLabel;
                 
                    CustomerEmotion cEmotion = new CustomerEmotion();
                    Map<Decimal, String> cEmotionMap = new Map<Decimal, String>();
                    cEmotionMap.put((Decimal)q.get('expr1'),'Anger');
                    cEmotionMap.put((Decimal)q.get('expr2'),'Concern');
                    cEmotionMap.put((Decimal)q.get('expr3'),'Happiness');
                    cEmotionMap.put((Decimal)q.get('expr4'),'Neutral');
                    cEmotionMap.put((Decimal)q.get('expr5'),'Frustration');
                	cEmotionMap.put((Decimal)q.get('expr6'),'Urgency');
                    system.debug('cEmotionMap: '+ cEmotionMap);
                    if(!cEmotionMap.isEmpty()){
                        List<Decimal> cList = new List<Decimal>();
                        cList.addAll(cEmotionMap.keySet());
                        cList.sort();
                        system.debug('cList.size(): '+ cList.size());
                        system.debug('cList: '+ cList);
                        if(!cList.isEmpty() && cList.size() > 0){
                            
                            decimal top1 = cList[cList.size()-1];
                            if(top1!=null){
                                cEmotion.top1emo = top1.setScale(2);
                                cEmotion.top1emoLabel = cEmotionMap.get(top1);
                            }
                                         //   displayAISEData = true;
                            /*decimal top2 = cList[2];
                            if(top2!=null){
                                cEmotion.top2emo = top2.setScale(2);
                                cEmotion.top2emoLabel = cEmotionMap.get(top2); 
                            }
                            
                            decimal top3 = cList[1];
                            if(top3!=null){
                                cEmotion.top3emo = top3.setScale(2);
                                cEmotion.top3emoLabel = cEmotionMap.get(top3);
                            }
                            
                            decimal top4 = cList[0];
                            if(top4!=null){
                                cEmotion.top4emo = top4.setScale(2);
                                cEmotion.top4emoLabel = cEmotionMap.get(top4);  
                            }*/
                        }
                    }
                    customerAI.cSentiment = cSentiment;
                    system.debug('customerAI.cSentiment.cSentLabel: '+ customerAI.cSentiment.cSentLabel);
                    customerAI.cEmotion = cEmotion;
                    system.debug('customerAI.cEmotion.top1emoLabel: '+ customerAI.cSentiment.cSentLabel);
                }
                
                //Agent Sentiment and Emotion
                Map<String,String> agentNameMap = new Map<String,String>();
                Set<Id> qaAIAgentSet = new set<id>();
                for(QA_AI__c qaAI: c.QA_AI__r){
                    if(qaAI.Source__c == 'Agent'){
                       qaAIAgentSet.add(qaAI.id);
                       agentNameMap.put(qaAI.Employee__c, qaAI.Employee__r.Full_Name__c);
                    }
                } 
                system.debug('agentNameMap: '+ agentNameMap);
                
                List<AggregateResult> agentQAList = [select in_gage__Employee__c,AVG(AI_Score__c),
                                                AVG(Anger_Score__c), AVG(Concern_Score__c), AVG(Happiness_Score__c),
                                                AVG(Neutral_Score__c), AVG(Frustration_Score__c) , AVG(Urgeny_Score__c) 
                                                from QA_AI__c
                                                where id in :qaAIAgentSet and AI_Score__c!= null group by Employee__c];
                
                system.debug('agentQAList size: '+ agentQAList.size());
                system.debug('agentQAList: '+ agentQAList);
                
                for(AggregateResult q : agentQAList){
                    
                    Decimal aSentScore = (Decimal)q.get('expr0')!=null ? ((Decimal)q.get('expr0')).setScale(2) : 0.00;
                    String aSentLabel = '';
                    Decimal actualScore = aSentScore * 3;
                    if(0 <= actualScore &&  actualScore < 0.5) aSentLabel = 'Very Negative';
                    else if(0.5 <= actualScore && actualScore < 1.5) aSentLabel = 'Negative';
                    else if(1.5 <= actualScore && actualScore < 2.5) aSentLabel = 'Neutral';
                    else if(2.5 <= actualScore && actualScore < 3.5) aSentLabel = 'Positive';
                    else if(3.5 <= actualScore) aSentLabel = 'Very Positive';
                    
                    AgentSentiment agentSent = new AgentSentiment();
                    agentSent.aSentScore = aSentScore;
                    agentSent.aSentLabel = aSentLabel;
                 
                    AgentEmotion aEmotion = new AgentEmotion();
                    Map<Decimal, String> aEmotionMap = new Map<Decimal, String>();
                    
                    aEmotionMap.put((Decimal)q.get('expr1'),'Anger');
                    aEmotionMap.put((Decimal)q.get('expr2'),'Concern');
                    aEmotionMap.put((Decimal)q.get('expr3'),'Happiness');
                    aEmotionMap.put((Decimal)q.get('expr4'),'Neutral');
                    aEmotionMap.put((Decimal)q.get('expr5'),'Frustration');
                	aEmotionMap.put((Decimal)q.get('expr6'),'Urgency');
                    system.debug('aEmotionMap: '+ aEmotionMap);
                    if(!aEmotionMap.isEmpty()){
                        List<Decimal> aList = new List<Decimal>();
                        aList.addAll(aEmotionMap.keySet());
                        aList.sort();
                         system.debug('aList: '+ aList);
                        if(!aList.isEmpty() && aList.size()>0){
                            
                            decimal top1 = aList[aList.size()-1];
                            if(top1!=null){
                                aEmotion.top1Aemo = top1.setScale(2);
                                aEmotion.top1AemoLabel = aEmotionMap.get(top1);
                            }
                            
                            /*decimal top2 = aList[2];
                            if(top2!=null){
                                aEmotion.top2Aemo = top2.setScale(2);
                                aEmotion.top2AemoLabel = aEmotionMap.get(top2); 
                            }
                            
                            decimal top3 = aList[1];
                            if(top3!=null){
                                aEmotion.top3Aemo = top3.setScale(2);
                                aEmotion.top3AemoLabel = aEmotionMap.get(top3);
                            }
                            
                            decimal top4 = aList[0];
                            if(top4!=null){
                                aEmotion.top4Aemo = top4.setScale(2);
                                aEmotion.top4AemoLabel = aEmotionMap.get(top4);  
                            }*/
                        }
                    }
                    system.debug('aEmotion: '+ aEmotion);
                    AgentAI agentAI = new AgentAI();
                    String agentId = (String)q.get('in_gage__Employee__c');
                    if(agentId!= null) agentAI.agentName = agentNameMap.get(agentId);
                    agentAI.aSentiment = agentSent;
                    agentAI.aEmotion = aEmotion;
                    system.debug('agentAI: '+ agentAI);
                    agentAIList.add(agentAI);
                }                  
            }        
        }
        if( c.High_Risk_Agent_Comms__c || c.Low_Risk_Agent_Comms__c || c.To_Investigate_Agent_Comms__c
           || c.High_Risk_Customer_Comms__c || c.Low_Risk_Customer_Comms__c || c.To_Investigate_Customer_Comms__c) aiComp = true;
    }

    
    //Agent AI Data   
    public class AgentAI{
        public String agentName {get;set;}
        public AgentSentiment aSentiment {get; set;}
        public AgentEmotion aEmotion {get; set;}
    }
    public class AgentSentiment{
        
    	public Decimal aSentScore {get; set;}
    	public String aSentLabel {get; set;}
        
   		public Decimal aPSentiment {get; set;}
    	public Decimal aNSentiment {get; set;}
    	public Decimal aNeuSentiment {get; set;}
    }
    public class AgentEmotion{
        
    	public String top1AemoLabel {get; set;}
    	public String top2AemoLabel {get; set;}
    	public String top3AemoLabel {get; set;}
    	public String top4AemoLabel {get; set;}
    	public Decimal top1Aemo {get; set;}
    	public Decimal top2Aemo {get; set;}
    	public Decimal top3Aemo {get; set;}
    	public Decimal top4Aemo {get; set;}   
    }  
    
    //Customer AI Data   
    public class CustomerAI{
        public CustomerSentiment cSentiment {get; set;}
        public CustomerEmotion cEmotion {get; set;}
    }
    public class CustomerSentiment{
        
    	public Decimal cSentScore {get; set;}
    	public String cSentLabel {get; set;}
        
   		public Decimal cPSentiment {get; set;}
    	public Decimal cNSentiment {get; set;}
    	public Decimal cNeuSentiment {get; set;}
    }
    public class CustomerEmotion{
    	public String top1emoLabel {get; set;}
    	public String top2emoLabel {get; set;}
    	public String top3emoLabel {get; set;}
    	public String top4emoLabel {get; set;}
    	public Decimal top1emo {get; set;}
    	public Decimal top2emo {get; set;}
    	public Decimal top3emo {get; set;}
    	public Decimal top4emo {get; set;}   
    }  
}