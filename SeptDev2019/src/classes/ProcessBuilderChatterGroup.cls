public without sharing class ProcessBuilderChatterGroup {

	public class Request {
		@InvocableVariable(label='Record Id To Work With' required=true)
		public  ID recordId;
	}

	@InvocableMethod(label='Process NPS Posts' description='This method receives the NPS posts')
	public static void SendPosts(List<Request> requests) {
		set<id> recordIds = new set<id>();
		for(Request request : requests) {
			if(request != null) recordIds.add(request.recordId);
		}
		if(recordIds!=null) postToChatter(recordIds);
	}
	

	public static void postToChatter(set<id> recordIds){
		ingage_Application_Settings__c setting= ingage_Application_Settings__c.getOrgDefaults();
		id groupId = setting.Chatter_Group_for_NPS__c;
		map<id,string> ownerNames = new map<id,string>();
		
		if(groupId != null){
			list<FeedItem> feedItems = new list<FeedItem>();
			//list<string> ownerNames = new list<string>();
			for(Case c: [SELECT ID, Owner.FirstName from Case where Id IN:recordIds]){
				//ownerNames.add(c.Owner.FirstName);
				ownerNames.put(c.OwnerId,c.Owner.FirstName);
			}
			for(Opportunity O: [SELECT ID, Owner.FirstName, OwnerId from Opportunity where Id IN:recordIds]){
				//ownerNames.add(o.Owner.FirstName);
				ownerNames.put(o.OwnerId,o.Owner.FirstName);
			}
			map<id,user> userMap = new map<id,user>([select id from user where id in:ownerNames.keySet()]);
			
			for(id id : ownerNames.keySet()){
				String message='Well done to '+ ownerNames.get(id) + ' for a positive Net Promoter Score';
				FeedItem fi = new FeedItem(Body = message, ParentId = groupId /*, CreatedBy = userMap.get(id), CreatedDate = datetime.now()*/ );
				system.debug('Feeditem = '+ fi);
				feedItems.add(fi);
			}
			if(!feedItems.isEmpty()) insert feedItems;
		}
	}
	
	
}