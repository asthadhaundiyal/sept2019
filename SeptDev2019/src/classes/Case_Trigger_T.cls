/**
 * Test Class for Case_Trigger
 */
@isTest
private class Case_Trigger_T {

    /**
     *
     */
    static testMethod void test1() {
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            FeatureManagement.setPackageBooleanValue('FCR_Feature_Activated', true);
        	FeatureConsoleAPI.enableFCR();
            Employee__c emp = new Employee__c();
            emp.First_Name__c = 'Test';
            insert emp;
            Account a = new account(name = 'test');
            insert a;
            Contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            Case c = new Case(accountId = a.id,contactId = con.id, subject = 'test', type='Other', Status = 'Closed');
            c.in_gage__Employee__c = emp.id;
            
            
            Test.startTest();
            
                insert c;
                
                //c.Status = [select MasterLabel from CaseStatus where isClosed = true limit 1].MasterLabel;
                
                update c;
                
                delete c;
                
                undelete c;
            
            Test.stopTest();
        }
    }
	static testMethod void testAI() {
        
        
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs (thisUser) {
            
			FeatureManagement.setPackageBooleanValue('Case_Language_Auto_Detect_Activated', true);
        	FeatureConsoleAPI.enableCaseLangAutoDetect();
            FeatureManagement.setPackageBooleanValue('QA_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQAWithAI();
            FeatureManagement.setPackageBooleanValue('QA_SE_with_AI_Activated', true);
        	FeatureConsoleAPI.enableQASEWithAI();
            Test.setMock(HttpCalloutMock.class, new AIMockHttpResponseGenerator());
            
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c(Reduce_QA_AI_Procesing__c = false, AI_Customer_ID__c = 'INGAI1802', AI_Case_Category__c = 'Type', AI_Case_SubCategory__c = 'Reason' );
            insert setting;
        
            Employee__c emp = new Employee__c();
        	emp.First_Name__c = 'Test';
        	insert emp;
            
            Account a = new account(name = 'test', Language__c='English');
			insert a;
			Contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
			insert con;
        	Case c = new Case(contactId = con.id, subject = 'test', type='Other', Status = 'New', Description='My test is disabled', accountId = a.id);
        	c.in_gage__Employee__c = emp.id;
            insert c;
            
            Test.startTest();
            c.Status = 'Closed';
            update c;
            Test.stopTest();
        }
            
    }

	static testMethod void testSendSurveyOnClosedCase() {
        
          User thisUser = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
          System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Customer_NPS_Survey_ID__c ='1';
            setting.Reminder_Survey_NPS__c = 1;
            insert setting;
            
            Survey_Settings__c surveySetting = new Survey_Settings__c(SetupOwnerId = thisUser.ProfileId, Send_NPS_Survey__c = true);
            insert surveySetting;
    		
            Case_Types_to_Run_Case_Trigger__c cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            Case_Types_For_Survey__c cSetting = new Case_Types_For_Survey__c();
            cSetting.Name = 'Other';
            insert cSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            Case c = new Case(accountId = a.id, contactId = con.id, subject = 'test', type='Other', Status = 'Closed', in_gage__Employee__c = emp.id);

            Test.startTest();
			  insert c;
            
            Test.stopTest();
        }
    }
    
    static testMethod void testCustomSurveySent() {
        
          User thisUser = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
          System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Customer_NPS_Survey_ID__c ='1';
            setting.Reminder_Survey_NPS__c = 1;
            setting.Is_Custom_Survey__c = true;
            insert setting;
            
            Survey_Settings__c surveySetting = new Survey_Settings__c(SetupOwnerId = thisUser.ProfileId, Send_NPS_Survey__c = true);
            insert surveySetting;
    		
            Case_Types_to_Run_Case_Trigger__c cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            Case_Types_For_Survey__c cSetting = new Case_Types_For_Survey__c();
            cSetting.Name = 'Other';
            insert cSetting;
        
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            system.assertNotEquals(null,emp.id);
            
            account a = new account(name = 'test');
            insert a;
            
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
            
            Case c = new Case(accountId = a.id, contactId = con.id, subject = 'test', type='Other', Status='Closed', in_gage__Employee__c = emp.id);

            Test.startTest();
            
               insert c;
            
            Test.stopTest();
        }
    }
    
    static testMethod void testSendSurveyReminderOnClosedCase() {
        User thisUser = [SELECT Id, ProfileId FROM User WHERE Id = :UserInfo.getUserId()];
          System.runAs (thisUser) {
			FeatureManagement.setPackageBooleanValue('NPS_Feature_Activated', true);
        	FeatureConsoleAPI.enableNPS();
            ingage_Application_Settings__c setting = new ingage_Application_Settings__c();
            setting.Customer_NPS_Survey_ID__c ='1';
            setting.Reminder_Survey_NPS__c = 1;
            insert setting;
            
            Survey_Settings__c surveySetting = new Survey_Settings__c(SetupOwnerId = thisUser.ProfileId, Send_NPS_Survey__c = true);
            insert surveySetting;
    		
            Case_Types_to_Run_Case_Trigger__c cTypeSetting = new Case_Types_to_Run_Case_Trigger__c();
            cTypeSetting.Name = 'Other';
            insert cTypeSetting;
            
            Case_Types_For_Survey__c cSetting = new Case_Types_For_Survey__c();
            cSetting.Name = 'Other';
            insert cSetting;
            
            account a = new account(name = 'test');
            insert a;
            
            contact con = new contact(lastname ='Test', email ='test.test@test.com', accountId = a.id);
            insert con;
              
            Employee__c emp = new employee__c(User_Record__c = userInfo.getUserId(), First_Name__c ='T', Last_Name__c = 'Est', Email__c = 'test.test@test.com', Has_In_Gage_Licence__c = true);
            insert emp;
            Case c = new Case(accountId = a.id, contactId = con.id, subject = 'test', type='Other', Status = 'Closed' , in_gage__Can_Send_NPS_Survey__c=true, in_gage__Survey_Reminder_Date__c=Date.today());
            c.in_gage__Employee__c = emp.id;
            
            Test.startTest();
            
                insert c;
            
            Test.stopTest();
        }
    }
}