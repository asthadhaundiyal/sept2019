@isTest
public with sharing class SurveyDML_T {
	
	 static Employee__c emp;
	 
	
	
	private static void setupData(){
	
		
      User u =[SELECT ID from User where Profile.Name='System Administrator' and isActive = true limit 1];
	
	  emp = new Employee__c();
      emp.First_Name__c = 'Test';
      emp.Default_Survey_Day__c = date.today().day();
      emp.Next_Survey_Date__c = date.today();
      emp.User_Record__c=u.Id;

      insert emp;
			
	}
	

	private static testmethod void test1(){
	
		setupData();
		
		Engagement_Survey_Feedback__c esf = new Engagement_Survey_Feedback__c(
			Employee_Lookup__c = emp.id
		);
		system.assertNotEquals(null,SurveyDML.insertESF(esf));

	}
    
    private static testmethod void test2(){
		
		setupData();
		
		NPS__c nps = new NPS__c(
			Employee__c = emp.id
		);
		system.assertNotEquals(null,SurveyDML.insertNPS(nps));
	}

	private static testmethod void test3(){
		setupData();
		system.assertNotEquals(false,SurveyDML.updateEmployee(emp));
	}
    
    private static testmethod void test4(){
		Opportunity o = new opportunity(name ='Test Oppty Name', StageName ='Test', CloseDate=date.today());
        insert o;
		system.assertNotEquals(false,SurveyDML.updateOppty(o));
	}
    
    private static testmethod void test5(){
		case c = new case(Subject='TestCase');
        insert c;
		system.assertNotEquals(false,SurveyDML.updateCase(c));
	}
    
    
    
}