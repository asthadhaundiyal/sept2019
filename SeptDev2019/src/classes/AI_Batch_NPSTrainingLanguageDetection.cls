global class AI_Batch_NPSTrainingLanguageDetection implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful{
    
    global final String query;
    global final set<id> npsAIFIds;
    global final Map<String, Set<String>> fbMap;
    global final Map<String, String> npsLangMap = createNPSLanguageMap();
    
    
    global AI_Batch_NPSTrainingLanguageDetection(set<id> npsAIFIds){
        this.npsAIFIds = npsAIFIds;
        query = getCaseQuery() + ' where id in :npsAIFIds';
    }
    global AI_Batch_NPSTrainingLanguageDetection(String q){
        query = q;
    }
    private string getCaseQuery(){
        return 'select id, Text_for_Training__c, Category__c, Language__c from NPS_AI_Feedback__c';
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<NPS_AI_Feedback__c> npsAIFList = (List<NPS_AI_Feedback__c>)scope;
        List<NPS_AI_Feedback__c> npsAIFWithLangList = new List<NPS_AI_Feedback__c>();       
        for(NPS_AI_Feedback__c nps :npsAIFList){
            if(nps.Text_for_Training__c != null){
                HttpRequest req = new HttpRequest();
                String endPoint = 'https://ingage-ai.co.uk/language/predict';
                system.debug('Language Detection EndPoint: '+ endPoint);
                req.setEndpoint(endPoint);
                req.setTimeout(120000);
                req.setHeader('Content-Type', 'application/json');
                req.setBody(stringToJSONforLangDetection(nps.Text_for_Training__c));
                req.setMethod('POST');
                system.debug('Request sent: '+ req);
                Http http = new Http();
                HTTPResponse res = http.send(req);
                if (res.getStatusCode() != 200) {
                    System.debug('The status code returned was not expected: ' + res.getStatusCode() + ' ' + res.getStatus());
                }else {
                    System.debug('The response: '+ res.getBody());
                    String langDetected = (String)JSON.deserialize(res.getBody(), String.class);
                    nps.Language__c = langDetected;
                    
                    String processedReason = '';
                    String fComment = nps.Text_for_Training__c;
                    //update nps reasons
                    if(fComment!= null && langDetected != ''){
                        //get the nps feedback type values for each type for the training language      
                        Map<String, NPS_Feedback_Types__c> npsReasonsMap = NPS_Feedback_Types__c.getAll();
                        List<String> npsReasonList = new List<String>();
                        npsReasonList.addAll(npsReasonsMap.keySet());
                        
                        Map<String, Set<String>> npsMap = new Map<String, Set<String>>();
                        for(String feedbackName : npsReasonsMap.keySet()){
                            if(feedbackName.containsIgnoreCase(langDetected) || Test.isRunningTest()){                  
                                Set<String> valuesSet = new Set<String>();
                                String feedbackType = npsReasonsMap.get(feedbackName).Type__c;
                                String searchText = npsReasonsMap.get(feedbackName).Values__c;
                                List<String> splitText = searchText.split(',');
                                for(String s : splitText)valuesSet.add(s.trim());
                                npsMap.put(feedbackType, valuesSet); 
                            }
                            
                        }
                        system.debug('npsMap: '+ npsMap);
                        if(!npsMap.isEmpty()){
                            String npsReason = '';
                            Set<String> npsReasonsSet = null;
                            List<String> partsList = fComment.toLowerCase().split('[\'\"\\n\\s,;@&.?$+-]+');
                            for(String fbType : npsMap.keySet()){
                                if(!partsList.isEmpty()){
                                    for(String s: partsList){
                                        if(npsMap.get(fbType).contains(s)){
                                            npsReason += fbType+', ';
                                        }
                                    }
                                }
                            } 
                            if(npsReason!=null && npsReason!=''){
                                npsReasonsSet = new Set<String>(npsReason.split(', '));
                            }
                            if(npsReasonsSet!=null){
                                for(String s: npsReasonsSet)
                                    processedReason  += s +', ';
                            }else processedReason = 'Other';
                        }else processedReason = 'Other';              
                    }
                    nps.Category__c = processedReason;
                    npsAIFWithLangList.add(nps);
                }
            }
        }
        system.debug('npsAIFWithLangList: '+ npsAIFWithLangList);
        if(!npsAIFWithLangList.isEmpty()) update npsAIFWithLangList;
    }
    global void finish(Database.BatchableContext BC){}
    
    //Helper methods
    private static String stringToJSONforLangDetection(String description) {
        if(description.length() >= 24) description =+ description.substring(0, 24);
        system.debug('description: '+ description);
        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
        generator.writeStringField('text', description);
        generator.writeEndObject();
        String jsonString =  generator.getAsString();
        system.debug('jsonString: '+ jsonString);
        return jsonString;
    }    
    private Map<String, String> createNPSLanguageMap(){
        Map<String, NPS_Feedback_Types__c> typeSettingMap = NPS_Feedback_Types__c.getAll();
        Map<String, String> langMap = new Map<String, String>();
        for(String feedbackName : typeSettingMap.keySet()){
            Set<String> valuesSet = new Set<String>();
            String feedbackType = typeSettingMap.get(feedbackName).Language__c;
            String aiLang = typeSettingMap.get(feedbackName).AI_Training_Language__c;
            langMap.put(feedbackType, aiLang);
        }
        return langMap;
    }
    
}