trigger UserTrigger on User (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.User_Trigger triggerHandler = new in_gage.User_Trigger();
    triggerHandler.execute();
}