trigger NPSAITrigger on NPS_AI__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.NPSAI_Trigger triggerHandler = new in_gage.NPSAI_Trigger();
    triggerHandler.execute();
}