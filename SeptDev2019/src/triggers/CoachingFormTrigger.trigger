trigger CoachingFormTrigger on Coaching_Form__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.CoachingForm_Trigger triggerHandler = new in_gage.CoachingForm_Trigger();
    triggerHandler.execute();
}