trigger EmployeeTrigger on Employee__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) {

    in_gage.Employee_Trigger triggerHandler = new in_gage.Employee_Trigger();
    triggerHandler.execute();
}