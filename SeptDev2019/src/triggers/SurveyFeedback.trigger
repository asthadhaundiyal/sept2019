trigger SurveyFeedback on Engagement_Survey_Feedback__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) 
{
	in_gage.SurveyFeedback_Trigger triggerHandler = new in_gage.SurveyFeedback_Trigger();
    triggerHandler.execute();  
}