trigger QualityAITrigger on QA_AI__c (after update) {

    in_gage.QualityAI_Trigger triggerHandler = new in_gage.QualityAI_Trigger();
    triggerHandler.execute();
}