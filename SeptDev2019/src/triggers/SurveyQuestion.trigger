trigger SurveyQuestion on Survey_Question__c (before insert, before update, after insert, after update, before delete, after delete, after undelete) 
{
	in_gage.SurveyQuestion_Trigger triggerHandler = new in_gage.SurveyQuestion_Trigger();
    triggerHandler.execute();  
}