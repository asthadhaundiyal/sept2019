trigger CoachingItemTrigger on Coaching_Items__c (before insert) {
  in_gage.CoachingItem_Trigger triggerHandler = new in_gage.CoachingItem_Trigger();
  triggerHandler.execute();
}