trigger EmailMessageTrigger on EmailMessage (before insert, before update, after insert, after update, before delete, after delete, after undelete) {
	in_gage.EmailMessage_Trigger triggerHandler = new in_gage.EmailMessage_Trigger();
    triggerHandler.execute();
}