({
	selectedQuestion : {},
	questionToIndex : {},
	questionToQuestionRender : {},
	isBlank : function(str) {
		if(str == null || str == undefined || str == '') {
			return true;
		}
		return false;
	},
	isArrayBlank : function(arr) {
		if(arr == null || arr == undefined) {
			return true;
		} else if(arr.length == 0){
			return true;
		}
		return false;
	},
	submitSurvey : function(component, wrapper) {
		console.log(wrapper);
		var action = component.get("c.submitSurveyResponse");
		action.setParams({
			'wrapperStr': JSON.stringify(wrapper)
		})
		action.setCallback(this, function(response) {
			if(response.getState() === 'SUCCESS') {
				component.set("v.isSubmitted", true);
			}
		})
		$A.enqueueAction(action);
	},
	setIndexForQuestion : function(helper, arr) {
		var i = 0;
		arr.forEach(element => {
			helper.questionToIndex[element.question.Id] = i;
			i++;
		});
	}
})