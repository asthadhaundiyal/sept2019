({
	doInit : function(component, event, helper) {
		var action = component.get("c.initialize");
		action.setParams({
			'surveyId': component.get("v.surveyId")
		})
		action.setCallback(this, function(response) {
			if(response.getState()) {
				var wrapper = JSON.parse(response.getReturnValue());
				component.set("v.isInitialize", true);
				component.set("v.wrapper", wrapper);
				component.set("v.currentQuestion", wrapper.survey.in_gage__First_Question__c);
				helper.setIndexForQuestion(helper, wrapper.listOfWrapper);
				if(helper.isBlank(wrapper.listOfWrapper[helper.questionToIndex[wrapper.survey.in_gage__First_Question__c]].question.in_gage__Default_Next_Question__c)) {
					component.set("v.isNextQuestion", false);
				} else {
					component.set("v.isNextQuestion", true);
				}
				if(component.get("v.isLiveChat")) {
					var i = 1;
					wrapper.listOfWrapper.forEach(wrap => {
						helper.selectedQuestion[i] = wrap.question.Id;
						i++;
					});
					i--;
					component.set("v.questionNumber", i);
					component.set("v.isSubmit", true);
				}
			}
		})
		$A.enqueueAction(action);
	},
	renderQuestion : function(component, event, helper) {
		var wrapper = component.get("v.wrapper");
		var questionId;
		if(typeof event.getSource().get("v.value") === "object") {
			questionId = wrapper.mapOfOptionIdToQuestionId[event.getSource().get("v.value")[0]];
		} else {
			questionId = wrapper.mapOfOptionIdToQuestionId[event.getSource().get("v.value")];
		}
		console.log(event.getSource().get("v.value"));
		var wrap = wrapper.listOfWrapper[helper.questionToIndex[questionId]];
		console.log(JSON.stringify(wrap));
		var nextQuestion;
		if(!helper.isBlank(wrap.optionId)){
			var next = wrap.surveyOptionsMap[wrap.optionId].in_gage__Next_Question__c;
			if(!helper.isBlank(next)) {
				nextQuestion = next;
			}
		} else if(!helper.isArrayBlank(wrap.optionIds)) {
			var next = new Set();
			wrap.optionIds.forEach(option => {
				if(!helper.isBlank(option)){
					next.add(wrap.surveyOptionsMap[option].in_gage__Next_Question__c);
				}
			});
			var nextArr = [];
			next.forEach(element => {
				if(!helper.isBlank(element)){
					nextArr.push(element);
				}
			});
			if(nextArr.length == 1) {
				nextQuestion = nextArr[0];
			}
		}
		if(helper.questionToQuestionRender[questionId]) {
			var renderedWrap = wrapper.listOfWrapper[helper.questionToIndex[helper.questionToQuestionRender[questionId]]];
			renderedWrap.isSkipped = true;
			wrapper.listOfWrapper[helper.questionToIndex[helper.questionToQuestionRender[questionId]]] = renderedWrap;
		}
		if(!helper.isBlank(nextQuestion)) {
			var renderWrap = wrapper.listOfWrapper[helper.questionToIndex[nextQuestion]];
			renderWrap.isSkipped = false;
			wrapper.listOfWrapper[helper.questionToIndex[nextQuestion]] = renderWrap;
			helper.questionToQuestionRender[questionId] = nextQuestion;
		}
		component.set("v.wrapper", wrapper);
	},
	previousQuestion : function(component, event, helper) {
		var wrapper = component.get("v.wrapper");
		component.set("v.isNextQuestion", true);
		component.set("v.isSubmit", false);
		component.set("v.questionNumber", component.get("v.questionNumber") - 1);
		if(component.get("v.questionNumber") == 1) {
			component.set("v.isPreviousQuestion", false);
		}
		component.set("v.currentQuestion", helper.selectedQuestion[component.get("v.questionNumber")]);
	},
	nextQuestion : function(component, event, helper) {
		component.set("v.isPreviousQuestion", true);
		helper.selectedQuestion[component.get("v.questionNumber")] = component.get("v.currentQuestion");
		component.set("v.questionNumber", component.get("v.questionNumber") + 1);

		var wrapper = component.get("v.wrapper");
		var wrap = wrapper.listOfWrapper[helper.questionToIndex[component.get("v.currentQuestion")]];
		var nextQuestion = wrap.question.in_gage__Default_Next_Question__c;

		if(!helper.isBlank(wrap.optionId)){
			var next = wrap.surveyOptionsMap[wrap.optionId].in_gage__Next_Question__c;
			if(!helper.isBlank(next)) {
				nextQuestion = next;
			}
		} else if(!helper.isArrayBlank(wrap.optionIds)) {
			var next = new Set();
			wrap.optionIds.forEach(option => {
				if(!helper.isBlank(option)){
					next.add(wrap.surveyOptionsMap[option].in_gage__Next_Question__c);
				}
			});
			var nextArr = [];
			next.forEach(element => {
				if(!helper.isBlank(element)){
					nextArr.push(element);
				}
			});
			if(nextArr.length == 1) {
				nextQuestion = nextArr[0];
			}
		}
		component.set("v.currentQuestion", nextQuestion);

		if(helper.isBlank(wrapper.listOfWrapper[helper.questionToIndex[nextQuestion]].question.in_gage__Default_Next_Question__c)) {
			component.set("v.isNextQuestion", false);
			component.set("v.isSubmit", true);
		} else {
			component.set("v.isNextQuestion", true);
			component.set("v.isSubmit", false);
		}
	},
	submitQuestion : function(component, event, helper) {
		var wrapper = component.get("v.wrapper");
		var wrap;
		if(!component.get("v.isLiveChat")) {
			helper.selectedQuestion[component.get("v.questionNumber")] = component.get("v.currentQuestion");
			Object.values(helper.selectedQuestion).forEach(element => {
				wrap = wrapper.listOfWrapper[helper.questionToIndex[element]];
				wrap.isSkipped = false;
				wrapper.listOfWrapper[helper.questionToIndex[element]] = wrap;
			});
		}
		helper.submitSurvey(component, wrapper);
	}
})