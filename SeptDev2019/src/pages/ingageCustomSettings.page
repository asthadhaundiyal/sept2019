<apex:page controller="ingageCustomSettingController" lightningStylesheets="true">
    <apex:form >
        
        <apex:pageBlock title="In-gage Custom Settings">
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!saveValues}" />
                <apex:commandButton value="Setup" action="{!setupPage}" />
                <apex:commandButton value="Employee Page View Mapping" action="{!pMappingSetupPage}" />
                <apex:commandButton value="AI Setup" action="{!aISetupPage}" />
            </apex:pageBlockButtons>
            
            <apex:pageMessages id="showmsg"></apex:pageMessages>
            
            <apex:pageBlockSection title="Survey Settings" collapsible="false" rendered="{!npsEnabled}">
                
                <apex:inputField value="{!setting.Company_Name__c}" label="Company Name"/>
                <apex:inputField value="{!setting.Survey_Site_URL__c}" label="Survey Site URL"/>
                <apex:inputField value="{!setting.Company_Logo__c}" label="Company Logo"/>
                <apex:inputField value="{!setting.Survey_Site_Background_Image_URL__c}" label="Survey Background Image URL" />
                
                <apex:inputField value="{!setting.Loyal_Customer__c}" label="Loyal Customer Threshold" />
                
                <apex:inputField value="{!setting.Lifetime_Value_of_Customer__c}" label="Lifetime Value of Customer (in {!userCCY})" />
                <apex:inputField value="{!setting.Detractor_Impact__c}" label="Detractor Impact" />
                
                <apex:SelectList value="{!chatterGroup }" size="1" label="Chatter Group for NPS Posts">
                    <apex:SelectOptions value="{!AllChatterGroups}"/>
                </apex:selectList>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection columns="1" collapsible="false" showHeader="false">
                <hr/>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="false" rendered="{!npsEnabled}">
                <apex:inputField value="{!setting.Employee_Engagement_Survey__c}" label="Employee Engagement Survey ID" rendered="{!eeEnabled}"/>
                <apex:inputField value="{!setting.Reminder_Survey_EES__c}" label="Reminder (days)" rendered="{!eeEnabled}"/>
                <apex:inputField value="{!setting.Customer_NPS_Survey_ID__c}" label="NPS Survey ID"/>
                <apex:inputField value="{!setting.Reminder_Survey_NPS__c}" label="Reminder (days)"/>
                <apex:inputField value="{!setting.Opportunity_Won_Survey__c}" label="Oppty Won Survey ID"/>
                <apex:inputField value="{!setting.Reminder_Survey_Opp__c}" label="Reminder (days)"/>
                <apex:inputField value="{!setting.Survey_Cooling_off_Period__c}" label="Survey Cooling-off period"/>
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Is_Custom_Survey__c.inlineHelpText}">
                    <apex:outputLabel >Is Custom Survey?</apex:outputLabel>
                    <apex:inputField value="{!setting.Is_Custom_Survey__c}"/>
                </apex:pageBlockSectionItem> 
                <apex:pageblockSectionItem />
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Reminder_Survey_Batch_Size__c.inlineHelpText}">
                    <apex:outputLabel >Maintenance Batch Size</apex:outputLabel>
                    <apex:inputField value="{!setting.Reminder_Survey_Batch_Size__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Analytics_Batch_Size__c.inlineHelpText}">
                    <apex:outputLabel >Analytics Batch Size</apex:outputLabel>
                    <apex:inputField value="{!setting.Analytics_Batch_Size__c}" />
                </apex:pageBlockSectionItem>               
            </apex:pageBlockSection>
            <apex:pageBlockSection columns="2" collapsible="false" showHeader="false" rendered="{!npsEnabled}">    
                <apex:selectList value="{!caseSurveyTypes}" multiselect="true" label="Case Types to Send Survey">
                    <apex:SelectOptions value="{!caseSurveyTypeOptions}"/>
                </apex:selectList>
            </apex:pageBlockSection>
            
            
            <apex:pageBlockSection title="Case Status for Non-FCR Cases" collapsible="false" rendered="{!fcrEnabled}">
                <apex:inputField value="{!setting.FCR_Additional_Status_1__c}" label="FCR Custom Status 1"/>
                <apex:inputField value="{!setting.FCR_Additional_Status_2__c}" label="FCR Custom Status 2"/>
                <apex:inputField value="{!setting.FCR_Additional_Status_3__c}" label="FCR Custom Status 3"/>
                <apex:inputField value="{!setting.FCR_Additional_Status_4__c}" label="FCR Custom Status 4"/>
                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="App Setting" collapsible="false">
                <apex:inputField value="{!setting.No_of_Days_Searched__c}" label="Days for Record Search on Quality Assessment Form" rendered="{!qualityEnabled}"/>
                <apex:inputField value="{!setting.No_of_Days_Lookup_for_Analytics__c}" label="Days for Analytic Record Search on Case and Opportunity" rendered="{!npsEnabled}"/>
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Profiles_Only__c.inlineHelpText}">
                    <apex:outputLabel >Profile Mapped at Install?</apex:outputLabel>
                    <apex:inputField value="{!setting.Profiles_Only__c}" label="Profile Mapped at Install"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem />
                <apex:selectList value="{!caseTypesTrigger}" multiselect="true" label="Case Types to Run Case Trigger">
                    <apex:SelectOptions value="{!caseTypeOptions}"/>
                </apex:selectList>
                <apex:selectList value="{!caseRecordTypesTrigger}" multiselect="true" label="Case Record Types to Run Case Trigger">
                    <apex:SelectOptions value="{!caseRecordTypesOptions}"/>
                </apex:selectList>  
                <apex:selectList value="{!oppTypesTrigger}" multiselect="true" label="Opportunity Types to Run Opportunity Trigger">
                    <apex:SelectOptions value="{!oppTypeOptions}"/>
                </apex:selectList>
                <apex:selectList value="{!oppRecordTypesTrigger}" multiselect="true" label="Opportunity Record Types to Run Opportunity Trigger">
                    <apex:selectOptions value="{!oppRecordTypesOptions}"/>
                </apex:selectList>
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Reduce_FCR30_Calculations__c.inlineHelpText}">
                    <apex:outputLabel >Reduce Background Calculations</apex:outputLabel>
                    <apex:inputField value="{!setting.Reduce_FCR30_Calculations__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Bulk_Case_Upload__c.inlineHelpText}">
                    <apex:outputLabel >Deactivate In-gage Case Trigger</apex:outputLabel>
                    <apex:inputField value="{!setting.Bulk_Case_Upload__c}"/>
                </apex:pageBlockSectionItem>  
                <apex:pageBlockSectionItem helpText="{!$ObjectType.ingage_Application_Settings__c.Fields.Deactivate_Opportunity_Trigger__c.inlineHelpText}">
                    <apex:outputLabel >Deactivate In-gage Opportunity Trigger</apex:outputLabel>
                    <apex:inputField value="{!setting.Deactivate_Opportunity_Trigger__c}"/>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>          
        </apex:pageBlock>
    </apex:form>
</apex:page>